package com.sita.cfmoto;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.sita.cfmoto.ui.view.ImgTextBtn;

/**
 * Created by markman on 2016/5/14.
 */
public class TabController {

    private ImgTextBtn[] mTabs;
    private Fragment[] mTabFragments;
    private int mTabCounts = 0;

    private Fragment mCurrentFragment = null;
    private int currentTabIndex = -1;
    private FragmentManager mFragmentManager = null;
    private int mContainer = -1;
    private OnTabSelectedListener mListener = null;

    public interface OnTabSelectedListener {
        void onSelected(int index);
    }

    public TabController(ImgTextBtn[] btns, Fragment[] fragments, FragmentManager fragmentManager, int container) {
        mTabs = btns;
        mTabFragments = fragments;
        mTabCounts = btns.length;
        mFragmentManager = fragmentManager;
        mContainer = container;
        initTabs();
    }

    public void showTab(int index) {
        selectFragment(index);
    }

    public void registerListener(OnTabSelectedListener listener) {
        mListener = listener;
    }

    private void initTabs() {
        if (mTabs.length != mTabFragments.length) {
            return;
        }

        mTabs[0].setSelected(true);
        TabButtonClickListener myTabButtonClickListener = new TabButtonClickListener();

        mTabs[0].setOnClickListener(myTabButtonClickListener);
        mTabs[1].setOnClickListener(myTabButtonClickListener);
        mTabs[2].setOnClickListener(myTabButtonClickListener);
        mTabs[3].setOnClickListener(myTabButtonClickListener);

        if (currentTabIndex == -1) {
            mTabs[0].performClick();
        }
    }

    private class TabButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int index = -1;
            for (int i = 0; i < mTabCounts; i++) {
                if (view == mTabs[i]) {
                    index = i;
                    if (mListener != null) {
                        mListener.onSelected(index);
                    }
                    break;
                }
            }

            selectFragment(index);
            selectTab(index);
        }
    }

    private void selectTab(int index) {
        for (ImgTextBtn btn : mTabs) {
            btn.setSelected(false);
        }
        mTabs[index].setSelected(true);
    }

    /*
     * tab fragment
     */
    private Fragment selectFragment(int index) {
        if (currentTabIndex == index || index < 0 || index >= mTabCounts) {
            return mCurrentFragment;
        }

        // change the fragment
        FragmentTransaction ftx = mFragmentManager.beginTransaction();
        if (mCurrentFragment != null) {
            ftx = ftx.hide(mCurrentFragment);
        }
        Fragment currentFragment = getFragment(currentTabIndex = index);
        if (!currentFragment.isAdded()) {
            ftx = ftx.add(mContainer, currentFragment, getFragmentTag(currentTabIndex));
        }
//        ftx.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        ftx.show(currentFragment).commit();
        return currentFragment;

    }

    public String getFragmentTag(int index) {
        return String.valueOf(index);
    }

    private Fragment getFragment(int index) {
        Fragment fragment = mTabFragments[index];
        mCurrentFragment = fragment;
        return fragment;

    }

}
