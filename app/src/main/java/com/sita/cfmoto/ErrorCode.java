package com.sita.cfmoto;

/**
 * Created by wxd on 16/6/14.
 */
public class ErrorCode {
    public static final String SUCCESS = "0";
    public static final String ACCOUNT_NOT_FOUND = "1";
    public static final String ROUTE_NOT_FOUND = "2";
    public static final String INVALID_REQUEST = "3";
    public static final String REGISTER_ACCOUNT_FAILED = "4";
    public static final String LOGIN_ACCOUNT_FAILED = "5";
    public static final String UPDATE_ACCOUNT_FAILED = "6";
    public static final String DELETE_ACCOUNT_FAILED = "7";
    public static final String LOGOUT_ACCOUNT_FAILED = "8";
    public static final String RESOURCE_NOT_FOUND = "9";
    public static final String LOGIN_REQUIRED = "10";
    public static final String INVALID_AUDIO_TYPE = "11";
    public static final String INTERNAL_SERVICE_ERROR = "12";
    public static final String ACCOUNT_UNIQUE_ID_EXISTS = "13";
    public static final String GROUP_NOT_FOUND = "14";
    public static final String RENTTRIP_ALREADY_BE_KILL = "15";
    public static final String SMS_CODE_IS_UNLAW = "16";
    public static final String NODE_LIST_INFO_FAILED = "17";
    public static final String MOBILE_EXIST = "18";
    public static final String MOBILE_NOT_EXIST = "19";
    public static final String WRONG_PASSWORD = "20";
    public static final String VEHICLE_SN_NOT_EXIST = "21";
    public static final String VEHICLE_SNPASSWORD_WRONG = "22";
    public static final String FAILURE = "23";
    public static final String CAPTCHA_TIMEOUT_OR_ERROR = "24";
}
