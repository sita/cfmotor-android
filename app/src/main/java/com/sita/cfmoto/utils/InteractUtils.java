package com.sita.cfmoto.utils;

import com.google.gson.reflect.TypeToken;
import com.sita.cfmoto.rest.RestClient;
import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.rest.model.Comment;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.rest.model.Inform;
import com.sita.cfmoto.rest.model.InteractCount;
import com.sita.cfmoto.rest.model.Member;
import com.sita.cfmoto.rest.model.RestResponse;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.rest.model.TopicAuthor;
import com.sita.cfmoto.rest.model.request.FetchCommentListRequest;
import com.sita.cfmoto.rest.model.request.FetchDynamicListRequest;
import com.sita.cfmoto.rest.model.request.FetchInformListRequest;
import com.sita.cfmoto.rest.model.request.FetchLikeListRequest;
import com.sita.cfmoto.rest.model.request.FetchMyActiveListRequest;
import com.sita.cfmoto.rest.model.request.FetchMyTopicListRequest;
import com.sita.cfmoto.rest.model.request.FetchTopicListRequest;
import com.sita.cfmoto.rest.model.request.ReleaseCommentRequest;
import com.sita.cfmoto.rest.model.request.ReleaseLikeRequest;
import com.sita.cfmoto.rest.model.request.ReleaseUnLikeRequest;

import java.util.Iterator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by lijuan zhang on 2016/8/15.
 */
public class InteractUtils {

    public static void fetchActiveList(int type, Integer size, Integer page, Long beginDate, Long endDate,
                                       Long maxId, Long minId, Integer source, final FetchActiveListCallback callback) {
        RestClient.getRestService().fetchActiveList(size, page, beginDate, endDate, maxId, minId, source, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Active> actives = RestClient.getGson().fromJson(str, new TypeToken<List<Active>>() {
                    }.getType());
                    Iterator<Active> iterator = actives.iterator();
                    while (iterator.hasNext()) {
                        Active active = iterator.next();
                        PersistUtils.saveActive(active);
                    }
                    if (type == 0 && page == 0) {
                        LocalStorage.setActiveListRefreshTime(endDate);
                    }
                    callback.onActiveListFetched(actives);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onActiveListFetched(null);
            }
        });
    }

    /*type: 自定义请求类型，动态列表为0，话题详情页的动态列表请求为1，其他为 非0，1*/
    public static void fetchDynamicList(int type, Integer size, Integer page, String topicId, Long beginDate, Long endDate, Long maxId, Long minId, Integer source, Integer mine, final FetchDynamicListCallback callback) {
        FetchDynamicListRequest request = new FetchDynamicListRequest();
        request.size = size;
        request.page = page;
        request.topicId = topicId;
        request.beginDate = beginDate;
        request.endDate = endDate;
        request.maxId = maxId;
        request.minId = minId;
        request.source = source;
        request.mine = mine;
        request.userId = String.valueOf(PersistUtils.getUsers().get(0).getAccountId());
        RestClient.getRestService().fetchDynamicList(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Dynamic> dynamics = RestClient.getGson().fromJson(str, new TypeToken<List<Dynamic>>() {
                    }.getType());
                    Iterator<Dynamic> iterator = dynamics.iterator();
                    while (iterator.hasNext()) {
                        Dynamic dynamic = iterator.next();
                        PersistUtils.saveDynamic(dynamic);
                    }
                    if (page == 0) {
                        if (type == 0) {
                            LocalStorage.setDynamicListRefreshTime(endDate);
                        } else if (type == 1) {
                            LocalStorage.setTopicDynamicListRefreshTime(endDate);
                        }
                    }
                    callback.onDynamicListFetched(dynamics);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onDynamicListFetched(null);

            }
        });
    }

    /*获取动态的评论列表*/
    public static void fetchCommentList(int size, int page, String postMsgId, final FetchCommentListCallback callback) {

        FetchCommentListRequest request = new FetchCommentListRequest();
        request.size = size;
        request.page = page;
        request.postMsgId = postMsgId;
        RestClient.getRestService().fetchCommentList(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Comment> comments = RestClient.getGson().fromJson(str, new TypeToken<List<Comment>>() {
                    }.getType());
                    callback.onCommentListFetched(comments);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onCommentListFetched(null);

            }
        });
    }

    /*发布评论*/
    public static void releaseComment(String accountId, String resId, String message, final ReleaseCommentCallback callback) {

        ReleaseCommentRequest request = new ReleaseCommentRequest();
        request.accountId = accountId;
        request.resId = resId;
        request.message = message;
        RestClient.getRestService().releaseComment(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
//                    String str = RestClient.getGson().toJson(restResponse.mData);
//                    List<Comment> comments = RestClient.getGson().fromJson(str, new TypeToken<List<Comment>>() {
//                    }.getType());
                    callback.onCommentReleased(true);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onCommentReleased(false);

            }
        });
    }


    /*发布点赞*/
    public static void releaseLike(long accountId, long resId, final ReleaseLikeCallback callback) {
        ReleaseLikeRequest request = new ReleaseLikeRequest();
        request.userId = accountId;
        request.resId = resId;
        RestClient.getRestService().releaseLike(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    //成功后返回点赞人数
                    callback.onLikeReleased(true);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onLikeReleased(false);
            }
        });
    }

    /*发布取消点赞*/
    public static void releaseUnLike(long accountId, long resId, final ReleaseUnLikeCallback callback) {
        ReleaseUnLikeRequest request = new ReleaseUnLikeRequest();
        request.userId = accountId;
        request.resId = resId;
        RestClient.getRestService().releaseUnLike(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    //成功后返回点赞人数
                    callback.onUnLikeReleased(true);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onUnLikeReleased(false);

            }
        });
    }

    /*获取动态的点赞列表*/
    public static void fetchLikeList(long resId, final FetchLikeListCallback callback) {
        FetchLikeListRequest request = new FetchLikeListRequest();
        request.resId = resId;
        RestClient.getRestService().fetchLikeList(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Member> likeMembers = RestClient.getGson().fromJson(str, new TypeToken<List<Member>>() {
                    }.getType());
                    callback.onLikeListFetched(likeMembers);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onLikeListFetched(null);

            }
        });
    }

    /*获取话题列表*/
    public static void fetchTopicList(int type, Integer size, Integer page, Long beginDate, Long endDate, Long maxId, Long minId, final FetchTopicListCallback callback) {
        FetchTopicListRequest request = new FetchTopicListRequest();
        request.size = size;
        request.page = page;
        request.beginDate = beginDate;
        request.endDate = endDate;
        request.maxId = maxId;
        request.minId = minId;
        request.type = 0;
        request.source = 1;
        RestClient.getRestService().fetchTopicList(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Topic> topics = RestClient.getGson().fromJson(str, new TypeToken<List<Topic>>() {
                    }.getType());
                    Iterator<Topic> iterator = topics.iterator();
                    while (iterator.hasNext()) {
                        Topic topic = iterator.next();
                        TopicAuthor topicAuthor = topic.author;
                        PersistUtils.saveTopicAuthor(topicAuthor);
                        PersistUtils.saveTopic(topic);
                    }
                    if (type == 0 && page == 0) {
                        LocalStorage.setTopicListRefreshTime(endDate);
                    }
                    callback.onTopicListFetched(topics);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onTopicListFetched(null);
            }
        });

    }

    /*查询我发起的或者wo参与的活动*/
    public static void fetchMyActiveList(long userId, int size, int page, String beginDate, String endDate, String maxId, String minId, int searchType, final FetchMyActiveListCallback callback) {
        FetchMyActiveListRequest request = new FetchMyActiveListRequest();
        request.userId = userId;
        request.size = size;
        request.page = page;
        request.beginDate = beginDate;
        request.endDate = endDate;
        request.maxId = maxId;
        request.minId = minId;
        request.searchType = searchType;
        RestClient.getRestService().fetchMyActive(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Active> actives = RestClient.getGson().fromJson(str, new TypeToken<List<Active>>() {
                    }.getType());
                    Iterator<Active> iterator = actives.iterator();
                    while (iterator.hasNext()) {
                        Active active = iterator.next();
                        PersistUtils.saveActive(active);
                    }
                    if (searchType == 0 && page == 0) {
                        LocalStorage.setInitiateActiveListRefreshTime(Long.parseLong(endDate));
                    } else if (searchType == 1 && page == 0) {
                        LocalStorage.setJoinActiveListRefreshTime(Long.parseLong(endDate));
                    }
                    callback.onMyActiveListFetched(actives);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onMyActiveListFetched(null);
            }
        });
    }

    /*查询发参与的话题*/

    public static void fetchMyTopicList(int size, int page, FetchMyTopicListCallback callback) {
        FetchMyTopicListRequest request = new FetchMyTopicListRequest();
        request.size = size;
        request.page = page;
        request.userId = PersistUtils.getUsers().get(0).getAccountId();
        RestClient.getRestService().fetchMyTopic(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Topic> topics = RestClient.getGson().fromJson(str, new TypeToken<List<Topic>>() {
                    }.getType());
                    Iterator<Topic> iterator = topics.iterator();
                    while (iterator.hasNext()) {
                        Topic topic = iterator.next();
                        PersistUtils.saveTopic(topic);
                    }
                    callback.onMyTopicListFetched(topics);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onMyTopicListFetched(null);
            }
        });

    }

    /*查询用户参与的互动数量*/
    public static void fetchInteractCount(long userId, final FetchInteractCountCallback callback) {
        RestClient.getRestService().fetchInteractCount(userId, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    InteractCount interactCount = RestClient.getGson().fromJson(str, InteractCount.class);
                    callback.onInteractCountFetched(interactCount);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onInteractCountFetched(null);
            }
        });
    }

    /*查询用户通知*/
    public static void fetchInform(int size, int page, final FetchInformListCallback callback) {
        FetchInformListRequest request = new FetchInformListRequest();
        request.page = page;
        request.size = size;
        request.userId = PersistUtils.getUsers().get(0).getAccountId();
        RestClient.getRestService().fetchInformList(request, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    List<Inform> informs = RestClient.getGson().fromJson(str, new TypeToken<List<Inform>>() {
                    }.getType());
                    callback.onInformListFetched(informs);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onInformListFetched(null);
            }
        });
    }

    /*根据动态ID查询单条动态的详情*/
    public static void fetchDynamic(long postMsgId, final FetchDynamicCallback callback) {
        RestClient.getRestService().fetchDynamic(postMsgId, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")) {
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    Dynamic dynamic = RestClient.getGson().fromJson(str, Dynamic.class);
                    callback.onDynamicFetched(dynamic);
                    PersistUtils.saveDynamic(dynamic);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onDynamicFetched(null);
            }
        });
    }

    /*根据话题ID查询单条话题的详情*/
    public static void fetchTopic(String topicId,final  FetchTopicCallback callback){
        RestClient.getRestService().fetchTopic(topicId, new Callback<RestResponse>() {
            @Override
            public void success(RestResponse restResponse, Response response) {
                if (restResponse.mErrorCode.equals("0")){
                    String str = RestClient.getGson().toJson(restResponse.mData);
                    Topic topic = RestClient.getGson().fromJson(str,Topic.class);
                    callback.onTopicFetched(topic);
                    PersistUtils.saveTopic(topic);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }
    public interface FetchActiveListCallback {
        void onActiveListFetched(List<Active> activeList);
    }

    public interface FetchDynamicListCallback {
        void onDynamicListFetched(List<Dynamic> dynamicList);
    }

    public interface FetchCommentListCallback {
        void onCommentListFetched(List<Comment> commentList);
    }

    public interface FetchLikeListCallback {
        void onLikeListFetched(List<Member> likeMemberList);
    }

    public interface ReleaseCommentCallback {
        void onCommentReleased(boolean b);
    }

    public interface ReleaseLikeCallback {
        void onLikeReleased(boolean b);
    }

    public interface ReleaseUnLikeCallback {
        void onUnLikeReleased(boolean b);
    }

    public interface FetchTopicListCallback {
        void onTopicListFetched(List<Topic> topicList);
    }

    public interface FetchMyActiveListCallback {
        void onMyActiveListFetched(List<Active> activeList);
    }

    public interface FetchMyTopicListCallback {
        void onMyTopicListFetched(List<Topic> topicList);
    }

    public interface FetchInteractCountCallback {
        void onInteractCountFetched(InteractCount interactCount);
    }

    public interface FetchInformListCallback {
        void onInformListFetched(List<Inform> informList);
    }

    public interface FetchDynamicCallback {
        void onDynamicFetched(Dynamic dynamic);
    }

    public interface FetchTopicCallback {
        void onTopicFetched(Topic topic);
    }
}
