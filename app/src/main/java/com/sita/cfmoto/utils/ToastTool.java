package com.sita.cfmoto.utils;

import android.content.Context;
import android.widget.Toast;

import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.support.GlobalContext;

import cn.trinea.android.common.util.ToastUtils;

/**
 * Created by mark man on 2016/7/5.
 */
public class ToastTool {
    public static void showDebugMessage(Context context, String message) {
        if (Constants.DEBUG) {
            ToastUtils.show(context == null ? GlobalContext.getGlobalContext() : context, message, Toast.LENGTH_LONG);
        }
    }

    public static void showDebugMessage(String message) {
        if (Constants.DEBUG) {
            ToastUtils.show(GlobalContext.getGlobalContext(), message, Toast.LENGTH_LONG);
        }
    }

//    public static void showMessage(Context context, String message) {
//        ToastUtils.show(context == null ? GlobalContext.getGlobalContext() : context, message, Toast.LENGTH_LONG);
//
//    }
}
