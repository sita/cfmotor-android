package com.sita.cfmoto.utils;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;

import com.sita.cfmoto.R;
import com.sita.cfmoto.support.GlobalContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by xiaodong on 16/2/17.
 */
public class DateUtils {
    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat YYYYMMDD_TIME = new SimpleDateFormat("yyyy年MM月dd日");
    public static final SimpleDateFormat YYYYMM = new SimpleDateFormat("yyyy年MM月");
    public static final SimpleDateFormat HHMMSS = new SimpleDateFormat("HH:mm:ss");

    public static String translateDateTime(Long time) {
        long oneDay = 24 * 60 * 60 * 1000;
        Calendar current = Calendar.getInstance();
        Calendar today = Calendar.getInstance();    //今天

        today.set(Calendar.YEAR, current.get(Calendar.YEAR));
        today.set(Calendar.MONTH, current.get(Calendar.MONTH));
        today.set(Calendar.DAY_OF_MONTH, current.get(Calendar.DAY_OF_MONTH));
        //  Calendar.HOUR——12小时制的小时数 Calendar.HOUR_OF_DAY——24小时制的小时数
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);

        long todayStartTime = today.getTimeInMillis();

        String prefix = "";
        if (android.text.format.DateUtils.isToday(time)) { // today
            long d = System.currentTimeMillis() - time;
            if (d <= 60) {
                return (String) GlobalContext.getGlobalContext().getText(R.string.just_before);
            }
            prefix = (String) GlobalContext.getGlobalContext().getText(R.string.today);
        } else if (time >= todayStartTime - oneDay && time < todayStartTime) { // yesterday
            prefix = (String) GlobalContext.getGlobalContext().getText(R.string.day_before);
        } else {
            long days = (time - todayStartTime) / oneDay;
            prefix = Math.abs(days) + (String) GlobalContext.getGlobalContext().getText(R.string.days_before);
        }
        String dateStr = TIME_FORMAT.format(new Date(time));
        if (!TextUtils.isEmpty(dateStr) && dateStr.contains(" 0")) {
            dateStr = dateStr.replace(" 0", " ");
        }
        return prefix + " " + dateStr;
    }

    public static String formatDuration(long duration) {
        Resources res = GlobalContext.getGlobalContext().getResources();
        long timePeriod = duration / 1000;
        long hours = timePeriod / 3600;
        long minutes = (timePeriod % 3600) / 60;
        long seconds = timePeriod % 60;
        if (seconds > 0) {
            minutes += 1;
        }
        return hours + res.getString(R.string.hours) + minutes + res.getString(R.string.minutes);
    }

    public static boolean isSameDay(long time1, long time2) {
        final Calendar firstCal = Calendar.getInstance();
        final Calendar secondCal = Calendar.getInstance();
        firstCal.setTimeInMillis(time1);
        secondCal.setTimeInMillis(time2);
        return (firstCal.get(Calendar.ERA) == secondCal.get(Calendar.ERA) &&
                firstCal.get(Calendar.YEAR) == secondCal.get(Calendar.YEAR) &&
                firstCal.get(Calendar.DAY_OF_YEAR) == secondCal.get(Calendar.DAY_OF_YEAR));
    }

    public static String formatDateTime(Date date) {
        if (isSameDay(date.getTime(), System.currentTimeMillis())) {
            return TIME_FORMAT.format(date);
        } else {
            return DATETIME_FORMAT.format(date);
        }
    }

    public static String formatDateTime(long time) {
        if (isSameDay(time, System.currentTimeMillis())) {
            return TIME_FORMAT.format(new Date(time));
        } else {
            return DATETIME_FORMAT.format(new Date(time));
        }
    }

    public static String formatDate(long time, SimpleDateFormat formatter) {
        return formatter.format(new Date(time));
    }

    private static String[] weeks = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};

    public static StringBuilder formatDayOfWeek(long time) {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(YYYYMMDD_TIME.format(new Date(time)));
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        return strBuilder.append(" ").append(weeks[week_index]);
    }

    private static String[] monthWeeks = {"第一周", "第二周", "第三周", "第四周"};

    public static StringBuilder formatWeekOfMonth(long time) {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(YYYYMM.format(new Date(time)));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int week_index = calendar.get(Calendar.WEEK_OF_MONTH);
        if (week_index < 0) {
            week_index = 0;
        } else if (week_index > 3) {
            week_index = 3;
        }
        return strBuilder.append(" ").append(monthWeeks[week_index]);
    }

    public static long convertDate(String date) {
        long result = 0;
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat((date.contains("-")) ? "yyyy-MM-dd" : "yyyy.MM.d");
        //将字符串形式的时间转化为Date类型的时间
        try {
            Date d = sdf.parse(date);
            result = d.getTime();

        } catch (ParseException exception) {
            Log.d("DateUtils", "Convert error");
        }

        return result;
    }

    public static long convertDateTime(String date) {
        long result = 0;
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat((date.contains("-")) ?
                "yyyy-MM-dd HH:mm:ss" : "yyyy.MM.dd HH:mm:ss");
        //将字符串形式的时间转化为Date类型的时间
        try {
            Date d = sdf.parse(date);
            result = d.getTime();

        } catch (ParseException exception) {
            Log.d("DateUtils", "Convert error");
        }

        return result;
    }

    public static String shortDate(String date) {
        if (date.contains(" ")) {
            StringBuilder sb = new StringBuilder();
            sb.append(date);
            return sb.substring(0, sb.indexOf(" "));
        }else {
            return date;
        }
    }
}
