package com.sita.cfmoto.utils;

import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.rest.model.TopicAuthor;

import java.util.List;

/**
 * Created by lijuan zhang on 2016/8/24.
 */
public class BeanUtils {
    public static Dynamic copyProperties(com.sita.cfmoto.persistence.Dynamic dynamicPer) {
        Dynamic dynamic = new Dynamic();
        dynamic.msgId = dynamicPer.getMsgId();
        dynamic.authorId = dynamicPer.getAuthorId();
        dynamic.content = dynamicPer.getContent();
        dynamic.lng = dynamicPer.getLng();
        dynamic.lat = dynamicPer.getLat();
        dynamic.address = dynamicPer.getAddress();
        dynamic.visible = dynamicPer.getVisible();
        dynamic.pic = dynamicPer.getPic();
        dynamic.video = dynamicPer.getVideo();
        dynamic.audio = dynamicPer.getAudio();
        dynamic.source = dynamicPer.getSource();
        dynamic.praiseCount = dynamicPer.getPraiseCount();
        dynamic.shareCount = dynamicPer.getShareCount();
        dynamic.commentCount = dynamicPer.getCommentCount();
        dynamic.refMsgId = dynamicPer.getRefMsgId();
        dynamic.replyMsgId = dynamicPer.getReplyMsgId();
        dynamic.campaignId = dynamicPer.getCampaignId();
        dynamic.topicId = dynamicPer.getTopicId();
        dynamic.statisDate = dynamicPer.getStatisDate();
        dynamic.isActivity = dynamicPer.getIsActivity();
        dynamic.activityUrl = dynamicPer.getActivityUrl();
        dynamic.doTime = dynamicPer.getDoTime();
        dynamic.createTime = dynamicPer.getCreateTime();
        dynamic.accountIdAvat = dynamicPer.getAccountIdAvat();
        dynamic.accountIdNickname = dynamicPer.getAccountIdNickname();
        dynamic.refMsgCounr = dynamicPer.getRefMsgCounr();
        String[] arrayStr = new String[]{};
        if (dynamicPer.getPicList() != null) {
            arrayStr = dynamicPer.getPicList().split(",");
            List list = java.util.Arrays.asList(arrayStr);
            dynamic.picList = list;
        }
        return dynamic;
    }

    public static com.sita.cfmoto.persistence.Dynamic copyProperties(Dynamic dynamic) {
        com.sita.cfmoto.persistence.Dynamic dynamicPer = new com.sita.cfmoto.persistence.Dynamic();
        dynamicPer.setAccountIdAvat(dynamic.accountIdAvat);
        dynamicPer.setAccountIdNickname(dynamic.accountIdNickname);
        dynamicPer.setActivityUrl(dynamic.activityUrl);
        dynamicPer.setAddress(dynamic.address);
        dynamicPer.setAudio(dynamic.audio);
        dynamicPer.setAuthorId(dynamic.authorId);
        dynamicPer.setIsActivity(dynamic.isActivity);
        dynamicPer.setCampaignId(dynamic.campaignId);
        dynamicPer.setCommentCount(dynamic.commentCount);
        dynamicPer.setContent(dynamic.content);
        dynamicPer.setCreateTime(dynamic.createTime);
        dynamicPer.setDoTime(dynamic.doTime);
        dynamicPer.setLat(dynamic.lat);
        dynamicPer.setLng(dynamic.lng);
        dynamicPer.setMsgId(dynamic.msgId);
        dynamicPer.setPic(dynamic.pic);
        List<String> picList = dynamic.picList;
        if (picList != null && !picList.isEmpty()) {
            StringBuilder picListStr = new StringBuilder();
            for (int k = 0; k < picList.size(); k++) {
                String picture = picList.get(k);
                if (k > 0) {
                    picListStr.append(",");
                }
                picListStr.append(picture);
            }
            dynamicPer.setPicList(picListStr.toString());
        }
        dynamicPer.setPraiseCount(dynamic.praiseCount);
        dynamicPer.setRefMsgCounr(dynamic.refMsgCounr);
        dynamicPer.setRefMsgId(dynamic.refMsgId);
        dynamicPer.setReplyMsgId(dynamic.replyMsgId);
        dynamicPer.setShareCount(dynamic.shareCount);
        dynamicPer.setSource(dynamic.source);
        dynamicPer.setStatisDate(dynamic.statisDate);
        dynamicPer.setTopicId(dynamic.topicId);
        dynamicPer.setVideo(dynamic.video);
        dynamicPer.setVisible(dynamic.visible);
        return dynamicPer;
    }

    public static Active copyProperties(com.sita.cfmoto.persistence.Active activePer) {
        Active active = new Active();
        active.arriAddr = activePer.getArriAddr();
        active.arriLat = activePer.getArriLat();
        active.arriLng = activePer.getArriLng();
        active.campaignEndTime = activePer.getCampaignEndTime();
        active.campaignId = activePer.getCampaignId();
        active.campaignStartTime = activePer.getCampaignStartTime();
        active.chatroomId = activePer.getChatroomId();
        active.content = activePer.getContent();
        active.cover = activePer.getCover();
        active.createTime = activePer.getCreateTime();
        active.deptAddr = activePer.getDeptAddr();
        active.deptLat = activePer.getDeptLat();
        active.deptLng = activePer.getDeptLng();
        active.maxMemberCount = activePer.getMaxMemberCount();
        active.members = activePer.getMembers();
        active.nickName = activePer.getNickName();
        active.registerEndTime = activePer.getRegisterEndTime();
        active.registerStartTime = activePer.getRegisterStartTime();
        active.source = activePer.getSource();
        active.state = activePer.getState();
        active.title = activePer.getTitle();
        active.userId = activePer.getUserId();
        return active;
    }

    public static com.sita.cfmoto.persistence.Active copyProperties(Active active) {
        com.sita.cfmoto.persistence.Active activePer = new com.sita.cfmoto.persistence.Active();
        activePer.setArriAddr(active.arriAddr);
        activePer.setArriLat(active.arriLat);
        activePer.setArriLng(active.arriLng);
        activePer.setCampaignEndTime(active.campaignEndTime);
        activePer.setCampaignId(active.campaignId);
        activePer.setCampaignStartTime(active.campaignStartTime);
        activePer.setChatroomId(active.chatroomId);
        activePer.setContent(active.content);
        activePer.setCover(active.cover);
        activePer.setCreateTime(active.createTime);
        activePer.setDeptAddr(active.deptAddr);
        activePer.setDeptLat(active.deptLat);
        activePer.setDeptLng(active.deptLng);
        activePer.setMaxMemberCount(active.maxMemberCount);
        activePer.setMembers(active.members);
        activePer.setNickName(active.nickName);
        activePer.setRegisterEndTime(active.registerEndTime);
        activePer.setRegisterStartTime(active.registerStartTime);
        activePer.setSource(active.source);
        activePer.setState(active.state);
        activePer.setTitle(active.title);
        activePer.setUserId(active.userId);
        return activePer;
    }

    public static Topic copyProperties(com.sita.cfmoto.persistence.Topic topicPer) {
        Topic topic = new Topic();
        topic.createTime = topicPer.getCreateTime();
        topic.content = topicPer.getContent();
        topic.headPic = topicPer.getHeadPic();
        topic.praiseCount = topicPer.getPraiseCount();
        topic.pic = topicPer.getPic();
        topic.readCounnt = topicPer.getReadCounnt();
        topic.shareCount = topicPer.getShareCount();
        topic.source = topicPer.getSource();
        topic.state = topicPer.getState();
        topic.statisDate = topicPer.getStatisDate();
        topic.subject = topicPer.getSubject();
        topic.topicId = topicPer.getTopicId();
        topic.type = topicPer.getType();
        topic.video = topicPer.getVideo();
        topic.author = copyProperties(topicPer.getTopicAuthor());
        return topic;
    }

    public static com.sita.cfmoto.persistence.Topic copyProperties(Topic topic) {
        com.sita.cfmoto.persistence.Topic topicPer = new com.sita.cfmoto.persistence.Topic();
        topicPer.setCreateTime(topic.createTime);
        topicPer.setContent(topic.content);
        topicPer.setHeadPic(topic.headPic);
        topicPer.setPraiseCount(topic.praiseCount);
        topicPer.setPic(topic.pic);
        topicPer.setReadCounnt(topic.readCounnt);
        topicPer.setShareCount(topic.shareCount);
        topicPer.setSource(topic.source);
        topicPer.setState(topic.state);
        topicPer.setStatisDate(topic.statisDate);
        topicPer.setSubject(topic.subject);
        topicPer.setTopicId(topic.topicId);
        topicPer.setType(topic.type);
        topicPer.setVideo(topic.video);
        topicPer.setUserId(topic.author.userId);
        topicPer.setTopicAuthor(copyProperties(topic.author));
        return topicPer;
    }

    public static TopicAuthor copyProperties(com.sita.cfmoto.persistence.TopicAuthor topicAuthorPer) {
        TopicAuthor topicAuthor = new TopicAuthor();
        topicAuthor.avatar = topicAuthorPer.getAvatar();
        topicAuthor.nickName = topicAuthorPer.getNickName();
        topicAuthor.userId = topicAuthorPer.getUserId();
        return topicAuthor;
    }

    public static com.sita.cfmoto.persistence.TopicAuthor copyProperties(TopicAuthor topicAuthor) {
        com.sita.cfmoto.persistence.TopicAuthor topicAuthorPer = new com.sita.cfmoto.persistence.TopicAuthor();
        topicAuthorPer.setAvatar(topicAuthor.avatar);
        topicAuthorPer.setNickName(topicAuthor.nickName);
        topicAuthorPer.setUserId(topicAuthor.userId);
        return topicAuthorPer;
    }
}
