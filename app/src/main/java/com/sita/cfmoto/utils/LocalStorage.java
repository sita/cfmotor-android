package com.sita.cfmoto.utils;

import android.content.SharedPreferences;

import com.sita.cfmoto.support.GlobalContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by mark on 2015/3/3.
 */
public class LocalStorage {
    private final static String USER_STATUS = "user_status";
    private final static String STATE_REFRESH_DATETIME = "state_refresh_datetime";
    private final static String UPDATE_GEO_DATETIME = "update_geo_datetime";
    private final static String FIRST_RUN_TAG = "first_run";
    private final static String LOGGED_IN = "logged_in";
    private final static String PUSH_TOKEN = "push_token";
    private final static String PUSH_DEVICE_ID = "push_device_id";
    private final static String SET_WAKEUP_FROM_BACKGROUND = "setWakeupFrom";
    private final static String AD_MODEL = "AD_MODEL";
    private final static String NO_ENABLE_BLUETOOTH_PROMPT = "no_enable_bluetooth_prompt";
    private final static String BLUETOOTH_ADDRESS = "bluetooth_address";
    private final static String RESOURCE_COUNT = "resource_count";
    private final static String CURR_SELECTED_VEHICLE = "curr_selected_vehicle";
    private final static String BATTERY_ALERT = "BATTERY_ALERT";
    private final static String NOTIFICATION_LAST_TIMESTAMP = "NOTIFICATION_LAST_TIMESTAMP";
    private static final long EPOCH_START = 1420070400000l; //2015-01-01
    private final static String HAS_BIND_VEHICLE = "has_bind_vehicle";
    private static final SharedPreferences localStorage = GlobalContext.getLocalStorage();

    public static boolean isFirstRun() {
        return localStorage.getBoolean(FIRST_RUN_TAG, true);
    }

    public static void setFirstRun(boolean firstRun) {
        localStorage.edit().putBoolean(FIRST_RUN_TAG, firstRun).commit();
    }

    public static boolean isLoggedIn() {
        return localStorage.getBoolean(LOGGED_IN, false);
    }

    public static void setLoggedIn(boolean isLogged) {
        localStorage.edit().putBoolean(LOGGED_IN, isLogged).apply();
    }

    public static boolean hasBindVehicle(){
        return localStorage.getBoolean(HAS_BIND_VEHICLE,false);
    }
    public static void setHasBindVehicle(boolean hasBindVehicle){
        localStorage.edit().putBoolean(HAS_BIND_VEHICLE,hasBindVehicle).apply();
    }

    public static void saveLastRefreshDatetime(long time) {
        localStorage.edit().putLong(STATE_REFRESH_DATETIME, time).apply();
    }

    public static long getLastRefreshDateTime() {
        return localStorage.getLong(STATE_REFRESH_DATETIME, 0);
    }

    public static void saveLastUpdateGeoDatetime(long time) {
        localStorage.edit().putLong(UPDATE_GEO_DATETIME, time).apply();
    }

    public static long getLastUpdateGeoDatetime() {
        return localStorage.getLong(UPDATE_GEO_DATETIME, 0);
    }

    public static void clearLocalStorage() {
        List<String> keys = new ArrayList<String>();
        Map map = localStorage.getAll();
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String key = (String) entry.getKey();
            if (key.equals(PUSH_TOKEN) || key.equals(PUSH_DEVICE_ID) || key.equals("account")) {
                continue;
            } else {
                keys.add(key);
            }
        }
        for (String key : keys) {
            localStorage.edit().remove(key).apply();
        }
    }

    public static String getPushToken() {
        return localStorage.getString(PUSH_TOKEN, "");
    }

    public static void setPushToken(String token) {
        localStorage.edit().putString(PUSH_TOKEN, token).apply();
    }

    public static String getPushDeviceId() {
        return localStorage.getString(PUSH_DEVICE_ID, "");
    }

    public static void setWakeupFromBackground(boolean setwakeup) {
        localStorage.edit().putBoolean(SET_WAKEUP_FROM_BACKGROUND, setwakeup).apply();
    }

    public static long getId() {
        return localStorage.getLong("imageId", 0);
    }

    public static void setId(long id) {
        localStorage.edit().putLong("imageId", id).apply();
    }

    public static long getCurrentTimeMillis() {
        return localStorage.getLong("currentTimeMillis", 0);
    }

    public static void setCurrentTimeMillis(long currentTimeMillis) {
        localStorage.edit().putLong("currentTimeMillis", currentTimeMillis).apply();
    }

    public static void clearAccountId() {
        localStorage.edit().remove("account").commit();
    }

    public static long getAccountId() {
        return localStorage.getLong("account", 0);
    }

    public static void setAccountId(long accountId) {
        localStorage.edit().putLong("account", accountId).commit();
    }

    public static void setEnableBluetoothPrompt(boolean value) {
        localStorage.edit().putBoolean(NO_ENABLE_BLUETOOTH_PROMPT, value).apply();
    }

    public static boolean getBluetoothPromptEnabled() {
        return localStorage.getBoolean(NO_ENABLE_BLUETOOTH_PROMPT, true);
    }

    public static String getBluetoothAddress() {
        return localStorage.getString(BLUETOOTH_ADDRESS, "");
    }

    public static void setBluetoothAddress(String address) {
        localStorage.edit().putString(BLUETOOTH_ADDRESS, address).apply();
    }

    public static long getDynamicListRefreshTime() {
        return localStorage.getLong("dynamic_list_refresh_time", EPOCH_START);
    }

    public static void setDynamicListRefreshTime(long time) {
        localStorage.edit().putLong("dynamic_list_refresh_time", time).apply();
    }

    public static long getTopicDynamicListRefreshTime() {
        return localStorage.getLong("topic_dynamic_list_refresh_time", EPOCH_START);
    }

    public static void setTopicDynamicListRefreshTime(long time) {
        localStorage.edit().putLong("topic_dynamic_list_refresh_time", time).apply();
    }

    public static long getActiveListRefreshTime() {
        return localStorage.getLong("active_list_refresh_time", EPOCH_START);
    }

    public static void setActiveListRefreshTime(long time) {
        localStorage.edit().putLong("active_list_refresh_time", time).apply();
    }

    public static long getInitiateActiveListRefreshTime() {
        return localStorage.getLong("initiate_active_list_refresh_time", EPOCH_START);
    }

    public static void setInitiateActiveListRefreshTime(long time) {
        localStorage.edit().putLong("initiate_active_list_refresh_time", time).apply();
    }

    public static long getJoinActiveListRefreshTime() {
        return localStorage.getLong("join_active_list_refresh_time", EPOCH_START);
    }

    public static void setJoinActiveListRefreshTime(long time) {
        localStorage.edit().putLong("join_active_list_refresh_time", time).apply();
    }

    public static long getTopicListRefreshTime() {
        return localStorage.getLong("topic_list_refresh_time", EPOCH_START);
    }

    public static void setTopicListRefreshTime(long time) {
        localStorage.edit().putLong("topic_list_refresh_time", time).apply();
    }

    public static long getResourceCount() {
        return localStorage.getLong(RESOURCE_COUNT, 0);
    }

    public static void setResourceCount(long count) {
        localStorage.edit().putLong(RESOURCE_COUNT, count).apply();
    }

    public static long getCheckUpdateTime() {
        return localStorage.getLong("check_update_time", EPOCH_START);
    }

    public static void updateCheckUpdateTime(long checkUpdateTime) {
        localStorage.edit().putLong("check_update_time", checkUpdateTime).apply();
    }

    public static void updateUserStatus(String status) {
        localStorage.edit().putString(USER_STATUS, status).apply();
    }

    public static String getUserStatus() {
        return localStorage.getString(USER_STATUS, "");
    }

    public static String getCurrSelectedVehicle() {
        return localStorage.getString(CURR_SELECTED_VEHICLE, "");
    }

    public static void setCurrSelectedVehicle(String id) {
        localStorage.edit().putString(CURR_SELECTED_VEHICLE, id).commit();
    }

    public static int getBatteryAlertLevel() {
        return localStorage.getInt(BATTERY_ALERT, 10);
    }

    public static void setBatteryAlertLevel(int battery) {
        localStorage.edit().putInt(BATTERY_ALERT, battery).commit();
    }

    public static long getAlarmTimeStamp() {
        return localStorage.getLong("battery_alarm_time_stamp", 0);
    }

    public static void setAlarmTimeStamp(long time) {
        localStorage.edit().putLong("battery_alarm_time_stamp", time).commit();
    }

    public static long getLastNotificationTimeStamp() {
        return localStorage.getLong("NOTIFICATION_LAST_TIMESTAMP", 0);
    }

    public static void setLastNotificationTimeStamp(long time) {
        localStorage.edit().putLong(NOTIFICATION_LAST_TIMESTAMP, time).commit();
    }
}
