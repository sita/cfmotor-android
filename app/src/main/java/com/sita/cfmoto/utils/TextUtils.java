package com.sita.cfmoto.utils;

import android.support.annotation.Nullable;
import android.widget.EditText;

/**
 * Created by xiaodong on 16/3/4.
 */
public class TextUtils {
    public static String nullIfEmpty(@Nullable String str) {
        return android.text.TextUtils.isEmpty(str) ? null : str;
    }

    /**
     * 判断edittext是否null
     */
    public static String checkEditText(EditText editText) {
        if (editText != null && editText.getText() != null
                && !(editText.getText().toString().trim().equals(""))) {
            return editText.getText().toString().trim();
        } else {
            return "";
        }
    }

    public static String getThrowableMessage(Throwable throwable) {
        String errorMsg = throwable.getMessage() != null ? throwable.getMessage() : throwable.getClass().getSimpleName();
        return errorMsg;
    }
}
