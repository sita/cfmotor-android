package com.sita.cfmoto.utils;

/**
 * Class com.sita.bike.utils is for
 *
 * @author xiaodong on 15/3/16.
 */
public class SessionUtils {

    public static boolean isLoggedIn() {
        return LocalStorage.isLoggedIn();
    }

    public static void logout() {
        LocalStorage.setLoggedIn(false);
        VehicleUtils.resetVehicleBean();
    }

    public static void login() {
        LocalStorage.setLoggedIn(true);
    }

}
