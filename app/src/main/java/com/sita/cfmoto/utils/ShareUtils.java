package com.sita.cfmoto.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.*;
import android.widget.Toast;

import com.sita.cfmoto.interact.activity.ActiveDetailActivity;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.support.GlobalContext;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
//import com.umeng.socialize.bean.SocializeEntity;
//import com.umeng.socialize.controller.UMServiceFactory;
//import com.umeng.socialize.controller.UMSocialService;
//import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;
//import com.umeng.socialize.weixin.controller.UMWXHandler;

/**
 * Created by hongyun on 2016/3/28.
 */
public class ShareUtils {

    private static String mResrouceId;

    public static void doSharePost(String postid, byte[] img, String title, String content, Context context, Activity activity) {
        String url = "/cfmotor/app/postmsg.html?postmsgId=" + postid;
        doShare(title, url, img, null, null, content, context, activity);
        mResrouceId = postid;
    }

    public static void doSharePost(String postid, String img_url, String title, String content, Context context, Activity activity) {
        String url = "/cfmotor/app/postmsg.html?postmsgId=" + postid;
        doShare(title, url, null, null, img_url, content, context, activity);
        mResrouceId = postid;
    }

    public static void doSharePost(String postid, Bitmap img, String title, String content, Context context, Activity activity) {
        String url = "/cfmotor/app/postmsg.html?postmsgId=" + postid;
        doShare(title, url, null, img, null, content, context, activity);
        mResrouceId = postid;
    }

    private static void doShare(final String title, final String url,
                                final byte[] imgbyte, final Bitmap img, final String img_url,
                                final String content, final Context context, Activity activity) {
        final String baseUrl = Constants.BASE_URI;
        final String targetUrl = baseUrl + url;

        UMImage image;
        if (img != null) {
            image = new UMImage(activity, img);
        } else if (imgbyte != null) {
            image = new UMImage(activity, imgbyte);
        } else {
            image = new UMImage(activity, img_url);
        }

        final SHARE_MEDIA[] list = new SHARE_MEDIA[]
                {
                        SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE
                };
        ShareAction action = new ShareAction(activity);
        action.setDisplayList(list);
        action.setShareboardclickCallback(new ShareBoardlistener() {
            @Override
            public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
                switch (share_media) {
                    case WEIXIN:
                        new ShareAction(activity).setPlatform(SHARE_MEDIA.WEIXIN).setCallback(umShareListener)
                                .withTitle(title)
                                .withTargetUrl(targetUrl)
                                .withMedia(image)
                                .withText(content)
                                .share();
                        break;
                    case WEIXIN_CIRCLE:
                        new ShareAction(activity).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).setCallback(umShareListener)
                                .withTitle(title)
                                .withTargetUrl(targetUrl)
                                .withText(content)
                                .withMedia(image)
                                .share();
                        break;
                }
            }
        });//设置友盟集成的分享面板的点击监听回调
        action.open();//打开集成的分享面板
    }

    private static UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
//            Toast.makeText(GlobalContext.getGlobalContext(), platform + " 分享成功啦", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
//            Toast.makeText(GlobalContext.getGlobalContext(), platform + " 分享失败啦", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
//            Toast.makeText(GlobalContext.getGlobalContext(), platform + " 分享取消了", Toast.LENGTH_SHORT).show();
        }
    };
}
