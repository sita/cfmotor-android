package com.sita.cfmoto.utils;

import android.content.Context;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.PolylineOptions;
import com.sita.cfmoto.R;
import com.sita.cfmoto.persistence.Route;
import com.sita.cfmoto.persistence.RouteData;
import com.sita.cfmoto.support.GlobalContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by hongyun on 2016/3/1.
 */
public class MapUtils {
    private static int trackColor = GlobalContext.getGlobalContext().getResources().getColor(R.color.app_third_color);

    public static void displayTrack(long trackId, final AMap aMap) {
        aMap.clear();
        final List<LatLng> latLngList = displaySingleTrack(trackId, aMap);
        aMap.setOnMapLoadedListener(new AMap.OnMapLoadedListener() {
            @Override
            public void onMapLoaded() {
                moveBound(latLngList, aMap);
            }
        });

    }

    //画多条路线在地图上
    public static void displayMultiTracks(List<Route> dayTracks, final AMap aMap) {
        aMap.clear();
        final List<LatLng> latLngTotalList = new ArrayList<>();
        Iterator iterator = dayTracks.iterator();
        while (iterator.hasNext()) {
            Route track = (Route) iterator.next();
            List<LatLng> latLngList = displaySingleTrack(track.getId(), aMap);
            latLngTotalList.addAll(latLngList);
        }
        aMap.setOnMapLoadedListener(new AMap.OnMapLoadedListener() {
            @Override
            public void onMapLoaded() {
                moveBound(latLngTotalList, aMap);
            }
        });
    }

    private static List<LatLng> displaySingleTrack(long trackId, final AMap aMap) {
        List<RouteData> gpsLocationList = PersistUtils.getRouteDataList(trackId);
        final List<LatLng> latLngList = new ArrayList<>();

        for (RouteData g : gpsLocationList) {
            latLngList.add(new LatLng(Double.valueOf(g.getLatitude()), Double.valueOf(g.getLongitude())));
        }
        if (latLngList.size() != 0) {
            LatLng startLatLng = latLngList.get(0);
            LatLng endLatLng = latLngList.get(latLngList.size() - 1);
            drawSrartEndMaker(startLatLng, endLatLng, aMap);
            LogUtils.d("绘制单条路线","起点："+latLngList.get(0)+"终点："+latLngList.get(latLngList.size()-1)+"个数："+latLngList.size());
        }
        aMap.addPolyline(new PolylineOptions().width(10).color(trackColor).addAll(latLngList));
        return latLngList;
    }

    public static void drawSrartEndMaker(LatLng startLatLng, LatLng endLatLng, AMap aMap) {
        BitmapDescriptor icona = BitmapDescriptorFactory.fromResource(R.drawable.location_a);
        BitmapDescriptor iconb = BitmapDescriptorFactory.fromResource(R.drawable.location_b);
        aMap.addMarker(new MarkerOptions().draggable(false).icon(icona).position(startLatLng));
        aMap.addMarker(new MarkerOptions().draggable(false).icon(iconb).position(endLatLng));
    }

    public static void moveBound(List<LatLng> latLngList, AMap aMap) {
        if (!latLngList.isEmpty()) {
            LatLngBounds bounds = aMap.getProjection().getVisibleRegion().latLngBounds;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            boolean changeBounds = false;

            for (LatLng latLng : latLngList) {
                if (!bounds.contains(latLng)) {
                    builder.include(latLng);
                    changeBounds = true;
                }
            }
            if (changeBounds) {
                aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 20));
//                float zoomLevel = aMap.getCameraPosition().zoom - 1;
//                aMap.moveCamera(CameraUpdateFactory.zoomTo(zoomLevel));
            }
        }
    }

    public static void drawLine(LatLng start, LatLng end, AMap aMap, Context context) {
        aMap.addPolyline((new PolylineOptions().width(10))
                .add(start, end)
                .width(10)
                .color(trackColor));
    }
}
