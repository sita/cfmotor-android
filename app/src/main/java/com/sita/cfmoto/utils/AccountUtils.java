package com.sita.cfmoto.utils;

import android.text.TextUtils;

import com.sita.cfmoto.beans.PersonBean;
import com.sita.cfmoto.persistence.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hongyun on 2016/1/12.
 */
public class AccountUtils {

    // Session saved data, cached in memory
    private static AccountUtils instance = null;
    private User mUser;
    private Map<String, PersonBean> mUsers = new HashMap<>();

    private AccountUtils() {
        mUser = PersistUtils.getCurrentUser();
    }

    public static AccountUtils getInstance() {
        if (instance == null) {
            instance = new AccountUtils();
        }
        return instance;
    }

    public PersonBean getUser(String accountId) {
        return mUsers.get(accountId);
    }

    public String getUserNickName(String accountId) {
        PersonBean person = mUsers.get(accountId);
        if (person != null) {
            return (TextUtils.isEmpty(person.user.getNickName())) ? String.valueOf(person.user.getId()) : person.user.getNickName();
        }
        return "";
    }

    public void addUser(User user) {
        PersonBean person = new PersonBean();
        person.user = user;
        mUsers.put(String.valueOf(user.getId()), person);
    }

    // Personal recording info;
//    private static long mRank;
//    private static Double mMileage;
//    private static Long mTotalTime;

//    public static Double getMileage() {
//        return mMileage;
//    }
//
//    public static Long getTotalTime() {
//        return mTotalTime;
//    }
//
//    public static long getRank() {
//        return mRank;
//    }

//    public static void getMileageInfoAsync() {
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("accountId", mAccountId);
//        RestClient.getRestNormalService().mileageSummaryForAccount(params, new Callback<MileageSummaryResponse>() {
//            @Override
//            public void success(MileageSummaryResponse mileageSummaryResponse, Response response) {
//                if (mileageSummaryResponse.errorCode.equals("0")) {
//                    mMileage = mileageSummaryResponse.data.mileage;
//                    mTotalTime = mileageSummaryResponse.data.period;
//                    PersonalInfoUpdateEvent event = new PersonalInfoUpdateEvent();
//                    event.mMileage = mMileage;
//                    event.mTotalTime = mTotalTime;
//                    EventBus.getDefault().post(event);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                L.e("getMileageInfoAsync()", error);
//            }
//        });
//
//    }


//    public static void getRankingInfoAsync(final PersonalInfoListener listener) {
//        if (TextUtils.isEmpty(mAccountId)) {
//            mAccountId = LocalStorage.getAccountId();
//        }
//        RestClient.getRestNormalService().mileageSummaryRanking(mAccountId, new Callback<MileageSummaryRankingResponse>() {
//            @Override
//            public void success(MileageSummaryRankingResponse cntResponse, Response response) {
//                if (cntResponse.errorCode.equals("0")) {
//                    mRank = cntResponse.data.ranking;
//                    PersonalInfoUpdateEvent event = new PersonalInfoUpdateEvent();
//                    event.mRank = mRank;
//                    EventBus.getDefault().post(event);
//                    PersonalInfo personalInfo = new PersonalInfo();
//                    personalInfo.setRank(mRank);
//                    listener.Success(personalInfo);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                L.e("getRankingInfoAsync()", error);
//            }
//        });
//    }

//    public static void getTrackInfoAsync(final PersonalInfoListener listener) {
//        if (TextUtils.isEmpty(mAccountId)) {
//            mAccountId = LocalStorage.getAccountId();
//        }
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("accountId", mAccountId);
//        RestClient.getRestNormalService().mileageSummaryForAccount(params, new Callback<MileageSummaryResponse>() {
//            @Override
//            public void success(MileageSummaryResponse mileageResponse, Response response) {
//                if (mileageResponse.errorCode.equals("0") && mileageResponse.data != null) {
//                    Double mileage = mileageResponse.data.mileage;
//                    Long period = mileageResponse.data.period;
//                    if (mileage == null) {
//                        mileage = new Double(0f);
//                    }
//                    if (period == null) {
//                        period = new Long(0);
//                    }
//                    PersonalInfo personalInfo = new PersonalInfo();
//                    personalInfo.setTotal(mileage.doubleValue());
//                    personalInfo.setTime(period.longValue());
//                    listener.Success(personalInfo);
//                } else if (mileageResponse.errorCode.equals("0")) {
//                    PersonalInfo personalInfo = new PersonalInfo();
//                    personalInfo.setTotal(0f);
//                    personalInfo.setTime(0);
//                    listener.Success(personalInfo);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                com.sita.tboard.ui.tools.L.e("getTrackInfoAsync()", error);
//            }
//        });
//    }

//    public static void getTrendCountAsync(final PersonalInfoListener listener) {
//        if (TextUtils.isEmpty(mAccountId)) {
//            mAccountId = LocalStorage.getAccountId();
//        }
//        GetRtResourceCountRequest cntReq = new GetRtResourceCountRequest();
//        cntReq.accountId = mAccountId;
//        RestClient.getRestNormalService().rtResourcesCount(cntReq, new Callback<RtResourcesCountResponse>() {
//            @Override
//            public void success(RtResourcesCountResponse cntResponse, Response response) {
//                if (cntResponse.errorCode.equals("0")) {
//                    long count = 0;
//                    if (cntResponse.data != null) {
//                        count = cntResponse.data.resourcesCount;
//                    }
//                    PersonalInfo personalInfo = new PersonalInfo();
//                    personalInfo.setTrendCount(count);
//                    listener.Success(personalInfo);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                com.sita.tboard.ui.tools.L.e("getTrendCountAsync()", error);
//            }
//        });
//    }

    public void resetAccountCacheInfo() {
        if (mUser != null) {
            mUser = null;
        }
    }

    public String getAccountID() {
        return String.valueOf(mUser.getAccountId());
    }

    public String getAvatar() {
        return mUser.getAvatar();
    }

    public String getNickName() {

        return (TextUtils.isEmpty(mUser.getNickName())) ? String.valueOf(mUser.getId()) : mUser.getNickName();

    }

}
