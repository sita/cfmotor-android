package com.sita.cfmoto.utils;

import android.graphics.Paint;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by hongyun on 2015/12/2.
 */
public class FormatUtils {

    private static Random mRandom = new Random();

    public static String formatCurrentTime() {
        long current = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        Date resultdate = new Date(current);
        return sdf.format(resultdate);
    }

    public static String formatShortCurrentTime() {
        long current = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date resultdate = new Date(current);
        return sdf.format(resultdate);
    }

    public static String formatShortTimeHHMM(long time) {
        String result = formatShortTime(time);
        result = result.substring(0, result.lastIndexOf(":"));
        return result;
    }

    public static String formatShortTime(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        sdf.setTimeZone(new TimeZone() {
            @Override
            public int getOffset(int era, int year, int month, int day, int dayOfWeek, int timeOfDayMillis) {
                return 0;
            }

            @Override
            public int getRawOffset() {
                return 0;
            }

            @Override
            public void setRawOffset(int offsetMillis) {

            }

            @Override
            public boolean inDaylightTime(Date time) {
                return false;
            }

            @Override
            public boolean useDaylightTime() {
                return false;
            }
        });
        Date resultdate = new Date(time);
        return sdf.format(resultdate);
    }

    public static StringBuilder formatTime(int seconds) {
        StringBuilder strBuilder = new StringBuilder();
        int h = seconds / 60 / 60;
        int m = (seconds - h * 60 * 60) / 60;
        int s = seconds - h * 60 * 60 - m * 60;
        strBuilder.append(_formatDigits(h));
        strBuilder.append(":");
        strBuilder.append(_formatDigits(m));
        strBuilder.append(":");
        strBuilder.append(_formatDigits(s));
        return strBuilder;
    }

    private static String _formatDigits(int i) {
        String result = "";
        if (i == 0) {
            result += "00";
        } else if (i < 10) {
            result += "0" + String.valueOf(i);
        } else {
            result += String.valueOf(i);
        }
        return result;

    }

    public static String formatDistance(float dist) {
        String result = "";
        long dkm = (long) dist / 1000;
        if (dkm == 0) {
            result = String.valueOf((int) dist) + "m";
        } else {
            result = String.format("%.2f", dist) + "km";
        }
        return result;
    }

    public static String formatDistance2(float dist) {
        float dist_km = dist / 1000;
        String dist_result = "0";
        DecimalFormat decimalFormat;
        if (dist_km < 10) {
            decimalFormat = new DecimalFormat("0.00");
            dist_result = decimalFormat.format(dist_km);
        } else if (dist_km >= 10 && dist_km < 100) {
            decimalFormat = new DecimalFormat("0.0");
            dist_result = decimalFormat.format(dist_km);
        } else if (dist_km >= 100) {
            decimalFormat = new DecimalFormat("0");
            dist_result = decimalFormat.format(dist_km);
        }
        return dist_result;
    }

    // v is m/s
    public static String formatSpeedString(float v) {
        String result = "";
        if (Float.compare(v, 0f) == 0) {
            result = "0";
        } else {
            DecimalFormat decimalFormat = new DecimalFormat("0");
            // change to KM/H
            result = decimalFormat.format(convertSpeedToKMH(v));
        }

        return result;
    }

    public static int convertSpeedToKMH(float v) {
        return (int) (v * 3600f / 1000f);
    }

    public static float convertSpeedToMS(float v) {
        return v * 1000f / 3600f;
    }

    public static int produceRandomSpeed(int max) {
        return mRandom.nextInt(max);
    }

    public static int getTextWidth(Paint paint, String str) {
        int iRet = 0;
        if (str != null && str.length() > 0) {
            int len = str.length();
            float[] widths = new float[len];
            paint.getTextWidths(str, widths);
            for (int j = 0; j < len; j++) {
                iRet += (int) Math.ceil(widths[j]);
            }
        }
        return iRet;
    }

    public static float calculateDistance(LatLng start, LatLng end) {
        return AMapUtils.calculateLineDistance(start, end);

    }

    public static double doubleDistance1(Double dou) {
        DecimalFormat nf = new DecimalFormat("0.0");
        dou = Double.parseDouble(nf.format(dou));
        return dou;
    }

    public static String formatShortTimeMMSS(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
        Date resultdate = new Date(time);
        String result = sdf.format(resultdate);
        return result;
    }

    //将double类型的数据保留两位小数且第三位四舍五入。
    public static Double formatMileages(double mileage) {
        BigDecimal decimal = new BigDecimal(mileage);
        double formatMileage = decimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        return formatMileage;
    }

    public static int formatSpeed(float speed) {
        return (int) (speed + 0.5f);
    }

    public static TimeHourMinute formatTimeHourMinute(int minutes) {
        TimeHourMinute time = new TimeHourMinute();
        time.hour = minutes / 60;
        time.minute = minutes - time.hour * 60;
        return time;
    }

    public static String formatTimeHour(int seconds) {
        String str;
        int hour = seconds /60/ 60;
        double minute = (seconds/60 - hour * 60) / 60.0;
        DecimalFormat df = new DecimalFormat("0.0");
        str = df.format(hour + minute);
        return str;
    }

    public static class TimeHourMinute {
        public int hour;
        public int minute;
    }
}
