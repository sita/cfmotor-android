package com.sita.cfmoto.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.sita.cfmoto.support.GlobalContext;

/**
 * Created by hongyun on 2016/3/14.
 */
public class VersionUtils {

    /**
     * 返回当前程序版本名
     */
    public static String getAppVersionName(Context context) {
        String versionName = "";
        String versionCode = "";
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionName;
            versionCode = String.valueOf(pi.versionCode);
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
            LogUtils.e("VersionInfo", "Exception");
        }
        return (isDebuggable()? "测试版" : "正式版:") + versionName + (isDebuggable() ? (", (" + versionCode + ")") : "");
    }

    public static StringBuilder getSimpleAppVersionName(Context context) {
        StringBuilder versionName = new StringBuilder();
        versionName.append("v");
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName.append(pi.versionName);
        } catch (Exception e) {
            LogUtils.e("VersionInfo", "Exception");
        }
        return versionName;
    }

    /**
     * 手机系统版本
     */
    public static String getSdkVersion() {
        return android.os.Build.VERSION.RELEASE;
    }


    public static boolean isDebuggable() {
        boolean debuggable = false;

        Context ctx = GlobalContext.getGlobalContext();
        PackageManager pm = ctx.getPackageManager();
        try {
            ApplicationInfo appinfo = pm.getApplicationInfo(ctx.getPackageName(), 0);
            debuggable = (0 != (appinfo.flags & ApplicationInfo.FLAG_DEBUGGABLE));
        } catch (PackageManager.NameNotFoundException e) {
        /*debuggable variable will remain false*/
        }

        return debuggable;
    }
}
