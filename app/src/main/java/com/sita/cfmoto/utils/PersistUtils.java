package com.sita.cfmoto.utils;

import com.sita.cfmoto.beans.VehicleBean;
import com.sita.cfmoto.persistence.ActiveDao;
import com.sita.cfmoto.persistence.BindedVehicleDao;
import com.sita.cfmoto.persistence.DynamicDao;
import com.sita.cfmoto.persistence.GroupChatDetailEvent;
import com.sita.cfmoto.persistence.GroupChatDetailEventDao;
import com.sita.cfmoto.persistence.Route;
import com.sita.cfmoto.persistence.RouteDao;
import com.sita.cfmoto.persistence.RouteData;
import com.sita.cfmoto.persistence.RouteDataDao;
import com.sita.cfmoto.persistence.RtBlacklistEntity;
import com.sita.cfmoto.persistence.RtBlacklistEntityDao;
import com.sita.cfmoto.persistence.RtResourceEntityDao;
import com.sita.cfmoto.persistence.TopicAuthorDao;
import com.sita.cfmoto.persistence.TopicDao;
import com.sita.cfmoto.persistence.User;
import com.sita.cfmoto.persistence.UserDao;
import com.sita.cfmoto.persistence.VehicleInfo;
import com.sita.cfmoto.persistence.VehicleInfoDao;
import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.rest.model.BindedVehicle;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.rest.model.TopicAuthor;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.support.GlobalContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.greenrobot.dao.DaoException;
import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition;

/**
 * Class PersistUtils is for
 *
 * @author xiaodong on 15/3/1.
 */
public class PersistUtils {
    public static List<User> getUsers() {
        UserDao userDao = GlobalContext.getDaoSession().getUserDao();
        long currUserId = LocalStorage.getAccountId();
        if (currUserId == 0) {
            return userDao.loadAll();
        } else {
            long userId = Long.valueOf(currUserId);
            return userDao.queryBuilder().where(UserDao.Properties.AccountId.eq(userId)).build().list();
        }
    }

    public static void saveUser(User user) {
        UserDao userDao = GlobalContext.getDaoSession().getUserDao();
        long exists = userDao.queryBuilder().where(UserDao.Properties.AccountId.eq(user.getAccountId()))
                .buildCount().count();
        if (exists == 0) {
            userDao.insertOrReplace(user);
        } else {
            updatePerson(user);
        }
        LocalStorage.setAccountId(user.getAccountId());
    }

    public static void updatePerson(User user) {
        UserDao userDao = GlobalContext.getDaoSession().getUserDao();
        List<User> list = userDao.queryBuilder().where(UserDao.Properties.AccountId.eq(user.getAccountId())).list();
        if (list.size() > 0) {
            User oldUser = list.get(0);
            user.setId(oldUser.getId());
            userDao.update(user);
        }

    }

    public static void deleteCurrentUser() {
        long userId = LocalStorage.getAccountId();
        UserDao userDao = GlobalContext.getDaoSession().getUserDao();
        userDao.queryBuilder().where(UserDao.Properties.AccountId.eq(userId)).buildDelete()
                .executeDeleteWithoutDetachingEntities();
        LocalStorage.clearAccountId();
    }

    public static User getCurrentUser() {
        long userId = LocalStorage.getAccountId();
        UserDao userDao = GlobalContext.getDaoSession().getUserDao();
        List<User> users = userDao.queryBuilder().where(UserDao.Properties.AccountId.eq(userId)).list();
        if (users.size() > 0) {
            return users.get(0);
        } else {
            return null;
        }
    }

    public static void clearDatabase() {
        try {
            deleteCurrentUser();

            RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
            RouteDataDao routeDataDao = GlobalContext.getDaoSession().getRouteDataDao();

            routeDataDao.deleteAll();
            routeDao.deleteAll();
        } catch (Exception e) {
            L.e(e);
        }
    }

    public static void deleteRouteData(long routeId) {
        RouteDataDao routeDataDao = GlobalContext.getDaoSession().getRouteDataDao();
        List<RouteData> routeDataList = getRouteDataList(routeId);
        routeDataDao.deleteInTx(routeDataList);
    }

    public static void deleteRoute(long routeId) {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        routeDao.deleteByKey(routeId);
    }

    public static long saveRoute(Route route) {
        QueryBuilder builder = GlobalContext.getDaoSession().queryBuilder(com.sita.cfmoto.persistence.Route.class);
        builder.where(RouteDao.Properties.ServerRouteId.eq(route.getServerRouteId()));
        builder.limit(1);//取前n条数据
        List list = builder.list();
        if (list.size()!=0){
            Route rou = (Route) list.get(0);
            route.setId(rou.getId());
        }
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        long id = routeDao.insertOrReplace(route);
//        route.setId(id);
        return id;
    }

    public static void updateRoute(Route route) {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        routeDao.update(route);
    }

    public static boolean findRoute() {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        List<Route> routes = routeDao.loadAll();
        return routes.size() != 0;
    }

    public static Route getRoute(long routeId) {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        List<Route> routeList = routeDao.queryBuilder().where(RouteDao.Properties.Id.eq(routeId)).build().list();
        return routeList.size() > 0 ? routeList.get(0) : null;
    }

    public static Route getRouteByServerId(String routeServerId) {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        List<Route> routeList = routeDao.queryBuilder().where(RouteDao.Properties.ServerRouteId.eq(routeServerId)).build().list();
        return routeList.size() > 0 ? routeList.get(0) : null;
    }

    public static List<Route> getRoutesNotUploaded() {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        List<Route> routeList = routeDao.queryBuilder().where(RouteDao.Properties.SyncStatus.eq(Constants.ROUTE_FILE_SYNC_STATUS_UPLOAD_NOT)).build().list();
        return routeList;
    }

    public static List<Route> getAllRoutes() {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        return routeDao.loadAll();
    }

    public static List<Route> getTimeSelectRoutes(long start, long end) {
        RouteDao routeDao = GlobalContext.getDaoSession().getRouteDao();
        List<Route> routeList = routeDao.queryBuilder().where(RouteDao.Properties.StartTime.ge(start), RouteDao.Properties.StartTime.lt(end)).build().list();
        return routeList;
    }

    public static List<RouteData> getRouteDataList(long routeId) {
        RouteDataDao routeDataDao = GlobalContext.getDaoSession().getRouteDataDao();
        List<RouteData> routeDataList = routeDataDao.queryBuilder().where(RouteDataDao.Properties.RouteId.eq(routeId)).build().list();
        return routeDataList;
    }

    public static void saveRouteData(RouteData routeData) {
        RouteDataDao routeDataDao = GlobalContext.getDaoSession().getRouteDataDao();
        routeDataDao.insertOrReplace(routeData);
    }

    /**
     * 根据环信群聊ID获取服务器群聊ID
     */
    public static String getServiceGroupId(String groupId) {
        String serviceGroupId = null;
        GroupChatDetailEventDao dao = GlobalContext.getDaoSession().getGroupChatDetailEventDao();
        List<GroupChatDetailEvent> groupChatDetailEvents = dao.queryBuilder().where(GroupChatDetailEventDao.Properties.Groupid.eq(groupId)).build().list();
        Iterator<GroupChatDetailEvent> iterator = groupChatDetailEvents.iterator();
        while (iterator.hasNext()) {
            serviceGroupId = iterator.next().getTargetid();
        }
        return serviceGroupId;
    }

    public static void deleteRtResource(String resourceId) {
        RtResourceEntityDao resourceDao = GlobalContext.getDaoSession().getRtResourceEntityDao();
        resourceDao.deleteByKey(Long.valueOf(resourceId));
    }

    public static void saveRtBlacklist(List<RtBlacklistEntity> entities) {
        RtBlacklistEntityDao entityDao = GlobalContext.getDaoSession().getRtBlacklistEntityDao();
        entityDao.insertOrReplaceInTx(entities);
    }

    public static void saveUserStatus(long account, String status) {
        // TODO: 2016/5/20 should use database for different user
        LocalStorage.updateUserStatus(status);
    }

    public static String getUserStatus(long account) {
        // TODO: 2016/5/20 should use database for different user
        return LocalStorage.getUserStatus();
    }

    public static void saveVehicleBean(VehicleBean vehicleBean) {
        // TODO: 2016/5/20  save it
        VehicleInfo vehicleInfo = new VehicleInfo();
        vehicleInfo.setId(vehicleBean.id);
        vehicleInfo.setVin(vehicleBean.vin);
        vehicleInfo.setVinpwd(vehicleBean.vinpwd);
        vehicleInfo.setAccountId(vehicleBean.accountId);
        vehicleInfo.setActivated(vehicleBean.activated);
        VehicleInfoDao vehicleInfoDao = GlobalContext.getDaoSession().getVehicleInfoDao();
        vehicleBean.id = vehicleInfoDao.insertOrReplace(vehicleInfo);
    }

    public static void updateVehicleBean(VehicleBean vehicleBean) {
        if (vehicleBean.id != null) {
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.setId(vehicleBean.id);
            vehicleInfo.setVin(vehicleBean.vin);
            vehicleInfo.setVinpwd(vehicleBean.vinpwd);
            vehicleInfo.setBleAddr(vehicleBean.controllerAddr);
            vehicleInfo.setBleKey(vehicleBean.controllerKey);
            vehicleInfo.setAccountId(vehicleBean.accountId);
            vehicleInfo.setActivated(vehicleBean.activated);
            VehicleInfoDao vehicleInfoDao = GlobalContext.getDaoSession().getVehicleInfoDao();
            vehicleInfoDao.update(vehicleInfo);
        } else {
//            vehicleBean = selectVehicleBean(vehicleBean);
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.setVin(vehicleBean.vin);
            vehicleInfo.setVinpwd(vehicleBean.vinpwd);
            vehicleInfo.setAccountId(vehicleBean.accountId);
            vehicleInfo.setActivated(vehicleBean.activated);
            vehicleInfo.setBleAddr(vehicleBean.controllerAddr);
            vehicleInfo.setBleKey(vehicleBean.controllerKey);
            VehicleInfoDao vehicleInfoDao = GlobalContext.getDaoSession().getVehicleInfoDao();
            vehicleInfoDao.insertOrReplace(vehicleInfo);
        }
    }

    public static VehicleBean selectVehicleBean(VehicleBean vehicleBean) {
        VehicleInfoDao vehicleInfoDao = GlobalContext.getDaoSession().getVehicleInfoDao();
        List<VehicleInfo> vehicleInfoList = vehicleInfoDao.queryBuilder().where(VehicleInfoDao.Properties.Vin.eq(vehicleBean.vin)).build().list();
        if (vehicleInfoList.size() != 0) {
            VehicleInfo info = vehicleInfoList.get(0);
            VehicleBean bean = new VehicleBean();
            bean.id = info.getId();
            bean.vin = info.getVin();
            bean.vinpwd = info.getVinpwd();
            bean.controllerAddr = info.getBleAddr();
            bean.controllerKey = info.getBleKey();
            bean.accountId = info.getAccountId();
            bean.activated = info.getActivated();
            return bean;
        }
        return null;
    }

    public static VehicleBean selectVehicleBean(long account, boolean activated) {
        VehicleInfoDao vehicleInfoDao = GlobalContext.getDaoSession().getVehicleInfoDao();
        List<VehicleInfo> vehicleInfoList;
        if (account == 0) {
            vehicleInfoList = vehicleInfoDao.loadAll();
        } else {
            vehicleInfoList = vehicleInfoDao.queryBuilder().where(VehicleInfoDao.Properties.AccountId.eq(account), VehicleInfoDao.Properties.Activated.eq(activated)).build().list();
        }

        if (vehicleInfoList.size() != 0) {
            VehicleInfo info = vehicleInfoList.get(0);
            VehicleBean bean = new VehicleBean();
            bean.id = info.getId();
            bean.vin = info.getVin();
            bean.vinpwd = info.getVinpwd();
            bean.controllerAddr = info.getBleAddr();
            bean.controllerKey = info.getBleKey();
            bean.accountId = info.getAccountId();
            bean.activated = info.getActivated();
            return bean;
        }
        return null;
    }

    public static void saveBindedVehicle(BindedVehicle bv) {
        BindedVehicleDao bindedVehicleDao = GlobalContext.getDaoSession().getBindedVehicleDao();
        long count = bindedVehicleDao.queryBuilder().where(BindedVehicleDao.Properties.UserId.eq(bv.userId),
                BindedVehicleDao.Properties.Vin.eq(bv.vin)).buildCount().count();
        if (count > 0) {
            return;
        }
        com.sita.cfmoto.persistence.BindedVehicle vehicle = new com.sita.cfmoto.persistence.BindedVehicle();
        vehicle.setUserId(bv.userId);
        vehicle.setMcuid(bv.mcuid);
        vehicle.setVin(bv.vin);
        vehicle.setIccid(bv.iccid);
        vehicle.setImsi(bv.imsi);
        vehicle.setVincpy(bv.vincpy);
        vehicle.setImei(bv.imei);
        vehicle.setDealer_id(bv.dealerId);
        vehicle.setSoft_ware_code(bv.softWareCode);
        vehicle.setHard_ware_code(bv.hardWareCode);
        vehicle.setVproto(bv.vproto);
        vehicle.setTerminal_id(bv.terminalId);
        vehicle.setSeeds(bv.seeds);
        vehicle.setPassword(bv.password);
        vehicle.setImeicpy(bv.imeicpy);
        vehicle.setDo_time(bv.doTime);
        vehicle.setMachine_status(bv.machineStatus);
        vehicle.setMachine_type(bv.machineType);
        vehicle.setMachine_kind(bv.machineKind);

        bindedVehicleDao.insertOrReplace(vehicle);
    }

    public static void saveBindedVehicles(List<BindedVehicle> vehicles) {
        BindedVehicleDao bindedVehicleDao = GlobalContext.getDaoSession().getBindedVehicleDao();
        List<com.sita.cfmoto.persistence.BindedVehicle> vehicleList = new ArrayList<>();
        if (vehicles != null && !vehicles.isEmpty()) {
            Iterator<BindedVehicle> iterator = vehicles.iterator();
            while (iterator.hasNext()) {
                BindedVehicle bv = iterator.next();

                long count = bindedVehicleDao.queryBuilder().where(BindedVehicleDao.Properties.UserId.eq(bv.userId),
                        BindedVehicleDao.Properties.Vin.eq(bv.vin)).buildCount().count();
                if (count > 0) {
                    continue;
                }
                com.sita.cfmoto.persistence.BindedVehicle vehicle = new com.sita.cfmoto.persistence.BindedVehicle();
                vehicle.setUserId(bv.userId);
                vehicle.setMcuid(bv.mcuid);
                vehicle.setVin(bv.vin);
                vehicle.setIccid(bv.iccid);
                vehicle.setImsi(bv.imsi);
                vehicle.setVincpy(bv.vincpy);
                vehicle.setImei(bv.imei);
                vehicle.setDealer_id(bv.dealerId);
                vehicle.setSoft_ware_code(bv.softWareCode);
                vehicle.setHard_ware_code(bv.hardWareCode);
                vehicle.setVproto(bv.vproto);
                vehicle.setTerminal_id(bv.terminalId);
                vehicle.setSeeds(bv.seeds);
                vehicle.setPassword(bv.password);
                vehicle.setImeicpy(bv.imeicpy);
                vehicle.setDo_time(bv.doTime);
                vehicle.setMachine_status(bv.machineStatus);
                vehicle.setMachine_type(bv.machineType);
                vehicle.setMachine_kind(bv.machineKind);
                vehicleList.add(vehicle);
            }
        }
        if (!vehicleList.isEmpty()) {
            bindedVehicleDao.insertOrReplaceInTx(vehicleList);
        }
    }

    public static boolean deleteUserAllBindedVehicle(Long accountId) {
//        long currUserId = Long.valueOf(LocalStorage.getAccountId());
        long currUserId = Long.valueOf(accountId);
        BindedVehicleDao bindedVehicleDao = GlobalContext.getDaoSession().getBindedVehicleDao();
        try {
            List<com.sita.cfmoto.persistence.BindedVehicle> bvs = bindedVehicleDao.queryBuilder()
                    .where(BindedVehicleDao.Properties.UserId.eq(currUserId)).build().list();
            if (bvs != null && bvs.size() > 0) {
                bindedVehicleDao.deleteInTx(bvs);
                return true;
            }
        } catch (DaoException e) {
        }
        return false;
    }

    public static boolean deleteBindedVehicle(BindedVehicle vehicle) {
        long currUserId = Long.valueOf(LocalStorage.getAccountId());
        BindedVehicleDao bindedVehicleDao = GlobalContext.getDaoSession().getBindedVehicleDao();
        try {
            com.sita.cfmoto.persistence.BindedVehicle bv = bindedVehicleDao.queryBuilder()
                    .where(BindedVehicleDao.Properties.UserId.eq(currUserId),
                            BindedVehicleDao.Properties.Vin.eq(vehicle.vin))
                    .unique();
            if (bv != null) {
                bindedVehicleDao.delete(bv);
                return true;
            }
        } catch (DaoException e) {
        }
        return false;
    }

    public static BindedVehicle getBindedVehicle(String sn) {
        long currUserId = Long.valueOf(LocalStorage.getAccountId());
        BindedVehicleDao bindedVehicleDao = GlobalContext.getDaoSession().getBindedVehicleDao();

        try {
            com.sita.cfmoto.persistence.BindedVehicle bv = bindedVehicleDao.queryBuilder()
                    .where(BindedVehicleDao.Properties.UserId.eq(currUserId), BindedVehicleDao.Properties.Vin.eq(sn))
                    .unique();

            if (bv == null) {
                return null;
            }
            BindedVehicle vehicle = new BindedVehicle();
            vehicle.userId = bv.getUserId();
            vehicle.userId = bv.getUserId();
            vehicle.mcuid = bv.getMcuid();
            vehicle.vin = bv.getVin();
            vehicle.iccid = bv.getIccid();
            vehicle.imsi = bv.getImsi();
            vehicle.vincpy = bv.getVincpy();
            vehicle.imei = bv.getImei();
            vehicle.dealerId = bv.getDealer_id();
            vehicle.softWareCode = bv.getSoft_ware_code();
            vehicle.hardWareCode = bv.getHard_ware_code();
            vehicle.vproto = bv.getVproto();
            vehicle.terminalId = bv.getTerminal_id();
            vehicle.seeds = bv.getSeeds();
            vehicle.password = bv.getPassword();
            vehicle.imeicpy = bv.getImeicpy();
            vehicle.doTime = bv.getDo_time();
            vehicle.machineStatus = bv.getMachine_status();
            vehicle.machineType = bv.getMachine_type();
            vehicle.machineKind = bv.getMachine_kind();

            return vehicle;
        } catch (DaoException e) {
            return null;
        }
    }

    public static List<BindedVehicle> getBindedVehicles() {
        long currUserId = Long.valueOf(LocalStorage.getAccountId());
        BindedVehicleDao bindedVehicleDao = GlobalContext.getDaoSession().getBindedVehicleDao();
        List<com.sita.cfmoto.persistence.BindedVehicle> vehicleList = bindedVehicleDao.queryBuilder()
                .where(BindedVehicleDao.Properties.UserId.eq(currUserId)).build().list();
        List<BindedVehicle> vehicles = new ArrayList<>();
        if (vehicleList != null && !vehicleList.isEmpty()) {
            for (Iterator<com.sita.cfmoto.persistence.BindedVehicle> iterator = vehicleList.iterator(); iterator.hasNext(); ) {
                com.sita.cfmoto.persistence.BindedVehicle bv = iterator.next();
                BindedVehicle vehicle = new BindedVehicle();
                vehicle.userId = bv.getUserId();
                vehicle.userId = bv.getUserId();
                vehicle.mcuid = bv.getMcuid();
                vehicle.vin = bv.getVin();
                vehicle.iccid = bv.getIccid();
                vehicle.imsi = bv.getImsi();
                vehicle.vincpy = bv.getVincpy();
                vehicle.imei = bv.getImei();
                vehicle.dealerId = bv.getDealer_id();
                vehicle.softWareCode = bv.getSoft_ware_code();
                vehicle.hardWareCode = bv.getHard_ware_code();
                vehicle.vproto = bv.getVproto();
                vehicle.terminalId = bv.getTerminal_id();
                vehicle.seeds = bv.getSeeds();
                vehicle.password = bv.getPassword();
                vehicle.imeicpy = bv.getImeicpy();
                vehicle.doTime = bv.getDo_time();
                vehicle.machineStatus = bv.getMachine_status();
                vehicle.machineType = bv.getMachine_type();
                vehicle.machineKind = bv.getMachine_kind();

                vehicles.add(vehicle);
            }
            return vehicles;
        }
        return null;
    }

    public static void saveDynamic(Dynamic dynamic) {
        DynamicDao dynamicDao = GlobalContext.getDaoSession().getDynamicDao();
        if (dynamic != null) {
            com.sita.cfmoto.persistence.Dynamic dynamicPer = BeanUtils.copyProperties(dynamic);
            dynamicDao.insertOrReplace(dynamicPer);
        }
    }

    public static List<com.sita.cfmoto.persistence.Dynamic> getAllDynamic() {
        DynamicDao dynamicDao = GlobalContext.getDaoSession().getDynamicDao();
        return dynamicDao.loadAll();
    }

    public static List<com.sita.cfmoto.persistence.Dynamic> getDynamic(int count) {
        QueryBuilder builder = GlobalContext.getDaoSession().queryBuilder(com.sita.cfmoto.persistence.Dynamic.class);
        // builder.whereOr(StudentDao.Properties.Address.eq("北京"), StudentDao.Properties.Age.eq(50));
        builder.orderDesc(DynamicDao.Properties.MsgId);
        builder.limit(count);//取前n条数据
        List list = builder.list();
        return list;
    }

    public static List<com.sita.cfmoto.persistence.Dynamic> getDynamic(int count, int source) {
        QueryBuilder builder = GlobalContext.getDaoSession().queryBuilder(com.sita.cfmoto.persistence.Dynamic.class);
        // builder.whereOr(StudentDao.Properties.Address.eq("北京"), StudentDao.Properties.Age.eq(50));
        builder.where(DynamicDao.Properties.Source.eq(source));
        builder.orderDesc(DynamicDao.Properties.MsgId);
        builder.limit(count);//取前n条数据
        List list = builder.list();
        return list;
    }

    public static void saveActive(Active active) {
        ActiveDao activeDao = GlobalContext.getDaoSession().getActiveDao();
        if (active != null) {
            com.sita.cfmoto.persistence.Active activePer = BeanUtils.copyProperties(active);
            activeDao.insertOrReplace(activePer);
        }
    }

    public static List<com.sita.cfmoto.persistence.Active> getAllActive() {
        ActiveDao activeDao = GlobalContext.getDaoSession().getActiveDao();
        return activeDao.loadAll();
    }

    public static List<com.sita.cfmoto.persistence.Active> getActive(int count) {
        QueryBuilder builder = GlobalContext.getDaoSession().queryBuilder(com.sita.cfmoto.persistence.Active.class);
        builder.orderDesc(ActiveDao.Properties.CampaignId);
        builder.limit(count);
        List list = builder.list();
        return list;
    }

    public static void saveTopic(Topic topic) {
        TopicDao topicDao = GlobalContext.getDaoSession().getTopicDao();
        if (topic != null) {
            com.sita.cfmoto.persistence.Topic topicPer = BeanUtils.copyProperties(topic);
            topicDao.insertOrReplace(topicPer);
        }
    }

    public static List<com.sita.cfmoto.persistence.Topic> getAllTopic() {
        TopicDao topicDao = GlobalContext.getDaoSession().getTopicDao();
        return topicDao.loadAll();
    }

    public static List<com.sita.cfmoto.persistence.Topic> getTopic(int count) {
        QueryBuilder builder = GlobalContext.getDaoSession().queryBuilder(com.sita.cfmoto.persistence.Topic.class);
        builder.orderDesc(TopicDao.Properties.TopicId);
        builder.limit(count);
        List list = builder.list();
        return list;
    }

    public static void saveTopicAuthor(TopicAuthor topicAuthor) {
        TopicAuthorDao topicAuthorDao = GlobalContext.getDaoSession().getTopicAuthorDao();
        if (topicAuthor != null) {
            com.sita.cfmoto.persistence.TopicAuthor topicAuthorPer = BeanUtils.copyProperties(topicAuthor);
            topicAuthorDao.insertOrReplace(topicAuthorPer);
        }
    }
}