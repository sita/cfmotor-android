package com.sita.cfmoto.utils;

import java.text.DecimalFormat;

/**
 * Created by xiaodong on 16/2/17.
 */
public class NumberUtils {
    private static final DecimalFormat CURRENCY_FORMAT = new DecimalFormat("#,###.00");

    public static String formatNumber(double d) {
        if (d == (long) d) {
            return String.format("%d", (long) d);
        } else {
            if (d > 50) {
                return new DecimalFormat("#").format(d);
            } else if (d > 10) {
                return new DecimalFormat("#.#").format(d);
            } else {
                return new DecimalFormat("#.#").format(d);
            }
        }
    }

    public static String formatCurrency(double d) {
        return CURRENCY_FORMAT.format(d);
    }

    public static float setValue(float value) {
        if (Float.isNaN(value)) {
            return 0f;
        } else {
            return value;
        }
    }

}
