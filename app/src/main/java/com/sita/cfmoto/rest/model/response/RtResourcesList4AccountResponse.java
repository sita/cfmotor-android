package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.RtResourcesList;

public class RtResourcesList4AccountResponse extends Reply {
	@SerializedName("data")
	public RtResourcesList mData;
}
