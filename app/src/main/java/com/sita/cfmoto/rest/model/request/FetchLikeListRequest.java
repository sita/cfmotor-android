package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */
/*
  "res_id": 427964494270959529
  */
public class FetchLikeListRequest {
    @SerializedName("res_id")
    public long resId;
}
