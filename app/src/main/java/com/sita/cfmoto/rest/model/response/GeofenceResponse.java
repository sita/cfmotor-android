package com.sita.cfmoto.rest.model.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaodong on 16/2/23.
 */

//"snid":"20001",
//        "sncpy":1,
//        "lat":22.23948567,
//        "lng":130.312345673,
//        "distance":20000,
//        "isweilan":1

public class GeofenceResponse {
    @SerializedName("lat")
    @JSONField(name = "lat")
    public double lat;
    @SerializedName("lng")
    @JSONField(name = "lng")
    public double lng;
    @SerializedName("distance")
    @JSONField(name = "distance")
    public int distance;
    @SerializedName("isweilan")
    @JSONField(name = "isweilan")
    public int set;
    @SerializedName("weilan_time")
    @JSONField(name = "weilan_time")
    public long timestamp;
}
