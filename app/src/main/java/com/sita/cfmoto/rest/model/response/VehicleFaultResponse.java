package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark man on 2016/5/20.
 */
//"brake_status": 1,
//        "turn_status": 1,
//        "controller_status": 1,
//        "motor_status": 1,
//        "voltage_status": 1,
//        "bms_status": 1,
//        "communication_status": 1,
//        "headlight_status": 1,
//        "rearlight_status": 1,
//        "speaker_status": 1,
//        "converter_status": 1,
//        "controlcenter_status": 1,
//        "gps_status": 1,
//        "dotime": 1465960763036

public class VehicleFaultResponse {
    @SerializedName("brake_status")
    public int brakeStatus;
    @SerializedName("turn_status")
    public int turnStatus;
    @SerializedName("controller_status")
    public int controllerStatus;
    @SerializedName("motor_status")
    public int motorStatus;
    @SerializedName("bms_status")
    public int bmsStatus;
    @SerializedName("communication_status")
    public int commStatus;
    @SerializedName("headlight_status")
    public int headlightStatus;
    @SerializedName("rearlight_status")
    public int rearlightStatus;
    @SerializedName("speaker_status")
    public int speakerStatus;
    @SerializedName("converter_status")
    public int converterStatus;
    @SerializedName("controlcenter_status")
    public int ecuStatus;
    @SerializedName("gps_status")
    public int gpsStatus;
    @SerializedName("dotime")
    public long timestamp; // 上传时间

    public boolean isOk() {
        return (brakeStatus + turnStatus + controllerStatus +
                motorStatus + bmsStatus + commStatus +
                headlightStatus + rearlightStatus +
                speakerStatus + converterStatus +
                ecuStatus + gpsStatus == 0);
    }
}
