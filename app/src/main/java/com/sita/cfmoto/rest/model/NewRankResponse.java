package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/2/2.
 */
public class NewRankResponse {
    @SerializedName("errorCode")
    public String mErrorCode;
}
