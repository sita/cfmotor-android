package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xiaodong on 16/2/18.
 */
public class RtResourceNoteList {
    @SerializedName("notes")
    public List<RtResourceNote> notes;
}
