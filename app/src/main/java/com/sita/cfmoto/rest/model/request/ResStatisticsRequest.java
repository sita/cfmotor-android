package com.sita.cfmoto.rest.model.request;

/**
 * Created by hongyun on 2016/4/5.
 */
public class ResStatisticsRequest {
    public String accountId;
    public String type;
    public String resourceId;
}
