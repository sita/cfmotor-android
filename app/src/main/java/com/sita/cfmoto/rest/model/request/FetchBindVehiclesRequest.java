package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wxd on 16/6/8.
 */
public class FetchBindVehiclesRequest {
    @SerializedName("user_id")
    @JSONField(name = "user_id")
    public long userId;
}
