package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark man on 2016/5/20.
 */
//"starttime": 1463917666000,
//        "endtime": 1463924866000,
//        "soc": 98,
//        "soh": 95,
//        "bsoc": 34,
//        "esoc": 98,
//        "dotime": 1463924866000,
//        "charge_status": 1

public class FetchVehicleBatteryHistoryResponse {

    @SerializedName("starttime")
    public long startTime;
    @SerializedName("endtime")
    public long endTime;
    @SerializedName("soc")
    public int soc;
    @SerializedName("soh")
    public int soh;
    @SerializedName("bsoc")
    public int bsoc;
    @SerializedName("esoc")
    public int esoc;
    @SerializedName("dotime")
    public long doTime;
    @SerializedName("charge_status")
    public int chargeStatus;

}
