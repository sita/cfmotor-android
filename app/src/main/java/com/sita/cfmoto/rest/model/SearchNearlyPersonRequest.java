package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/1/19.
 */
public class SearchNearlyPersonRequest {
    @SerializedName("accountId")
    public String mAccountId;
    public Location location;
    @SerializedName("geofence")
    public int mGeofence;
    @SerializedName("pageNumber")
    public String mPageNumber;
    @SerializedName("pageSize")
    public String mPageSize;
}
