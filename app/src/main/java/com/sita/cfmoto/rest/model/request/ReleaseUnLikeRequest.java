package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */

/*
*  "user_id":425867899668794452,
  "res_id":427424797547302790
        */
public class ReleaseUnLikeRequest {
    @SerializedName("user_id")
    public long userId;
    @SerializedName("res_id")
    public long resId;
}
