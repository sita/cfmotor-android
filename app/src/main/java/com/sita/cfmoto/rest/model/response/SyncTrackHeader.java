package com.sita.cfmoto.rest.model.response;

import java.io.Serializable;

public class SyncTrackHeader implements Serializable {
    public String description; //说明
    public long startTime;//开始时间
    public long endTime;//结束时间
    public double startLat;//开始地点纬度
    public double startLong;//开始地点经度
    public double endLat;//结束地点纬度
    public double endLong;//结束地点经度
    public long mileage;//里程，单位 米
    public int maxRpm;//转速
    public float maxSpeed;//速度 单位 米/秒
}
