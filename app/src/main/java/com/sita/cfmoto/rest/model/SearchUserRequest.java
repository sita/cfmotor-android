package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/23.
 */
public class SearchUserRequest {
//    "accountId": "249539626602071046",
//            "mobile": "18600000000",
//            "includeSelf": true

    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("keyword")
    public String mKeyword;
    @SerializedName("includeSelf")
    public Boolean mIncludeSelf;

}
