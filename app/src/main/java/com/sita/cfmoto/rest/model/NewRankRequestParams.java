package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/2/2.
 */
public class NewRankRequestParams {
    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("mileage")
    public double mMileage;
    @SerializedName("period")
    public long mPeriod;
}
