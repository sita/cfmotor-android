package com.sita.cfmoto.rest.model;

/**
 * Created by xiaodong on 15/9/16.
 */
public class LocType {
    public static final String WGS84 = "WGS84";
    public static final String GCJ02 = "GCJ02";
    public static final String BD09 = "BD09";
    public static final String BD09LL = "BD09LL";
    public static final String BD09MC = "BD09MC";
}
