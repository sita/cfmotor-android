package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manhongyun on 15/11/12.
 */
public class SyncTrackItem implements Serializable {
    @SerializedName("id")
    public String mTrackId;
    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("header")
    public String mHeader;
    @SerializedName("detailFile")
    public String mTrackFile;
    @SerializedName("createDate")
    public String mCreateDate;

}
