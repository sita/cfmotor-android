package com.sita.cfmoto.rest.model.response;

import com.sita.cfmoto.rest.model.Account;

import java.io.Serializable;

/**
 * Created by wxd on 16/6/9.
 */
public class AccountResponse implements Serializable {
    public Account account;
}
