package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/1/26.
 */
public class CommentListResponse {
    @SerializedName("errorCode")
    public String mErrorCode;
    @SerializedName("data")
    public DynamicDetail mData;
}
