package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/23.
 */
public class ListUserRequest {
//    "accountId": "245183991651501070"

    @SerializedName("accountId")
    public String mAccountId;
}
