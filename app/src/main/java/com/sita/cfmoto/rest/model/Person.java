package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/1/19.
 */
public class Person {

    //    "accountId": "275678775285384763",
//            "avatar": "http://q.qlogo.cn/qqapp/1105003410/0B3C46723E72F2805996554E6506B83C/100",
//            "nickName": "张德鹏",
//            "gender": 1,
//            "distance": 0,
//            "geoTime": 0
    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("mobile")
    public String mMobile;
    @SerializedName("avatar")
    public String mAvatar;
    @SerializedName("nickName")
    public String mNickName;
    @SerializedName("gender")
    public int mGender;
    @SerializedName("distance")
    public double mDistance;
    @SerializedName("geoTime")
    public double mGeoTime;
}
