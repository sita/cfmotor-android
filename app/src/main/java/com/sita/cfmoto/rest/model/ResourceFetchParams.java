package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/12.
 */
public class ResourceFetchParams {

    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("resourceId")
    public String mResourceId;

}
