package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */
/*
  "maxId": 457207709952053000,
  "minId": 325881181930655000,
  "beginDate": 1370810593133,
  "endDate": 1570967912312,
  "size": 6,
  "page": 0,
  "mode": 1,
  "user_id": null,
  "topic_id": null,
  "campagn_id": null
  */
public class FetchDynamicListRequest {
    @SerializedName("maxId")
    public Long maxId;

    @SerializedName("minId")
    public Long minId;

    @SerializedName("beginDate")
    public Long beginDate;

    @SerializedName("endDate")
    public Long endDate;

    @SerializedName("size")
    public Integer size;

    @SerializedName("page")
    public Integer page;

        @SerializedName("mode")
    public int mode;
//
    @SerializedName("user_id")
    public String userId;
//
    @SerializedName("topic_id")
    public String topicId;
//
    @SerializedName("campagn_id")
    public String campagnId;

    @SerializedName("source")
    public Integer source;

    @SerializedName("mine")
    public Integer mine;
}
