package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */
/*
  "maxId": 457207709952053000,
  "minId": 325881181930655000,
  "beginDate": 1370810593133,
  "endDate": 1570967912312,
  "size": 6,
  "page": 0,
  "mode": 1,
  "user_id": null,
  "topic_id": null,
  "campagn_id": null
  */
public class FetchCommentListRequest {
    @SerializedName("size")
    public int size;
    @SerializedName("page")
    public int page;
    @SerializedName("postmsgId")
    public String postMsgId;
}
