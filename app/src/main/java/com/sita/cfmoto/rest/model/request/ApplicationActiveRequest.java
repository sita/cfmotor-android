package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark on 2016/8/31.
 */

/*
  "roomId":"229465432581472684",
  "userId":"425967341902236765",
  "campaignsId":"436032336681766390"
*/

public class ApplicationActiveRequest {
    @SerializedName("userId")
    public String userId;
    @SerializedName("campaignsId")
    public String campaignId;
}
