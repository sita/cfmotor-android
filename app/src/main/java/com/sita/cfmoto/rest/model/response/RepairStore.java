package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark man on 2016/5/20.
 */
//"id": 886,
//        "lat": 39.42758,
//        "lng": 116.27809,
//        "storename": "华本车行",
//        "storeaddress": "固安县新中街228号",
//        "storetel": "13191978880",
//        "distance": 112129.82101883354

//"id": 1224,
//        "lat": 32.167454,
//        "lng": 119.30072,
//        "storename": "福州市鼓楼区电动自行车店",
//        "storeaddress": "镇江市丹徒区高姿邦俊摩托车经销部",
//        "storetel": "051185687477",
//        "distance": 110629.5479744813

public class RepairStore {
    @SerializedName("id")
    public long id;
    @SerializedName("lat")
    public double storeLatitude;
    @SerializedName("lng")
    public double storeLongitude;
    @SerializedName("storename")
    public String storeName;
    @SerializedName("storeaddress")
    public String storeAddress;
    @SerializedName("storetel")
    public String storeTel;
    @SerializedName("distance")
    public float distance;
}
