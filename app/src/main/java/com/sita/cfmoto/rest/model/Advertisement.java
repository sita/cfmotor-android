package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/2/15.
 */
public class Advertisement {
   @SerializedName("id")
    public long id;
    @SerializedName("picurl")
    public String picurl;
    @SerializedName("adurl")
    public String adurl;
    @SerializedName("playcount")
    public String playcount;
}
