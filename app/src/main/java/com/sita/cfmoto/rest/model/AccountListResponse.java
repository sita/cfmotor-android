package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zhangdepeng on 2016/1/19.
 */
public class AccountListResponse {
    @SerializedName("errorCode")
    public String mErrorCode;
    @SerializedName("data")
    public List<Account> mData;
}
