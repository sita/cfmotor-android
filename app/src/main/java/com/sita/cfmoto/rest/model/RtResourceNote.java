package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

public class RtResourceNote {
    @SerializedName("noteId")
    public String noteId;
    @SerializedName("resId")
    public String resId;
    @SerializedName("accountId")
    public String accountId;
    @SerializedName("message")
    public String message;
    @SerializedName("createTime")
    public long createTime;
    @SerializedName("lastModified")
    public long lastModified;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RtResourceNote note = (RtResourceNote) o;

        if (createTime != note.createTime) return false;
        if (lastModified != note.lastModified) return false;
        if (noteId != null ? !noteId.equals(note.noteId) : note.noteId != null) return false;
        if (resId != null ? !resId.equals(note.resId) : note.resId != null) return false;
        if (accountId != null ? !accountId.equals(note.accountId) : note.accountId != null) return false;
        return message != null ? message.equals(note.message) : note.message == null;

    }

    @Override
    public int hashCode() {
        int result = noteId != null ? noteId.hashCode() : 0;
        result = 31 * result + (resId != null ? resId.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (int) (createTime ^ (createTime >>> 32));
        result = 31 * result + (int) (lastModified ^ (lastModified >>> 32));
        return result;
    }
}
