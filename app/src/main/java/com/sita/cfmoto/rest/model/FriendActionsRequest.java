package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/23.
 */
public class FriendActionsRequest {
//    "accountId": "251773449175827469",
//            "actionType": "send_real_geo",
//            "targetType": "users",
//            "targetId": "253157136316497939",
//            "actionBody": "your custom body"
//
//    {
//        "accountId": "251773449175827469",
//            "actionType": "send_real_geo",
//            "targetType": "groups",
//            "targetId": "277101651955810327",
//            "actionBody": {"key": "value"}
//    }
//

//    ADD_FRIEND("add_friend"),
//    ACCEPT_FRIEND("accept_friend"),
//    REJECT_FRIEND("reject_friend"),
//    CREATE_CHAT_GROUP("create_chat_group"),
//    DELETE_CHAT_GROUP("delete_chat_group"),
//    CREATE_CHAT_ROOM("create_chat_room"),
//    DELETE_CHAT_ROOM("delete_chat_room"),
//    SEND_REAL_GEO("send_real_geo"),
//    SEND_GEO_ONCE("send_geo_once"),
//    STOP_REAL_GEO("stop_real_geo"),
//    SEND_POI("send_poi"),
//    SEND_TEXT_MSG("send_text_msg"),
//    SEND_VOICE_MSG("send_voice_msg"),
//    SEND_VIDEO_MSG("send_video_msg")


//    USERS("users"),
//    GROUPS("groups"),
//    CHATROOMS("chatrooms"),
//    CHATGROUPS("chatgroups");


    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("actionType")
    public String mActionType;
    @SerializedName("targetType")
    public String mTargetType;
    @SerializedName("targetId")
    public String mTargetId;
    @SerializedName("actionBody")
    public Object mActionBody;

}
