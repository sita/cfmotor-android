package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by manhongyun on 15/11/23.
 */
public class SearchResponse {
    @SerializedName("errorCode")
    public String mErrorCode;
    @SerializedName("data")
    public List<Account> mData;
}
