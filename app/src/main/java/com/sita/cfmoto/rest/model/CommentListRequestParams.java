package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/1/26.
 */
public class CommentListRequestParams {
    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("resId")
    public String mResId;
}
