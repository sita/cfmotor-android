package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by xiaodong on 16/2/17.
 */
public class Reply implements Serializable {
    @SerializedName("errorCode")
    public String errorCode;

}
