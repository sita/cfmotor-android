package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hongyun on 2016/4/5.
 */

public class CfmotorCommonRequest2 {
    @SerializedName("vin")
    @JSONField(name = "vin")
    public String vin;
    @SerializedName("vinCpy")
    @JSONField(name = "vinCpy")
    public long vinCpy;
}
