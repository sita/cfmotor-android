package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/*
"user_id":6666666666666,
  "vin_id":"8888",
  "vin_cpy":1
  */

public class UnbindVehicleRequest implements Serializable {
    @JSONField(name = "vin_id")
    @SerializedName("vin_id")
    public String vinId;
    @JSONField(name = "vin_cpy")
    @SerializedName("vin_cpy")
    public long vinCpy;
    @JSONField(name = "user_id")
    @SerializedName("user_id")
    public long userId;
}
