package com.sita.cfmoto.rest.model.request;

import java.io.Serializable;

/**
 * Created by wxd on 16/6/8.
 */
public class UserLoginRequest implements Serializable {
    public String mobile;
    public String password;
}
