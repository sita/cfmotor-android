package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.Account;

/**
 * Created by xiaodong on 16/2/23.
 */
public class GetAccountResponse extends Reply {
    @SerializedName("data")
    public Account account;
}
