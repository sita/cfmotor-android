package com.sita.cfmoto.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/7/28.
 */
public class BindUsers {
    @SerializedName("username")
    public String username;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("avatar")
    public String avatar;
    @SerializedName("allavatarfilename")
    public String allavatarfilename;
}
