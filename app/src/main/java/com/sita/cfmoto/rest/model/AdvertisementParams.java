package com.sita.cfmoto.rest.model;

public class AdvertisementParams {
    public String params;

    public AdvertisementParams() {
        this("");
    }

    public AdvertisementParams(String parameters) {
        params = parameters;
    }
}
