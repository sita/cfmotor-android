package com.sita.cfmoto.rest.model.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaodong on 16/2/23.
 */

//        "issoc": 0,
//        "soc": 20,
//        "soc_time": 1467280798139


public class BatteryAlarmResponse {
    @SerializedName("issoc")
    @JSONField(name = "issoc")
    public int enabled;
    @SerializedName("soc")
    @JSONField(name = "soc")
    public int soc;
    @SerializedName("soc_time")
    @JSONField(name = "soc_time")
    public long socTime;
}
