package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/23.
 */
public class LoginRequest {
    @SerializedName("mobile")
    public String mMobile;
    @SerializedName("password")
    public String mPassword;

}
