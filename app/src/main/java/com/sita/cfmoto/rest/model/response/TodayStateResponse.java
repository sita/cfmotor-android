package com.sita.cfmoto.rest.model.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xiaodong on 16/2/23.
 */

//"distanceToday": "676.4726584291473",
//        "scoToday": "0",
//        "timeToday": "6923204478",
//        "distancetotal": "5149.4128855600303",
//        "timetotal": "8660695574"

public class TodayStateResponse {
    @SerializedName("distanceToday")
    @JSONField(name = "distanceToday")
    public String distanceToday;
    @SerializedName("scoToday")
    @JSONField(name = "scoToday")
    public String socToday;
    @SerializedName("timeToday")
    @JSONField(name = "timeToday")
    public String timeToday;
    @SerializedName("distancetotal")
    @JSONField(name = "distancetotal")
    public String distanceTotal;
    @SerializedName("timetotal")
    @JSONField(name = "timetotal")
    public String timeTotal;
}
