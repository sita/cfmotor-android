package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

//"campaign_id":436032336681766390,
//        "user_id":425967341902236765,
//        "state":1

public class ConfirmUsersRequest implements Serializable {
    @SerializedName("campaign_id")
    public Long campaignId;
    @SerializedName("user_id")
    public Long userId;
    @SerializedName("state")
    public Integer state;
}
