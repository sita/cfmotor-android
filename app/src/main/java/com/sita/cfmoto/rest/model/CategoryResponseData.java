package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zhangde001 on 2015/12/10.
 */
public class CategoryResponseData {
    @SerializedName("data")
    public List<Category> mCategory;
}
