package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hongyun on 2016/2/2.
 */
public class GroupCreateResponse {

    @SerializedName("groupId")
    public String mGroupId;

    @SerializedName("name")
    public String mName;

    @SerializedName("admins")
    public Object mAdmins;

    @SerializedName("members")
    public List<Account> mMembers;

    @SerializedName("imGroupId")
    public String mImGroupId;

    @SerializedName("owner")
    public Account mOwner;

    @SerializedName("public")
    public String mPublic;


}
