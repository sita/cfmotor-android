package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangde001 on 2016/1/15.
 */
public class ThirdPartyRegisterRequestParams {
    @SerializedName("provider")
    public String mProvider;
    @SerializedName("account")
    public String mAccount;
    @SerializedName("accessToken")
    public String mAccessToken;
    @SerializedName("expire")
    public int mExpire;
    @SerializedName("avatar")
    public String mAvatar;
    @SerializedName("nickname")
    public String mNickname;
    @SerializedName("gender")
    public String mGender;
    @SerializedName("location")
    public Location mLocation;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"provider\":\"").append(mProvider).append("\"").append(",");
        sb.append("\"account\":\"").append(mAccount).append("\"").append(",");
        sb.append("\"accessToken\":\"").append(mAccessToken).append("\"").append(",");
        sb.append("\"expire\":").append(mExpire).append(",");
        sb.append("\"avatar\":\"").append(mAvatar).append("\"").append(",");
        sb.append("\"nickname\":\"").append(mNickname).append("\"").append(",");
        sb.append("\"gender\":\"").append(mGender).append("\"");
        sb.append('}');
        return sb.toString();
    }
}
