package com.sita.cfmoto.rest.model.request;

/**
 * Created by xiaodong on 16/2/23.
 */
public class GetAccountRequest {
    public String accountId;
    public boolean showDetail = false;
}
