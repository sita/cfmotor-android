package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manhongyun on 15/11/12.
 */
public class ResourceDetail2 {

//    "id": "277180113131406368",
//            "account":{
//        "id": "274873508234790443",
//                "avatar": "http://files.sitayun.com/avatar/dist/default.png",
//                "gender": 0
//    },
//            "trafficType": "SLOW",
//            "title": "55",
//            "location":{"longitude": "117.394086", "latitude": "39.160694", "locType": "GCJ02", "time": "1453112843621"…},
//            "pic":[
//    {
//        "link": "http://files.sitayun.com/roadtrust/pic/277180113114629155.jpg"
//    }
//    ],
//            "audio":[],
//            "createDate": "1453112843621"


    @SerializedName("id")
    public String mAccountId;
    @SerializedName("account")
    public Account mAccount;
    @SerializedName("trafficType")
    public String mTrafficType;
    @SerializedName("title")
    public String mTitle;
    @SerializedName("location")
    public Location mLocation;
    @SerializedName("pic")
    public List<Picture> mPics;
    @SerializedName("audio")
    public List<Audio> mAudio;
    @SerializedName("createDate")
    public String mCreateDate;

    public List<Picture> getPics() {
        if (mPics == null) {
            mPics = new ArrayList<Picture>();
        }
        return mPics;
    }

    public ResourceDetail2 setPics(List<Picture> mPics) {
        this.mPics = mPics;
        return this;
    }

    public List<Audio> getAudio() {
        if (mAudio == null) {
            mAudio = new ArrayList<Audio>();
        }
        return mAudio;
    }

    public ResourceDetail2 setAudio(List<Audio> mAudio) {
        this.mAudio = mAudio;
        return this;
    }
}
