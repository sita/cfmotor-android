package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/26.
 */
/*
说明：
"userId":用户id,（long型）
"size":分页、每页显示数量,（int型）
"page":分页，第几页,（int型）
"maxId":"",（String型）
"minId":"",（String型）
"beginDate":"",（String型）
"endDate":"",（String型）
"searchType":查询类型、0查我发起的 1查我参与的（int型）
  */
public class FetchMyActiveListRequest {
    @SerializedName("userId")
    public long userId;

    @SerializedName("size")
    public int size;

    @SerializedName("page")
    public int page;

    @SerializedName("maxId")
    public String maxId;

    @SerializedName("minId")
    public String minId;

    @SerializedName("beginDate")
    public String beginDate;

    @SerializedName("endDate")
    public String endDate;

    @SerializedName("searchType")
    public int searchType;
}
