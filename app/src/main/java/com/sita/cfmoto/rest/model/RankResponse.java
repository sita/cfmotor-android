package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zhangdepeng on 2016/2/1.
 */
public class RankResponse {
    @SerializedName("errorCode")
    public String mErrorCode;
    @SerializedName("data")
    public List<Rank> mData;
}
