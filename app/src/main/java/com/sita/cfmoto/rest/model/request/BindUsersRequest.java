package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class BindUsersRequest implements Serializable {
    @SerializedName("sn")
    @JSONField(name = "sn")
    public String sn;
    @SerializedName("sncpy")
    @JSONField(name = "sncpy")
    public long snCpy;
}
