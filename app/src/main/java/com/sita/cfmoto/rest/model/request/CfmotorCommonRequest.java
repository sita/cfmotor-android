package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hongyun on 2016/4/5.
 */

public class CfmotorCommonRequest {
    @SerializedName("vin")
    @JSONField(name = "vin")
    public String vin;
    @SerializedName("vincpy")
    @JSONField(name = "vincpy")
    public long vinCpy;
}
