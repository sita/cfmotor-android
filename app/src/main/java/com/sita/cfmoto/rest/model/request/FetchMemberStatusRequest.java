package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark on 2016/8/31.
 */

/*
{
        "user_id":426454588787264608,
        "campaign_id":439904414216490048
        }
*/

public class FetchMemberStatusRequest {
    @SerializedName("user_id")
    public Long userId;
    @SerializedName("campaign_id")
    public Long campaignId;
}
