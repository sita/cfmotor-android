package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by wxd on 16/6/8.
 */
/*{
id": 1,
"mcuid": "1",
"vin": "1",
"iccid": "1",
"imsi": "1",
"imei": "1",
"dealer_id": "1",
"hard_ware_code": "1",
"soft_ware_code": "1",
"vproto": "0",
"terminal_id": 26,
"seeds": "1",
"password": "123455",
"vincpy": 1,
"imeicpy": 1,
"do_time": 12345,
"machine_status": "1",
"machine_type": "NK150",
"machine_kind": "NK150"
}*/
public class Vehicle implements Serializable {

    @SerializedName("id")
    @JSONField(name = "id")
    public long id;
    @SerializedName("mcuid")
    @JSONField(name = "mcuid")
    public String mcuid;
    @SerializedName("vin")
    @JSONField(name = "vin")
    public String vin;
    @SerializedName("iccid")
    @JSONField(name = "iccid")
    public String iccid;
    @SerializedName("imsi")
    @JSONField(name = "imsi")
    public String imsi;
    @SerializedName("imei")
    @JSONField(name = "imei")
    public String imei;
    @SerializedName("dealer_id")
    @JSONField(name = "dealer_id")
    public String dealerId;
    @SerializedName("hard_ware_code")
    @JSONField(name = "hard_ware_code")
    public String hardWareCode;
    @SerializedName("soft_ware_code")
    @JSONField(name = "soft_ware_code")
    public String softWareCode;
    @SerializedName("vproto")
    @JSONField(name = "vproto")
    public String vproto;
    @SerializedName("terminal_id")
    @JSONField(name = "terminal_id")
    public long terminalId;
    @SerializedName("seeds")
    @JSONField(name = "seeds")
    public String seeds;
    @SerializedName("password")
    @JSONField(name = "password")
    public String password;
    @SerializedName("vincpy")
    @JSONField(name = "vincpy")
    public long vincpy;
    @SerializedName("imeicpy")
    @JSONField(name = "imeicpy")
    public int imeicpy;
    @SerializedName("do_time")
    @JSONField(name = "do_time")
    public long doTime;
    @SerializedName("machine_status")
    @JSONField(name = "machine_status")
    public String machineStatus;
    @SerializedName("machine_type")
    @JSONField(name = "machine_type")
    public String machineType;
    @SerializedName("machine_kind")
    @JSONField(name = "machine_kind")
    public String machineKind;
}
