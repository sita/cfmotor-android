package com.sita.cfmoto.rest.model.request;

/**
 * Created by xiaodong on 16/3/1.
 */
public class RtDelResourceRequest {
    public String accountId;
    public String resourceId;
    public RtDelResourceRequest(String mAccountId, String mResourceId){
        accountId = mAccountId;
        resourceId = mResourceId;
    }
}
