package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.RtResourceCount;

/**
 * Created by xiaodong on 16/2/24.
 */
public class RtResourcesCountResponse extends Reply {
    @SerializedName("data")
    public RtResourceCount data;
}
