package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark man on 2016/5/20.
 */
//"snid":"23345",
//        "sncpy":1,
//        "lat":39.148658,
//        "lng":117.277504,
//        "kilometer":50


public class RepairFetchRequest {
//    @SerializedName("snid")
//    public String sn;
//    @SerializedName("sncpy")
//    public long sncpy;
    @SerializedName("lat")
    public double lat;
    @SerializedName("lng")
    public double lng;
    @SerializedName("kilometer")
    public int distance;
}
