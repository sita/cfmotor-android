package com.sita.cfmoto.rest.model.request;

/**
 * Created by xiaodong on 16/2/22.
 */
public class ResourceListByAccountRequest {
    public String accountId;
    public String authorId;
    public String pageNumber;
    public String pageSize;
}
