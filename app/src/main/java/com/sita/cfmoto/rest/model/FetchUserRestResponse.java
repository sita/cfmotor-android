package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.persistence.User;

import java.util.List;

/**
 * Created by manhongyun on 15/11/21.
 */
public class FetchUserRestResponse {
    @JSONField(name = "errorCode")
    @SerializedName("errorCode")
    public String mErrorCode;

    @JSONField(name = "data")
    @SerializedName("data")
    public List<User> mUsers;

}
