package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lijuan zhang on 2016/8/15.
 */
/*
"topic_id": "429437092410428736",
"author":{"user_id": "429372744237321320", "nickname": "13820825057",
 "avatar": "http://123.206.201.243/yun/userinfo/avatar/0/default.png"…},
"type": 0,
"source": 0,
"head_pic": "http://123.206.201.243/yun/userinfo/topic/20160813/429437092410428736/48514500858.jpg",
"pic": "http://123.206.201.243/yun/userinfo/topic/20160813/429437092410428736/48514500856.jpg",
"video": "",
"subject": "标题5",
"content": "万智牌是不是一个烧钱的游戏",
"state": 0,
"praise_count": 0,
"share_count": 0,
"read_counnt": 0,
"create_time": "2016-08-13 17:31",
"statis_date": "20160815"
* */
public class Topic implements Serializable {
    @SerializedName("topic_id")
    @JSONField(name = "topic_id")
    public String topicId;
    @SerializedName("author")
    @JSONField(name = "author")
    public TopicAuthor author;
    @SerializedName("type")
    @JSONField(name = "type")
    public int type;
    @SerializedName("source")
    @JSONField(name = "source")
    public int source;

    @SerializedName("head_pic")
    @JSONField(name = "head_pic")
    public String headPic;

    @SerializedName("pic")
    @JSONField(name = "pic")
    public String pic;

    @SerializedName("video")
    @JSONField(name = "video")
    public String video;

    @SerializedName("subject")
    @JSONField(name = "subject")
    public String subject;

    @SerializedName("content")
    @JSONField(name = "content")
    public String content;

    @SerializedName("state")
    @JSONField(name = "state")
    public int state;

    @SerializedName("praise_count")
    @JSONField(name = "praise_count")
    public long praiseCount;

    @SerializedName("share_count")
    @JSONField(name = "share_count")
    public long shareCount;

    @SerializedName("read_counnt")
    @JSONField(name = "read_counnt")
    public long readCounnt;

    @SerializedName("participants")
    @JSONField(name = "participants")
    public long joinedCounnt;

    @SerializedName("create_time")
    @JSONField(name = "create_time")
    public String createTime;

    @SerializedName("statis_date")
    @JSONField(name = "statis_date")
    public String statisDate;

}
