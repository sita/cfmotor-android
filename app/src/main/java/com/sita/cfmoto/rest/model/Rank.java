package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/2/1.
 */
public class Rank {
    @SerializedName("mileage")
    public double mMileage;
    @SerializedName("period")
    public long mPeriods;
    @SerializedName("accountId")
    public String accountId;
}
