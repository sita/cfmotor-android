package com.sita.cfmoto.rest.model.request;

import com.sita.cfmoto.rest.model.Location;

/**
 * Created by hongyun on 2016/4/5.
 */
public class UpdateGeoRequest {
    public String accountId;
    public Location location;
}
