package com.sita.cfmoto.rest.model;

import java.util.List;

public class RtResourcesList {
    public long count = 0;
    public List<RtResource> resources;
}
