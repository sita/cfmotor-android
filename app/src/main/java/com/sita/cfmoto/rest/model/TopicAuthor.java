package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lijuan zhang on 2016/8/15.
 */
/*
user_id": "429372744237321320",
"nickname": "13820825057",
"avatar": "http://123.206.201.243/yun/userinfo/avatar/
* */
public class TopicAuthor implements Serializable {
    @SerializedName("user_id")
    @JSONField(name = "user_id")
    public String userId;

    @SerializedName("nickname")
    @JSONField(name = "nickname")
    public String nickName;

    @SerializedName("avatar")
    @JSONField(name = "avatar")
    public String avatar;
}
