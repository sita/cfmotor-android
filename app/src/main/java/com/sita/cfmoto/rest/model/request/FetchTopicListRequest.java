package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */
public class FetchTopicListRequest {
    @SerializedName("size")
    public Integer size;
    @SerializedName("page")
    public Integer page;
    @SerializedName("beginDate")
    public Long beginDate;
    @SerializedName("endDate")
    public Long endDate;
    @SerializedName("type")
    public Integer type;
    @SerializedName("source")
    public Integer source;
    @SerializedName("maxId")
    public Long maxId;
    @SerializedName("minId")
    public Long minId;

}
