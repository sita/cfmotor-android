package com.sita.cfmoto.rest.model.response;

/**
 * Created by mark man on 2016/5/20.
 */
//"brake_status": 1,
//        "turn_status": 1,
//        "controller_status": 1,
//        "motor_status": 1,
//        "voltage_status": 1,
//        "bms_status": 1,
//        "communication_status": 1,
//        "headlight_status": 1,
//        "rearlight_status": 1,
//        "speaker_status": 1,
//        "converter_status": 1,
//        "controlcenter_status": 1,
//        "gps_status": 1,
//        "dotime": 1464577689060

public class VehicleStatusResponse {
    public int turn_status; // 转向
    public int controller_status; // 中控
    public int motor_status; // 电机
    public int voltage_status; // 电压
    public int bms_status; // 电池
    public int communication_status; // 通讯
    public int headlight_status; // 大灯
    public int rearlight_status; // 尾灯
    public int speaker_status; // 喇叭
    public int converter_status; // 转换器
    public int gps_status; // gps
    public long dotime; // 上传时间

}
