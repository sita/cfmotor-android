package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark on 2016/8/31.
 */

/*
{
"size":10,
"page":0,
"user_id":426454588787264608,
  "campaign_id":436032336681766390
}
*/

public class FetchApplicationListRequest {
//    @SerializedName("size")
//    public Long pageSize;
//    @SerializedName("page")
//    public Long pageNumber;
    @SerializedName("campaign_id")
    public Long activeId;
}
