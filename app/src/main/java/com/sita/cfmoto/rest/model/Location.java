package com.sita.cfmoto.rest.model;


import android.support.annotation.Nullable;

import java.io.Serializable;

public class Location implements Serializable {
    public String latitude;
    public String longitude;
    @Nullable
    public String locType;
    @Nullable
    public String time;
    @Nullable
    public String province;
    @Nullable
    public String city;
}
