package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lijuan zhang on 2016/9/2.
 */
/*
"topic_count": 0,
"postmsg_count": 13,
"campaign_count": 0
* */
public class InteractCount implements Serializable {
    @SerializedName("topic_count")
    @JSONField(name = "topic_count")
    public int topicCount;

    @SerializedName("postmsg_count")
    @JSONField(name = "postmsg_count")
    public int postmsgCount;

    @SerializedName("campaign_count")
    @JSONField(name = "campaign_count")
    public int campaignCount;

}
