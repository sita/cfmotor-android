package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.RtResourceLike;

/**
 * Created by xiaodong on 16/2/18.
 */
public class RtResourceLikeResponse extends Reply {
    @SerializedName("data")
    public RtResourceLike data;
}
