package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.MileageSummaryRanking;

/**
 * Created by xiaodong on 16/2/24.
 */
public class MileageSummaryRankingResponse extends Reply {
    @SerializedName("data")
    public MileageSummaryRanking data;
}
