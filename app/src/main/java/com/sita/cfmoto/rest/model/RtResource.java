package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.model.Orgpicname;

import java.io.Serializable;
import java.util.List;

public class RtResource implements Serializable {
    @SerializedName("id")
    public String id;
    @SerializedName("trafficType")
    public String trafficType;
    @SerializedName("title")
    public String title;
    @SerializedName("accountId")
    public String accountId;
    @SerializedName("location")
    public Location location;
    @SerializedName("createDate")
    public Long createDate;
    @SerializedName("playcount")
    public Long playCount;
    @SerializedName("voteCount")
    public Long voteCount;
    @SerializedName("thumbnails")
    public List<Picture> thumbnails;
    @SerializedName("pics")
    public List<Picture> pics;
    @SerializedName("audios")
    public List<Audio> audios;

    // for big pic
    public List<Orgpicname> orgpicname;

    @SerializedName("votable")
    public boolean votable;
    @SerializedName("shortVideo")
    public String shortVideo;
    @SerializedName("fullVideo")
    public String fullVideo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RtResource that = (RtResource) o;

        if (votable != that.votable) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (trafficType != null ? !trafficType.equals(that.trafficType) : that.trafficType != null)
            return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null)
            return false;
        if (playCount != null ? !playCount.equals(that.playCount) : that.playCount != null)
            return false;
        if (voteCount != null ? !voteCount.equals(that.voteCount) : that.voteCount != null)
            return false;
        if (pics != null ? !pics.equals(that.pics) : that.pics != null) return false;
        if (audios != null ? !audios.equals(that.audios) : that.audios != null) return false;
        if (shortVideo != null ? !shortVideo.equals(that.shortVideo) : that.shortVideo != null)
            return false;
        return !(fullVideo != null ? !fullVideo.equals(that.fullVideo) : that.fullVideo != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (trafficType != null ? trafficType.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (playCount != null ? playCount.hashCode() : 0);
        result = 31 * result + (voteCount != null ? voteCount.hashCode() : 0);
        result = 31 * result + (pics != null ? pics.hashCode() : 0);
        result = 31 * result + (audios != null ? audios.hashCode() : 0);
        result = 31 * result + (votable ? 1 : 0);
        result = 31 * result + (shortVideo != null ? shortVideo.hashCode() : 0);
        result = 31 * result + (fullVideo != null ? fullVideo.hashCode() : 0);
        return result;
    }
}
