package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/13.
 */
public class ResourceFetchResponse {
    //    {
//        "errorCode": "0",
//            "data": {
//        "accountId": "1",
//                "trafficType": "SLOW",
//                "title": "55",
//                "location": {
//            "longitude": "117.318367",
//                    "latitude": "39.112078",
//                    "locType": "GCJ02",
//                    "time": "1447337381868"
//        },
//        "pic": "[{\"name\":\"oFq0NTcsRjOdTqSMlqiUDg.jpg\",\"url\":\"http://files.sitayun.com/roadtrust/pic/oFq0NTcsRjOdTqSMlqiUDg.jpg\"},{\"name\":\"u4SkMsy3T4OKtjGWeBUVvA.jpg\",\"url\":\"http://files.sitayun.com/roadtrust/pic/u4SkMsy3T4OKtjGWeBUVvA.jpg\"},{\"name\":\"DOMlNR4PRiuHiKEjtssOcg.jpg\",\"url\":\"http://files.sitayun.com/roadtrust/pic/DOMlNR4PRiuHiKEjtssOcg.jpg\"}]",
//                "audio": "[{\"name\":\"-9eUu2bORe6XfO6e3MXH0w.mp3\",\"url\":\"http://files.sitayun.com/roadtrust/audio/-9eUu2bORe6XfO6e3MXH0w.mp3\"}]",
//                "createDate": "1447337470417"
//        }
//    }
    @SerializedName("errorCode")
    public String mErrorCode;
    @SerializedName("data")
    public ResourceDetail2 mData;
}
