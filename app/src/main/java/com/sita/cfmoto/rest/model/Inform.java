package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by  by lijuan zhang on 2016/9/2.
 */
/*
* "id": 441900715955195152, 通知id
"other_id": 441858481746611560, 活动或者动态 id
"type": 1,
"a_user_id": 425967341902236765, 发起者id
"b_user_id": 425939193751931996, 申请者id 或点赞者id
"nick_name": "宋军-测试2",
"avatar": "http://123.206.201.243/yun/userinfo/avatar/425939193751931996/48701486319.jpg",
"statis_data": "20160901",
"create_time": "2016-09-01 16:57:50",
"dotime": 1472720270401,
"info": "申请加入你的活动",
"state": 0*/
public class Inform implements Serializable {
    public static final int TYPE_APPLICATION_RESPONSE = 1;
    public static final int TYPE_AGREE = 2;
    public static final int TYPE_COMMENT = 3;
    public static final int TYPE_APPLICATION_REQUEST = 4;

    @SerializedName("other_id")
    public String entityId;
    @SerializedName("type")
    public int type;
    @SerializedName("a_user_id")
    public String userId;
    @SerializedName("b_user_id")
    public String informFromId;
    @SerializedName("nick_name")
    public String nickName;
    @SerializedName("avatar")
    public String avatar;
    @SerializedName("create_time")
    public String createTime;
    @SerializedName("dotime")
    public long doTime;
    @SerializedName("info")
    public String info;
    @SerializedName("state")
    public int state;


}
