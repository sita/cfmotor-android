package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/12.
 */
public class ReleaseDynamicParams {
    @SerializedName("addr")
    public String address;
    @SerializedName("content")
    public String content;
    @SerializedName("visible")//0为false
    public int visible;
    @SerializedName("authorId")
    public long authorId;
    @SerializedName("lat")
    public double lat;
    @SerializedName("lng")
    public double lng;
    @SerializedName("source")//客户端传0
    public int source;
    @SerializedName("topicId")
    public String topicId;
    @SerializedName("campaignId")
    public String campaignId;
}
