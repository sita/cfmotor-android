package com.sita.cfmoto.rest.model.request;

/**
 * Created by xiaodong on 16/2/18.
 */
public class RtLeaveNoteNewRequest {
    public String accountId;
    public String resId;
    public String message;
}
