package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */
/*
"sn": "1234567890abcdefg",
  "sncpy":1
  */
public class FetchVehicleDataRequest {
    @SerializedName("sn")
    public String sn;
    @SerializedName("sncpy")
    public long sncpy;
}
