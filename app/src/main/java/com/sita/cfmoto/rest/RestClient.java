package com.sita.cfmoto.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.sita.cfmoto.rest.model.AccountListResponse;
import com.sita.cfmoto.rest.model.AddRequest;
import com.sita.cfmoto.rest.model.AdvertisementParams;
import com.sita.cfmoto.rest.model.AdvertisementResponse;
import com.sita.cfmoto.rest.model.CommentListRequestParams;
import com.sita.cfmoto.rest.model.CommentListResponse;
import com.sita.cfmoto.rest.model.FetchUserRestResponse;
import com.sita.cfmoto.rest.model.FriendActionsRequest;
import com.sita.cfmoto.rest.model.GroupCreateRequest;
import com.sita.cfmoto.rest.model.ListUserRequest;
import com.sita.cfmoto.rest.model.LoginRequest;
import com.sita.cfmoto.rest.model.MemberStatusResponse;
import com.sita.cfmoto.rest.model.NewRankRequestParams;
import com.sita.cfmoto.rest.model.NewRankResponse;
import com.sita.cfmoto.rest.model.RankResponse;
import com.sita.cfmoto.rest.model.ResourceCommentRequest;
import com.sita.cfmoto.rest.model.ResourceCommentResponse;
import com.sita.cfmoto.rest.model.ResourceFetchParams;
import com.sita.cfmoto.rest.model.ResourceFetchResponse;
import com.sita.cfmoto.rest.model.RestResponse;
import com.sita.cfmoto.rest.model.RtResourceListRequest;
import com.sita.cfmoto.rest.model.SearchNearlyPersonRequest;
import com.sita.cfmoto.rest.model.SearchResponse;
import com.sita.cfmoto.rest.model.SearchUserRequest;
import com.sita.cfmoto.rest.model.ThirdPartyRegisterRequestParams;
import com.sita.cfmoto.rest.model.request.ApplicationActiveRequest;
import com.sita.cfmoto.rest.model.request.BindUsersRequest;
import com.sita.cfmoto.rest.model.request.BindVehicleRequest;
import com.sita.cfmoto.rest.model.request.CfmotorCommonRequest;
import com.sita.cfmoto.rest.model.request.CfmotorCommonRequest2;
import com.sita.cfmoto.rest.model.request.ConfirmUsersRequest;
import com.sita.cfmoto.rest.model.request.FetchApplicationListRequest;
import com.sita.cfmoto.rest.model.request.FetchBindVehiclesRequest;
import com.sita.cfmoto.rest.model.request.FetchCommentListRequest;
import com.sita.cfmoto.rest.model.request.FetchDynamicListRequest;
import com.sita.cfmoto.rest.model.request.FetchInformListRequest;
import com.sita.cfmoto.rest.model.request.FetchLikeListRequest;
import com.sita.cfmoto.rest.model.request.FetchMemberStatusRequest;
import com.sita.cfmoto.rest.model.request.FetchMyActiveListRequest;
import com.sita.cfmoto.rest.model.request.FetchMyTopicListRequest;
import com.sita.cfmoto.rest.model.request.FetchTopicListRequest;
import com.sita.cfmoto.rest.model.request.FetchVehicleBatteryHistoryRequest;
import com.sita.cfmoto.rest.model.request.FetchVehicleDataRequest;
import com.sita.cfmoto.rest.model.request.FetchVehicleDriveHistoryRequest;
import com.sita.cfmoto.rest.model.request.GetAccountRequest;
import com.sita.cfmoto.rest.model.request.GetRtResourceCountRequest;
import com.sita.cfmoto.rest.model.request.ListVehicleIdRequest;
import com.sita.cfmoto.rest.model.request.ReleaseCommentRequest;
import com.sita.cfmoto.rest.model.request.ReleaseLikeRequest;
import com.sita.cfmoto.rest.model.request.ReleaseUnLikeRequest;
import com.sita.cfmoto.rest.model.request.RepairFetchRequest;
import com.sita.cfmoto.rest.model.request.ResStatisticsRequest;
import com.sita.cfmoto.rest.model.request.ResourceListByAccountRequest;
import com.sita.cfmoto.rest.model.request.RtBlacklistRequest;
import com.sita.cfmoto.rest.model.request.RtDeLikeResourceRequest;
import com.sita.cfmoto.rest.model.request.RtDelResourceRequest;
import com.sita.cfmoto.rest.model.request.RtLeaveNoteListRequest;
import com.sita.cfmoto.rest.model.request.RtLeaveNoteNewRequest;
import com.sita.cfmoto.rest.model.request.RtLikeResourceRequest;
import com.sita.cfmoto.rest.model.request.SetBatteryAlarmRequest;
import com.sita.cfmoto.rest.model.request.SetGeoFenceRequest;
import com.sita.cfmoto.rest.model.request.UpdateGeoRequest;
import com.sita.cfmoto.rest.model.request.UserLoginRequest;
import com.sita.cfmoto.rest.model.request.UserRegisterRequest;
import com.sita.cfmoto.rest.model.response.ApplicationResponse;
import com.sita.cfmoto.rest.model.response.GetAccountResponse;
import com.sita.cfmoto.rest.model.response.MemberResponse;
import com.sita.cfmoto.rest.model.response.MileageSummaryRankingResponse;
import com.sita.cfmoto.rest.model.response.MileageSummaryResponse;
import com.sita.cfmoto.rest.model.response.RtBlacklistResponse;
import com.sita.cfmoto.rest.model.response.RtDelResourceResponse;
import com.sita.cfmoto.rest.model.response.RtResourceDelikeResponse;
import com.sita.cfmoto.rest.model.response.RtResourceLikeResponse;
import com.sita.cfmoto.rest.model.response.RtResourceNoteListResponse;
import com.sita.cfmoto.rest.model.response.RtResourceNoteNewResponse;
import com.sita.cfmoto.rest.model.response.RtResourcesCountResponse;
import com.sita.cfmoto.rest.model.response.RtResourcesList4AccountResponse;
import com.sita.cfmoto.rest.model.response.RtResourcesListResponse;
import com.sita.cfmoto.rest.model.response.SyncTrackListResponse;
import com.sita.cfmoto.support.Constants;
import com.squareup.okhttp.OkHttpClient;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class RestClient {
    private static final String BASE_URL = Constants.BASE_URI;
    private static RestService restService;
    private static RestService restNormalService;
    private static Gson mGson = new GsonBuilder()
            .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
            .serializeNulls()
            .registerTypeAdapter(Double.class, new JsonSerializer<Double>() {
                @Override
                public JsonElement serialize(Double src, Type typeOfSrc, JsonSerializationContext context) {
                    if (src == src.longValue())
                        return new JsonPrimitive(src.longValue());
                    return new JsonPrimitive(src);
                }
            })
            .registerTypeAdapter(Double.class, new JsonSerializer<Double>() {
                @Override
                public JsonElement serialize(Double src, Type typeOfSrc, JsonSerializationContext context) {
                    if (src.toString().contains("E")) {
//                        BigDecimal bd = new BigDecimal(src.toString());
                        DecimalFormat df = new DecimalFormat("0");
                        Double d = new Double(src);
                        long l = Long.parseLong(df.format(d));
                        return new JsonPrimitive(l);
//                        long l= Long.parseLong(bd.toPlainString());
//                        return new JsonPrimitive(l);
                    } else {
                        return new JsonPrimitive(src);
                    }
                }
            })
            .create();

    private static Gson mGson2 = new GsonBuilder()
            .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
            .create();

    private static RequestInterceptor restInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Content-Type", Constants.APPLICATION_JSON);
            request.addHeader("Accept", Constants.APPLICATION_JSON);
        }
    };

    private static RequestInterceptor restFormInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Accept", Constants.APPLICATION_JSON);
        }
    };

    private static void createRestFormClient() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(120, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(mGson))
                .setRequestInterceptor(restFormInterceptor)
                .setClient(new OkClient(okHttpClient))
                .build();

        restService = restAdapter.create(RestService.class);
    }

    private static void createRestNormalClient() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(120, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(mGson))
                .setRequestInterceptor(restInterceptor)
                .setClient(new OkClient(okHttpClient))
                .build();

        restNormalService = restAdapter.create(RestService.class);
    }

    public static RestService getRestService() {
        if (restService == null) {
            createRestFormClient();
        }
        return restService;
    }

    public static RestService getRestNormalService() {
        if (restNormalService == null) {
            createRestNormalClient();
        }
        return restNormalService;
    }

    public static Gson getGson() {
        return mGson;
    }

    public static Gson getGsonNull() {
        return mGson2;
    }

    public interface RestService {
        @Multipart
        @POST("/usermgmt/v1/accounts/register")
        void register(@Part("params") TypedString params, Callback<RestResponse> callback);

        @Multipart
        @POST("/usermgmt/v1/accounts/update")
        void update(@Part("avatar") TypedFile header, @Part("params") TypedString params, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/accounts/login")
        void login(@Body LoginRequest request, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/accounts/search")
        void search(@Body SearchUserRequest request, Callback<SearchResponse> callback);

        @POST("/usermgmt/v1/friends/add")
        void add(@Body AddRequest request, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/friends/reject")
        void reject(@Body AddRequest request, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/friends/accept")
        void accept(@Body AddRequest request, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/friends/list")
        void listUser(@Body ListUserRequest request, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/actions")
        void actions(@Body FriendActionsRequest request, Callback<RestResponse> callback);

        @POST("/roadtrust/v1/resources/fetch")
        void fetchOneResource(@Body ResourceFetchParams params, Callback<ResourceFetchResponse> callback);

        @POST("/usermgmt/v1/accounts/register3rd")
        void thirdPartyRegister(@Body ThirdPartyRegisterRequestParams request, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/accounts/login3rd")
        void thirdPartyLogin(@Body ThirdPartyRegisterRequestParams request, Callback<RestResponse> callback);

        @POST("/usermgmt/v1/accounts/list/geofence")
        void searchNearlyAccounts(@Body SearchNearlyPersonRequest request, Callback<AccountListResponse> callback);

        @GET("/usermgmt/v1/accounts")
        void listAccounts(@Query("page") int pageNumber, @Query("size") int pageSize, Callback<AccountListResponse> callback);

        @POST("/usermgmt/v1/groups/new")
        void newGroup(@Body GroupCreateRequest request, Callback<RestResponse> callback);

        @GET("/usermgmt/v1/groups/{groupid}")
        void getGroupDetail(@Path("groupid") String groupId, @Query("im") boolean isImGroup, Callback<RestResponse> callback);

//        @POST("/roadtrust/v1/range/list")
//        void getDynamicState(@Body RtResourceListRequest request, Callback<RtResourcesList4AccountResponse> callback);

        @POST("/roadtrust/v1/notes/new")
        void addResourceComment(@Body ResourceCommentRequest request, Callback<ResourceCommentResponse> callback);

        @POST("/roadtrust/v1/notes/list")
        void getCommentList(@Body CommentListRequestParams request, Callback<CommentListResponse> callback);

        @Multipart
        @POST("/usermgmt/v1/messages")
        void sendMessage(@Part("accountId") TypedString accountId,
                         @Part("targetType") TypedString targetType,
                         @Part("targetId") TypedString targetId,
                         @Part("message") TypedString message,
                         Callback<RestResponse> callback);

        @POST("/usermgmt/v1/mileages/new")
        void sendNewRanking(@Body NewRankRequestParams request, Callback<NewRankResponse> callback);

        @POST("/tinterest/v1/advertisinges/getone")
        void getAdvertisementData(@Body AdvertisementParams request, Callback<AdvertisementResponse> callback);

        @POST("/roadtrust/v1/resources/vote")
        void resourceLike(@Body RtLikeResourceRequest request, Callback<RtResourceLikeResponse> callback);

        @POST("/roadtrust/v1/resources/devote")
        void resourceDelike(@Body RtDeLikeResourceRequest request, Callback<RtResourceDelikeResponse> callback);

        @POST("/roadtrust/v1/resources/list")
        void rtResourceList(@Body RtResourceListRequest request, Callback<RtResourcesListResponse> callback);

        @POST("/roadtrust/v1/resources/get")
        void rtResourceListByAccount(@Body ResourceListByAccountRequest request, Callback<RtResourcesList4AccountResponse> callback);

        @POST("/roadtrust/v1/notes/new")
        void rtResourceNewNote(@Body RtLeaveNoteNewRequest request, Callback<RtResourceNoteNewResponse> callback);

        @POST("/roadtrust/v1/notes/list")
        void rtResourceListNote(@Body RtLeaveNoteListRequest request, Callback<RtResourceNoteListResponse> callback);

        @POST("/usermgmt/v1/accounts/obtain")
        void accountGet(@Body GetAccountRequest request, Callback<GetAccountResponse> callback);

        /**
         * For query on person, forSelf=true
         *
         * @param params   key can be accountId, startTime, endTime
         * @param callback
         */
        @GET("/usermgmt/v1/mileages/summaries?forSelf=true")
        void mileageSummaryForAccount(@QueryMap Map<String, String> params, Callback<MileageSummaryResponse> callback);

        /**
         * @param params   key can be accountId, startTime, endTime
         * @param callback
         */
        @GET("/usermgmt/v1/mileages/summaries")
        void mileageSummaryForFriends(@QueryMap Map<String, String> params, Callback<MileageSummaryResponse> callback);

        @GET("/usermgmt/v1/mileages/summaries")
        void getRanking(@Query("accountId") String accountId, Callback<RankResponse> callback);

        @GET("/usermgmt/v1/mileages/summaries/ranking")
        void mileageSummaryRanking(@Query("accountId") String accountId, Callback<MileageSummaryRankingResponse> callback);

        @POST("/roadtrust/v1/resources/count")
        void rtResourcesCount(@Body GetRtResourceCountRequest request, Callback<RtResourcesCountResponse> callback);

        @POST("/roadtrust/v1/accounts/{accountId}/blacklists")
        void rtAddBlacklist(@Path("accountId") String accountId, @Body RtBlacklistRequest request,
                            Callback<RtBlacklistResponse> callback);

        @PUT("/roadtrust/v1/accounts/{accountId}/blacklists")
        void rtUpdateBlacklist(@Path("accountId") String accountId, @Body RtBlacklistRequest request,
                               Callback<RtBlacklistResponse> callback);

        @POST("/roadtrust/v1/resources/delete")
        void rtDeleteResource(@Body RtDelResourceRequest request, Callback<RtDelResourceResponse> callback);

        @Multipart
        @POST("/roadtrust/v1/accounts/{id}/routes")
        void uploadRoadTrust(@Path("id") String id, @Part("params") TypedString params, @Part("file") TypedFile file, Callback<RestResponse> callback);

        @GET("/roadtrust/v1/accounts/{id}/routes")
        void syncTracksList(@Path("id") String accountId, Callback<SyncTrackListResponse> callback);

        @POST("/usermgmt/v1/accounts/updategeo")
        void updateGeo(@Body UpdateGeoRequest request, Callback<RestResponse> callback);

        @POST("/roadtrust/v1/resstatistics")
        void resStatistics(@Body ResStatisticsRequest request, Callback<RestResponse> callback);

        @POST("/cfmotor/v1/machines//machine/sn/list")
        void listVehicleId(@Body ListVehicleIdRequest request, Callback<RestResponse> callback);

        @POST("/cfmotor/v1/machines/machine/bind/machine")
        void bindVehicle(@Body BindVehicleRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/battery/status_impl")
        void batterStatus(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/local_impl")
        void vehicleLocation(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);

        //        @POST("/yadea/v1/machine/status_impl")
//        void getVehicleStatus(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);
         /*根据坐标获取维修点信息*/
        @POST("/cfmotor/v1/system/stores")
        void fetchRepairStores(@Body RepairFetchRequest request, Callback<RestResponse> callback);

        /*获取车辆的月汇总数据,目前没有此接口*/
        @POST("/cfmotor/v1/machines/machine/sum/mon")
        void fetchVehicleDriveMonthState(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);

        /*获取车辆的日汇总数据*/
        @POST("/cfmotor/v1/machines/machine/sum/day")
        void fetchVehicleDriveDayState(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);

        /*获取车辆的历史形成数据*/
        @POST("/cfmotor/v1/machines/machine/routeinfo/get")
        void fetchVehicleDriveHistory(@Body FetchVehicleDriveHistoryRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/bind/list")
        void fetchBindVehicles(@Body FetchBindVehiclesRequest request, Callback<RestResponse> callback);

        @GET("/yadea/v1/machine/today/{sn}")
        void fetchVehicleTodayState(@Path("sn") String sn, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/battery/charge_his")
        void fetchVehicleBatteryHistory(@Body FetchVehicleBatteryHistoryRequest request, Callback<RestResponse> callback);

        @POST("/cfmotor/v1/actors/app/register")
        void userRegister(@Body UserRegisterRequest request, Callback<RestResponse> callback);

        @POST("/cfmotor/v1/actors/app/login")
        void userLogin(@Body UserLoginRequest request, Callback<RestResponse> callback);

        @Multipart
        @POST("/cfmotor/v1/actors/app/update")
        void updateProfile(@Part("avatar") TypedFile header, @Part("params") TypedString params, Callback<RestResponse> callback);

        @POST("/cfmotor/v1/actors/app/resetpassword")
        void userModifyPassword(@Body UserRegisterRequest request, Callback<RestResponse> callback);

        @GET("/yadea/v1/actors/message/authentication/{mobile}")
        void getCheckCode(@Path("mobile") String mobile, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/getweilan/impl")
        void getGeofence(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/setweilan/impl")
        void setGeofence(@Body SetGeoFenceRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/getdianliang/impl")
        void getBatteryAlarm(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/setdianliang/impl")
        void setBatteryAlarm(@Body SetBatteryAlarmRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/yadeafault/status/impl")
        void getFault(@Body CfmotorCommonRequest request, Callback<RestResponse> callback);

        @POST("/yadea/v1/machine/bind/user")
        void getBindUserList(@Body BindUsersRequest request, Callback<RestResponse> callback);

        /*发布活动*/
        @Multipart
        @POST("/cfmotor/v1/social/campaign/new")
        void releaseActive(@Part("params") TypedString params, @Part("pic") TypedFile pic, Callback<RestResponse> callback);

        /*活动查询*/
//        @GET("/cfmotor/v1/social/campaign/search")
        @GET("/cfmotor/v1/social/campaign/more")
        void fetchActiveList(@Query("size") Integer size, @Query("page") Integer page,
                             @Query("beginDate") Long beginDate, @Query("endDate") Long endDate,
                             @Query("maxId") Long maxId, @Query("minId") Long minId,
                             @Query("source") Integer source, Callback<RestResponse> callback);

        /*发布动态*/
        @Multipart
        @POST("/cfmotor/v1/social/postmsg/new")
        void releaseDynamic(@Part("pic") TypedFile pic, @Part("params") TypedString params, Callback<RestResponse> callback);

        /*动态查询*/
        @POST("/cfmotor/v1/social/postmsg/search")
        void fetchDynamicList(@Body FetchDynamicListRequest request, Callback<RestResponse> callback);

        /*动态评论查询*/
        @POST("/cfmotor/v1/social/searchnotes")
        void fetchCommentList(@Body FetchCommentListRequest request, Callback<RestResponse> callback);

        /*根据动态id获取点赞*/
        @POST("/cfmotor/v1/social/praise/list")
        void fetchLikeList(@Body FetchLikeListRequest request, Callback<RestResponse> callback);

        /*首页车辆数据查询*/
        @POST("/cfmotor/v1/machine/local")
        void fetchVehicleData(@Body FetchVehicleDataRequest request, Callback<RestResponse> callback);

        /*动态发布评论*/
        @POST("/cfmotor/v1/social/postmsg/addnote")
        void releaseComment(@Body ReleaseCommentRequest request, Callback<RestResponse> callback);

        /*动态点赞*/
        @POST("/cfmotor/v1/social/praise/add")
        void releaseLike(@Body ReleaseLikeRequest request, Callback<RestResponse> callback);

        /*动态取消点赞*/
        @POST("/cfmotor/v1/social/praise/del")
        void releaseUnLike(@Body ReleaseUnLikeRequest request, Callback<RestResponse> callback);

        /*获取单条动态详情*/
        @GET("/cfmotor/v1/social/postmsg/findone")
        void fetchDynamic(@Query("postmsgid") long postmsgId, Callback<RestResponse> callback);

        /*查询话题*/
        @POST("/cfmotor/v1/topics/topic/search")
        void fetchTopicList(@Body FetchTopicListRequest request, Callback<RestResponse> callback);

        /*获取用户信息*/
        @GET("/cfmotor/v1/actors/users/get")
        void fetchUserInfo(@Query("userIdStr") String users, Callback<FetchUserRestResponse> callback);

        /*查询我发起的活动&我参与的活动*/
        @POST("/cfmotor/v1/social/campaign/search/mysponsor")
        void fetchMyActive(@Body FetchMyActiveListRequest request, Callback<RestResponse> callback);

        /*查询我参与的话题*/
        @POST("/cfmotor/v1/topics/topic/searchjoin")
        void fetchMyTopic(@Body FetchMyTopicListRequest request, Callback<RestResponse> callback);

        /*查询用户参与活动状态*/
        @POST("/cfmotor/v1/social/campaign/state")
        void fetchMemberStatus(@Body FetchMemberStatusRequest request, Callback<MemberStatusResponse> callback);

        /*用户申请加入活动*/
        @POST("/cfmotor/v1/social/campaign/addmember")
        void applicationActive(@Body ApplicationActiveRequest request, Callback<RestResponse> callback);

        /*获取参加活动用户*/
        @GET("/cfmotor/v1/social/campaign/member")
        void fetchActiveMemberList(@Query("campaignId") long activeId, Callback<MemberResponse> callback);

        /*获取用户参与互动数量*/
        @GET("/cfmotor/v1/topics/user/findUserAllCount")
        void fetchInteractCount(@Query("userId") long userId, Callback<RestResponse> callback);

        /*查询通知列表*/
        @POST("/cfmotor/v1/social/inform/search")
        void fetchInformList(@Body FetchInformListRequest request, Callback<RestResponse> callback);

        /*活动审核*/
        @POST("/cfmotor/v1/social/campaign/member/modify")
        void confirmActiveApplication(@Body ConfirmUsersRequest request, Callback<RestResponse> callback);

        /*活动审核成员列表*/
        @POST("/cfmotor/v1/social/campaign/memberlist")
        void fetchApplicationList(@Body FetchApplicationListRequest request, Callback<ApplicationResponse> callback);

        /*故障列表*/
        @POST("/cfmotor/v1/machines/machine/fault/status/impl")
        void fetchErrorList(@Body CfmotorCommonRequest2 request, Callback<RestResponse> callback);

        /*根据话题ID查询话题详情*/
        @GET("/cfmotor/v1/topics/{id}/get")
        void fetchTopic(@Path("id") String topicId, Callback<RestResponse> callback);
    }
}
