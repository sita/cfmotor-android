package com.sita.cfmoto.rest.model;

/**
 * Created by lijuan zhang on 2016/8/12.
 */
public class ReleaseActiveParams {
    public String userId;
    public String title;
    public String source;
    public String campaign_start_time;
    public String campaign_end_time;
    public String register_start_time;
    public String register_end_time;
    public String content;
    public Double deptLng;
    public Double deptLat;
    public String deptAddr;
    public Double arriLng;
    public Double arriLat;
    public String arriAddr;
    public int maxMemberCount;
}
