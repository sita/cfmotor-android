package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lijuan zhang on 2016/8/15.
 */
/*
"msg_id": 430878532952919272,
"author_id": 425967341902236765,
"content": "钱包",
"lng": 117.278109,
"lat": 39.148532,
"addr": "东北地锅(太阳城店)",
"visible": 1,
"pic": "48686323796.jpg",
"video": null,
"audio": null,
"source": 0,
"praise_count": 0,
"share_count": 0,
"comment_count": 2,
"ref_msg_id": 0,
"reply_msg_id": 0,
"campaign_id": 0,
"topic_id": 0,
"statis_date": "20160817",
"is_activity": 0,
"activity_url": "",
"picList":["http://123.206.201.243/yun/userinfo/pic/20160817/430878532952919272/48686323796.jpg"],
"dotime": 1471406323796,
"create_time": "2016-08-17 11:58",
"account_id_avat": "http://123.206.201.243/yun/userinfo/avatar/425967341902236765/48678468723.jpg",
"account_id_nickname": "啦啦啦啦啦",
"ref_msg_counr": 0
* */
public class Dynamic implements Serializable {
    @SerializedName("msg_id")
    @JSONField(name = "msg_id")
    public String msgId;

    @SerializedName("author_id")
    @JSONField(name = "author_id")
    public String authorId;

    @SerializedName("content")
    @JSONField(name = "content")
    public String content;

    @SerializedName("lng")
    @JSONField(name = "lng")
    public double lng;

    @SerializedName("lat")
    @JSONField(name = "lat")
    public double lat;

    @SerializedName("addr")
    @JSONField(name = "addr")
    public String address;
    @SerializedName("visible")
    @JSONField(name = "visible")
    public int visible;
    @SerializedName("pic")
    @JSONField(name = "pic")
    public String pic;
    @SerializedName("video")
    @JSONField(name = "video")
    public String video;
    @SerializedName("audio")
    @JSONField(name = "audio")
    public String audio;
    @SerializedName("source")
    @JSONField(name = "source")
    public int source;
    @SerializedName("praise_count")
    @JSONField(name = "praise_count")
    public long praiseCount;

    @SerializedName("share_count")
    @JSONField(name = "share_count")
    public long shareCount;

    @SerializedName("comment_count")
    @JSONField(name = "comment_count")
    public long commentCount;

    @SerializedName("ref_msg_id")
    @JSONField(name = "ref_msg_id")
    public String refMsgId;

    @SerializedName("reply_msg_id")
    @JSONField(name = "reply_msg_id")
    public String replyMsgId;

    @SerializedName("campaign_id")
    @JSONField(name = "campaign_id")
    public String campaignId;

    @SerializedName("topic_id")
    @JSONField(name = "topic_id")
    public String topicId;

    @SerializedName("statis_date")
    @JSONField(name = "statis_date")
    public String statisDate;

    @SerializedName("is_activity")
    @JSONField(name = "is_activity")
    public int isActivity;

    @SerializedName("activity_url")
    @JSONField(name = "activity_url")
    public String activityUrl;

    @SerializedName("dotime")
    @JSONField(name = "dotime")
    public long doTime;

    @SerializedName("create_time")
    @JSONField(name = "create_time")
    public String createTime;

    @SerializedName("account_id_avat")
    @JSONField(name = "account_id_avat")
    public String accountIdAvat;

    @SerializedName("account_id_nickname")
    @JSONField(name = "account_id_nickname")
    public String accountIdNickname;

    @SerializedName("ref_msg_counr")
    @JSONField(name = "ref_msg_counr")
    public int refMsgCounr;

    @SerializedName("picList")
    @JSONField(name = "picList")
    public List<String> picList;

}
