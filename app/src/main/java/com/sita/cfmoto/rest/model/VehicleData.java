package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */
/*
* vin": "1234567890abcdefg",
"lat": 30.1,
"lng": 121.21,
"total_mileage": 20,
"left_distance": 30,
"water_temperature": 22,
"oil_wear": 32,
"oil_mass": 44
*/
public class VehicleData {
    @SerializedName("vin")
    public String vin;
    @SerializedName("lat")
    public double lat;
    @SerializedName("lng")
    public double lng;
    @SerializedName("total_mileage")
    public long totalMileage;
    @SerializedName("left_distance")
    public long leftDistance;
    @SerializedName("water_temperature")
    public long waterTemperature;
    @SerializedName("oil_wear")
    public long oilWear;
    @SerializedName("oil_mass")
    public long oilMass;
}
