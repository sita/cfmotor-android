package com.sita.cfmoto.rest.model;

/**
 * Created by zhangdepeng on 2016/2/3.
 */
public class DynamicAccountd {
    private String accountId;
    private byte[] avatar;
    private Double distance;
    private String nickName;

    public Double getDistance() {
        return distance;
    }

    public DynamicAccountd setDistance(Double distance) {
        this.distance = distance;
        return this;
    }

    public String getNickName() {
        return nickName;
    }

    public DynamicAccountd setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }


    public String getAccountId() {
        return accountId;
    }

    public DynamicAccountd setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public DynamicAccountd setAvatar(byte[] avatar) {
        this.avatar = avatar;
        return this;
    }
}
