package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manhongyun on 16/03/18.
 */
public class SyncTrackRTData implements Serializable {
    @SerializedName("latitude")
    public double mLatitude;
    @SerializedName("longitude")
    public double mLongitude;
    @SerializedName("time")
    public long mTime;
    @SerializedName("speed")
    public float mSpeed;
    @SerializedName("rpm")
    public int mRpm;
    @SerializedName("shift")
    public int mShift;

}
