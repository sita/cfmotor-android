package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark man on 2016/5/20.
 */
//"battery_power": 0,
//        "battery_healthf": 0,
//        "battery_cnt": 0,
//        "dotime": 1463697699030

//         sn_cpy": 0,
//        "mileage_last_day10": 1176,
//        speed_last_day10": 20.3,
//        soc_last_day10": 567,
//        "times_last_day10": 3475,

public class FetchVehicleDriveDayDataResponse {
    @SerializedName("vin_id")
    public String vinId;
    @SerializedName("vin_cpy")
    public int vinCpy;
    @SerializedName("mileage_last_day10")
    public float mileageLastDay10;
    @SerializedName("speed_last_day10")
    public float speedLastDay10;
    @SerializedName("oilwear_last_day10")
    public float oilLastDay10;
    @SerializedName("times_last_day10")
    public long timesLastDay10;
    @SerializedName("ranking_last_day10")
    public int rankLastDay10;

    @SerializedName("mileage_last_day09")
    public float mileageLastDay09;
    @SerializedName("speed_last_day09")
    public float speedLastDay09;
    @SerializedName("oilwear_last_day09")
    public float oilLastDay09;
    @SerializedName("times_last_day09")
    public long timesLastDay09;
    @SerializedName("ranking_last_day09")
    public int rankLastDay09;

    @SerializedName("mileage_last_day08")
    public float mileageLastDay08;
    @SerializedName("speed_last_day08")
    public float speedLastDay08;
    @SerializedName("oilwear_last_day08")
    public float oilLastDay08;
    @SerializedName("times_last_day08")
    public long timesLastDay08;
    @SerializedName("ranking_last_day08")
    public int rankLastDay08;


    @SerializedName("mileage_last_day07")
    public float mileageLastDay07;
    @SerializedName("speed_last_day07")
    public float speedLastDay07;
    @SerializedName("oilwear_last_day07")
    public float oilLastDay07;
    @SerializedName("times_last_day07")
    public long timesLastDay07;
    @SerializedName("ranking_last_day07")
    public int rankLastDay07;


    @SerializedName("mileage_last_day06")
    public float mileageLastDay06;
    @SerializedName("speed_last_day06")
    public float speedLastDay06;
    @SerializedName("oilwear_last_day06")
    public float oilLastDay06;
    @SerializedName("times_last_day06")
    public long timesLastDay06;
    @SerializedName("ranking_last_day06")
    public int rankLastDay06;


    @SerializedName("mileage_last_day05")
    public float mileageLastDay05;
    @SerializedName("speed_last_day05")
    public float speedLastDay05;
    @SerializedName("oilwear_last_day05")
    public float oilLastDay05;
    @SerializedName("times_last_day05")
    public long timesLastDay05;
    @SerializedName("ranking_last_day05")
    public int rankLastDay05;


    @SerializedName("mileage_last_day04")
    public float mileageLastDay04;
    @SerializedName("speed_last_day04")
    public float speedLastDay04;
    @SerializedName("oilwear_last_day04")
    public float oilLastDay04;
    @SerializedName("times_last_day04")
    public long timesLastDay04;
    @SerializedName("ranking_last_day04")
    public int rankLastDay04;

    @SerializedName("mileage_last_day03")
    public float mileageLastDay03;
    @SerializedName("speed_last_day03")
    public float speedLastDay03;
    @SerializedName("oilwear_last_day03")
    public float oilLastDay03;
    @SerializedName("times_last_day03")
    public long timesLastDay03;
    @SerializedName("ranking_last_day03")
    public int rankLastDay03;

    @SerializedName("mileage_last_day02")
    public float mileageLastDay02;
    @SerializedName("speed_last_day02")
    public float speedLastDay02;
    @SerializedName("oilwear_last_day02")
    public float oilLastDay02;
    @SerializedName("times_last_day02")
    public long timesLastDay02;
    @SerializedName("ranking_last_day02")
    public int rankLastDay02;

    @SerializedName("mileage_last_day01")
    public float mileageLastDay01;
    @SerializedName("speed_last_day01")
    public float speedLastDay01;
    @SerializedName("oilwear_last_day01")
    public float oilLastDay01;
    @SerializedName("times_last_day01")
    public long timesLastDay01;
    @SerializedName("ranking_last_day01")
    public int rankLastDay01;

}
