package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hongyun on 2016/4/5.
 */

public class SetGeoFenceRequest {
    @SerializedName("snid")
    @JSONField(name = "snid")
    public String sn;
    @SerializedName("sncpy")
    @JSONField(name = "sncpy")
    public long sncpy;

    @SerializedName("lat")
    @JSONField(name = "lat")
    public double lat;

    @SerializedName("lng")
    @JSONField(name = "lng")
    public double lng;

    @SerializedName("distance")
    @JSONField(name = "distance")
    public int distance;

    @SerializedName("isweilan")
    @JSONField(name = "isweilan")
    public int enabled;
}
