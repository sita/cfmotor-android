package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mark man on 2016/5/20.
 */

public class RepairFetchResponse {
    @SerializedName("errorCode")
    public String errorCode;
    @SerializedName("data")
    public List<RepairStore> stores;
}
