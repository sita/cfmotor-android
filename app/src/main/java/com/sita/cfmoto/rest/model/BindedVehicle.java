package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by wxd on 16/6/8.
 */
/*{
    "user_id": 379124698841089724,
    "sn_id": "23345",
    "sn_cpy": 1,
    "sn_password": "password",
    "gps_id": "11113",
    "gps_cpy": 1,
    "controller_id": "1",
    "controller_password": "NRWOipVE+USOlfiotZC56g==",
    "machine_status": 2,
    "machine_type": 1,
    "machine_kind": "Z3",
    "create_time": 1465359248453,
    "controller_address": "C0:27:15:09:B0:C9",
    "elet_controller_id": "elec23456",
    "front_fork_id": "fork001",
    "meter_id": "meter001",
    "imsi": "600039287374"
}*/
public class BindedVehicle implements Serializable {
    @SerializedName("user_id")
    @JSONField(name = "user_id")
    public long userId;
    @SerializedName("id")
    @JSONField(name = "id")
    public long id;
    @SerializedName("mcuid")
    @JSONField(name = "mcuid")
    public String mcuid;
    @SerializedName("vin")
    @JSONField(name = "vin")
    public String vin;
    @SerializedName("iccid")
    @JSONField(name = "iccid")
    public String iccid;
    @SerializedName("imsi")
    @JSONField(name = "imsi")
    public String imsi;
    @SerializedName("imei")
    @JSONField(name = "imei")
    public String imei;
    @SerializedName("dealer_id")
    @JSONField(name = "dealer_id")
    public String dealerId;
    @SerializedName("hard_ware_code")
    @JSONField(name = "hard_ware_code")
    public String hardWareCode;
    @SerializedName("soft_ware_code")
    @JSONField(name = "soft_ware_code")
    public String softWareCode;
    @SerializedName("vproto")
    @JSONField(name = "vproto")
    public String vproto;
    @SerializedName("terminal_id")
    @JSONField(name = "terminal_id")
    public long terminalId;
    @SerializedName("seeds")
    @JSONField(name = "seeds")
    public String seeds;
    @SerializedName("password")
    @JSONField(name = "password")
    public String password;
    @SerializedName("vincpy")
    @JSONField(name = "vincpy")
    public long vincpy;
    @SerializedName("imeicpy")
    @JSONField(name = "imeicpy")
    public int imeicpy;
    @SerializedName("do_time")
    @JSONField(name = "do_time")
    public long doTime;
    @SerializedName("machine_status")
    @JSONField(name = "machine_status")
    public String machineStatus;
    @SerializedName("machine_type")
    @JSONField(name = "machine_type")
    public String machineType;
    @SerializedName("machine_kind")
    @JSONField(name = "machine_kind")
    public String machineKind;
}
