package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/3/15.
 */
public class UploadRouteHeaderParams {
    @SerializedName("description")
    public String mDescription;
    @SerializedName("startLat")
    public double mStartLat;
    @SerializedName("startLong")
    public double mStartLong;
    @SerializedName("endLat")
    public double mEndLat;
    @SerializedName("endLong")
    public double mEndLong;
    @SerializedName("startTime")
    public long mStartTime;
    @SerializedName("endTime")
    public long mEndTime;
    @SerializedName("mileage")
    public long mMileage;
    @SerializedName("maxSpeed")
    public float mMaxSpeed;
    @SerializedName("maxRpm")
    public long mMaxRpm;
}
