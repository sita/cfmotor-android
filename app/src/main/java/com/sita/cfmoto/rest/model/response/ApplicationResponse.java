package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.Inform;

import java.util.List;

/**
 * Created by mark on 2016/9/2.
 */

//"userid" -> "426454588787264608"
// "nick_name" -> "满满"
// "avatar" -> "http://123.206.201.243/yun/userinfo/avatar/426454588787264608/49459834872.jpg"

public class ApplicationResponse {
    @SerializedName("errorCode")
    public String mErrorCode;

    @SerializedName("data")
    public List<Inform> applicationList;

}
