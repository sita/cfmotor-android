package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/23.
 */
public class LoginResponseData {
    @SerializedName("account")
    public Account mAccount;

}
