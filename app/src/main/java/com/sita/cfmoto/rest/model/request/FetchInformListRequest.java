package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/9/2.
 */
/*
"size":10,
"page":0,
"userId":429372744237321320
  */
public class FetchInformListRequest {
    @SerializedName("size")
    public int size;

    @SerializedName("page")
    public int page;

    @SerializedName("user_id")
    public long userId;
}
