package com.sita.cfmoto.rest.model.response;

/**
 * Created by mark man on 2016/5/20.
 */

//"snid": "23345",
//        "sncpy": 1,
//        "gpsid": "11113",
//        "gpscpy": 1,
//        "sn_password": "password",
//        "controller_id": "B00G4U9FL3",
//        "controller_address": "C0:27:15:09:B0:C9",
//        "controller_password": "NRWOipVE+USOlfiotZC56g==",
//        "elet_controller_id": "elec23456",
//        "front_fork_id": "fork001",
//        "meter_id": "meter001",
//        "imsi": 600039287374,
//        "machine_status": 2,
//        "machine_type": 1,
//        "machine_kind": "Z3",
//        "create_time": 1451606400000

public class ListVehicleResponse {
    public String snid;
    public String sn_password;
//    public String controller_id;
    public String controller_password;
    public String controller_address;
}
