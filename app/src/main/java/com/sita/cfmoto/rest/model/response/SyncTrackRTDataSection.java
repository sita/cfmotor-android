package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by manhongyun on 15/11/12.
 */
public class SyncTrackRTDataSection implements Serializable {
    @SerializedName("points")
    public List<SyncTrackRTData> mPoints;

}
