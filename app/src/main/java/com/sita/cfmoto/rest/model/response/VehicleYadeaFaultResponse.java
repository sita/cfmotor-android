package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark man on 2016/5/20.
 */
//        "status": 0,
//        "electric": 0,
//        "controller": 0,
//        "turnedHandle": 0,
//        "brakeHandle": 0,
//        "atmosphereLamp": 0,
//        "nearLight": 0,
//        "beam": 0,
//        "drivingLamp": 0,
//        "brakeLight": 0,
//        "frontLeftTurn": 0,
//        "frontRightTurn": 0,
//        "backLeftTurn": 0,
//        "backRightTurn": 0,
//        "horn": 0,
//        "converter": 0,
//        "batteryOverCharge": 0,
//        "elecChipHighTemperature": 0,
//        "elecChipLowTemperature": 0,
//        "btCommunication": 0,
//        "gpsCommunication": 0,
//        "gsmNet": 0,
//        "radioFrequencyCommunication": 0,
//        "centerControllerCommunication": 0,
//        "controllerCommunication": 0,
//        "batteryCommunication": 0,
//        "powerManager": 0

public class VehicleYadeaFaultResponse {

    @SerializedName("status")
    public int status;
    @SerializedName("electric")
    public int electric;
    @SerializedName("controller")
    public int controller;
    @SerializedName("turnedHandle")
    public int turnedHandle;
    @SerializedName("brakeHandle")
    public int brakeHandle;
    @SerializedName("atmosphereLamp")
    public int atmosphereLamp;
    @SerializedName("nearLight")
    public int nearLight;
    @SerializedName("beam")
    public int beam;
    @SerializedName("drivingLamp")
    public int drivingLamp;
    @SerializedName("brakeLight")
    public int brakeLight;
    @SerializedName("frontLeftTurn")
    public int frontLeftTurn;
    @SerializedName("frontRightTurn")
    public int frontRightTurn;
    @SerializedName("backLeftTurn")
    public int backLeftTurn;
    @SerializedName("backRightTurn")
    public int backRightTurn;
    @SerializedName("horn")
    public int horn;
    @SerializedName("converter")
    public int converter;
    @SerializedName("batteryOverCharge")
    public int batteryOverCharge;
    @SerializedName("elecChipHighTemperature")
    public int elecChipHighTemperature;
    @SerializedName("elecChipLowTemperature")
    public int elecChipLowTemperature;
    @SerializedName("btCommunication")
    public int btCommunication;
    @SerializedName("gpsCommunication")
    public int gpsCommunication;
    @SerializedName("gsmNet")
    public int gsmNet;
    @SerializedName("radioFrequencyCommunication")
    public int radioFrequencyCommunication;
    @SerializedName("centerControllerCommunication")
    public int centerControllerCommunication;
    @SerializedName("controllerCommunication")
    public int controllerCommunication;
    @SerializedName("batteryCommunication")
    public int batteryCommunication;
    @SerializedName("powerManager")
    public int powerManager;

    public boolean isOk() {
        return (status == 0);
    }
}
