package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.RtResourceNoteList;

/**
 * Created by xiaodong on 16/2/18.
 */
public class RtResourceNoteListResponse extends Reply {
    @SerializedName("data")
    public RtResourceNoteList data;
}
