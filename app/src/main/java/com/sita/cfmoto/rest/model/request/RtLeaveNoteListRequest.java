package com.sita.cfmoto.rest.model.request;

/**
 * Created by xiaodong on 16/2/18.
 */
public class RtLeaveNoteListRequest {
    public String resId;
    public String pageNumber;
    public String pageSize;
}
