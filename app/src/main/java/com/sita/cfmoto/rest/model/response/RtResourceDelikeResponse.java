package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.RtResourceDelike;

/**
 * Created by xiaodong on 16/2/18.
 */
public class RtResourceDelikeResponse extends Reply {
    @SerializedName("data")
    public RtResourceDelike data;
}
