package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangde001 on 2015/12/10.
 */
public class Cover {
    @SerializedName("format")
    public String mFormat;
    @SerializedName("data")
    public String mData;
}
