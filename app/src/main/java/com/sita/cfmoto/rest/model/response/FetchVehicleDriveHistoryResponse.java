package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mark man on 2016/5/20.
 */
/*"routeId": "402540466274316000",
"lat": 31.582076,
"lng": 120.491976,
"begintime": 1468053308923,
"endtime": 1468056955142,
"distance": 25523.6536571406,
"speed": 25.2001191277063,
"status": 0,
"dotime": 0,
"statis_date": null,
"routefile": "http://123.206.201.243/yun/vehicleinfo/routes/20160709/434248137834508247.zip",
"terminal_id": 0,
"oil_wear": 34*/

public class FetchVehicleDriveHistoryResponse {

    @SerializedName("routeId")
    public String routeId;
    @SerializedName("lat")
    public float lat;
    @SerializedName("lng")
    public float lng;
    @SerializedName("begintime")
    public long begintime;
    @SerializedName("endtime")
    public long endtime;
    @SerializedName("distance")
    public float distance;
    @SerializedName("speed")
    public float speed;
    @SerializedName("oil_wear")
    public float oilWear;
    @SerializedName("routefile")
    public String routefile;

}
