package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/23.
 */
public class AddRequest {
//    "accountId": "245183991651501070",
//            "contactId": "245183391723422732"

    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("contactId")
    public String mContactId;

}
