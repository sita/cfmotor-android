package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangde001 on 2016/1/21.
 */
public class DynamicPersonRequest {
    @SerializedName("accountId")
    public String mAccountId;
    @SerializedName("location")
    public Location mLocation;
    @SerializedName("range")
    public long mRange;
    @SerializedName("startDate")
    public long mStartData;
    @SerializedName("pageNumber")
    public String mPageNumber;
    @SerializedName("pageSize")
    public String mPageSize;
}
