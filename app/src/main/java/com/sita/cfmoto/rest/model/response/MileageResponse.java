package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.MileageSummary;

/**
 * Created by xiaodong on 16/2/24.
 */
public class MileageResponse extends Reply {
    @SerializedName("data")
    public MileageSummary data;
}
