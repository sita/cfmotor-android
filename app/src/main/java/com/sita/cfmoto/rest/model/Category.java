package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangde001 on 2015/12/10.
 */
public class Category {


    //        "id": "243132660333937665"
//        "name": "体育"
//        "cover": {...}
    @SerializedName("id")
    public String mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("cover")
    public Cover mCover;
}
