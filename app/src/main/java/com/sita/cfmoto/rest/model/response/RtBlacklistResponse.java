package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xiaodong on 16/2/29.
 */
public class RtBlacklistResponse extends Reply{
    @SerializedName("data")
    public List<String> data;
}
