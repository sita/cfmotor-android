package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lijuan zhang on 2016/2/17.
 */
public class GroupCreateResponseData {
    //服务器的id
    @SerializedName("groupId")
    public String mGroupId;
    @SerializedName("name")
    public String mName;
    @SerializedName("admins")
    public List<Account> mAdmins;
    @SerializedName("members")
    public List<Account> mMember;
    @SerializedName("imGroupId")
    public String mImGroupId;
    @SerializedName("owner")
    public Account mOwner;
    @SerializedName("public")
    public boolean mPublic;
}
