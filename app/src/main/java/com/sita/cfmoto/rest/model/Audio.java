package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manhongyun on 15/11/13.
 */
public class Audio implements Serializable {
    @SerializedName("link")
    public String mUrl;
}
