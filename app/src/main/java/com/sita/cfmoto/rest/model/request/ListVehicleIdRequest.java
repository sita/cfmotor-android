package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by hongyun on 2016/4/5.
 */
//"sn":"23345",
//        "sncpy":1,
//        "snpassword":"password"
public class ListVehicleIdRequest {
    @JSONField(name = "vin")
    public String sn;
    @JSONField(name = "vincpy")
    public long sncpy;
    @JSONField(name = "passWord")
    public String snpassword;
}
