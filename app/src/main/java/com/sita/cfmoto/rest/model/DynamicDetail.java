package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zhangdepeng on 2016/1/26.
 */
public class DynamicDetail {
    @SerializedName("notes")
    public List<Notes> mNotes;

}
