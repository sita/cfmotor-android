package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mark on 2016/9/2.
 */

//"userid" -> "426454588787264608"
// "nick_name" -> "满满"
// "avatar" -> "http://123.206.201.243/yun/userinfo/avatar/426454588787264608/49459834872.jpg"

public class Member implements Serializable {
    @SerializedName("userid")
    public String userId;

    @SerializedName("nick_name")
    public String nickName;

    @SerializedName("avatar")
    public String avatar;
}

