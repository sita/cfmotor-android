package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijuan zhang on 2016/8/16.
 */

/*
* "accountId": "426454588787264608",
        "res_id": "428069551712767981",
        "message": "我的评论"
        */
public class ReleaseCommentRequest {
    @SerializedName("accountId")
    public String accountId;
    @SerializedName("res_id")
    public String resId;
    @SerializedName("message")
    public String message;
}
