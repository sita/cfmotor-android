package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by xiaodong on 16/2/24.
 */
public class MileageSummaryRanking implements Serializable {
    @SerializedName("ranking")
    public long ranking;
}
