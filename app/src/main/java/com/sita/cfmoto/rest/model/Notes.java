package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangde001 on 2016/1/27.
 */
public class Notes {
    @SerializedName("account")
    public Person mPerson;
    @SerializedName("message")
    public String mMessage;
    @SerializedName("createTime")
    public long mCreateTime;
}
