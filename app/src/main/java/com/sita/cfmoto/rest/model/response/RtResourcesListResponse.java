package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.RtResource;

import java.util.List;

public class RtResourcesListResponse extends Reply {
	@SerializedName("data")
	public List<RtResource> mData;
}
