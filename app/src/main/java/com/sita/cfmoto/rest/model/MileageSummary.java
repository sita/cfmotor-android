package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by xiaodong on 16/2/24.
 */
public class MileageSummary implements Serializable {
    @SerializedName("mileage")
    public Double mileage;
    @SerializedName("account")
    public Account account;
    @SerializedName("period")
    public Long period;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MileageSummary that = (MileageSummary) o;

        if (mileage != null ? !mileage.equals(that.mileage) : that.mileage != null) return false;
        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        return !(period != null ? !period.equals(that.period) : that.period != null);

    }

    @Override
    public int hashCode() {
        int result = mileage != null ? mileage.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (period != null ? period.hashCode() : 0);
        return result;
    }
}
