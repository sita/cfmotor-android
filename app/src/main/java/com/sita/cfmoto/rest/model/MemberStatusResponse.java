package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manhongyun on 15/11/21.
 */
public class MemberStatusResponse {
    @JSONField(name = "errorCode")
    @SerializedName("errorCode")
    public String mErrorCode;
    @JSONField(name = "data")
    @SerializedName("data")
    public Integer mData;
}
