package com.sita.cfmoto.rest.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hongyun on 2016/4/5.
 */

public class SetBatteryAlarmRequest {
    @SerializedName("snid")
    @JSONField(name = "snid")
    public String sn;
    @SerializedName("sncpy")
    @JSONField(name = "sncpy")
    public long sncpy;

    @SerializedName("soc")
    @JSONField(name = "soc")
    public int soc;

    @SerializedName("isalarm")
    @JSONField(name = "isalarm")
    public int enabled;
}
