package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

//
//        "accountId": "287253532740943873",
//        "name": "分组 1",
//        "description": "说明",
//        "memberIds": "287253620125073410,282787744755746399,272120661353170409",
//        "isPublic": true,
//        "maxUsersNumber": 100,
//        "approval": false
//
public class GroupCreateRequest {

    @SerializedName("accountId")
    public String mAccountId;

    @SerializedName("name")
    public String mName;

    @SerializedName("description")
    public String mDescription;

    @SerializedName("memberIds")
    public String mMemberIds;

    @SerializedName("isPublic")
    public boolean mIsPublic;

    @SerializedName("maxUsersNumber")
    public int mMaxUsersNumber;

    @SerializedName("approval")
    public boolean mApproval;
}
