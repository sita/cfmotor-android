package com.sita.cfmoto.rest.model;

import java.io.Serializable;

/**
 * Created by manhongyun on 15/11/12.
 */
public class Account implements Serializable {
    private String accountId;
    private String userName;
    private String mobile;
    private String email;
    private String avatar;
    private String nickName;
    private String birthday;
    private Integer gender;
    private String address;
    private String uniqueId;
    private String signature;
    private String imPassword;

    public String getImPassword() {
        return imPassword;
    }

    public Account setImPassword(String imPassword) {
        this.imPassword = imPassword;
        return this;
    }

    public String getSignature() {
        return signature;
    }

    public Account setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public Account setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public Account setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Account setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public Account setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Account setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public Account setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public String getNickName() {
        return nickName;
    }

    public Account setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }


    public String getAddress() {
        return address;
    }

    public Account setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getBirthday() {
        return birthday;
    }

    public Account setBirthday(String birthday) {
        this.birthday = birthday;
        return this;
    }

    public Integer getGender() {
        return gender;
    }

    public Account setGender(Integer gender) {
        this.gender = gender;
        return this;
    }
}
