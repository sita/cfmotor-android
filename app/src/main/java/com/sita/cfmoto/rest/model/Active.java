package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lijuan zhang on 2016/8/15.
 */
/*
 campaignid": "428159359881580543",
"userid": "425939193751931996",
"nick_name": "宋军-测试2",
"title": "428159359881580543",
"content": "本次活动描述",
"cover": "http://123.206.201.243/yun/userinfo/campaign/20160813/428159359881580543/48362173106.jpg",
"campaign_start_time": "2034-02-13",
"campaign_end_time": "2034-04-27",
"register_start_time": "2024-08-13",
"register_end_time": "2024-02-13",
"deptLng": "117.399433",
"deptLat": "39.161153",
"deptAddr": "天津市东丽区保税区空港国际物流经济区拿渡美食城(空港燕莎奥特莱斯店)天津新燕莎奥特莱斯",
"arriLng": "117.701734",
"arriLat": "37.699817",
"arriAddr": "山东省滨州市无棣县棣丰街道郭家村",
"maxMemberCount": 200,
"source": "0",
"state": null,
"createTime": "2016-08-13 17:56:13",
"members": 0,
"chatroom_id": "229855367788495276"
* */
public class Active implements Serializable {
    @SerializedName("campaignid")
    @JSONField(name = "campaignid")
    public String campaignId;

    @SerializedName("userid")
    @JSONField(name = "userid")
    public String userId;

    @SerializedName("nick_name")
    @JSONField(name = "nick_name")
    public String nickName;

    @SerializedName("title")
    @JSONField(name = "title")
    public String title;

    @SerializedName("content")
    @JSONField(name = "content")
    public String content;

    @SerializedName("cover")
    @JSONField(name = "cover")
    public String cover;

    @SerializedName("campaign_start_time")
    @JSONField(name = "campaign_start_time")
    public String campaignStartTime;

    @SerializedName("campaign_end_time")
    @JSONField(name = "campaign_end_time")
    public String campaignEndTime;

    @SerializedName("register_start_time")
    @JSONField(name = "register_start_time")
    public String registerStartTime;

    @SerializedName("register_end_time")
    @JSONField(name = "register_end_time")
    public String registerEndTime;

    @SerializedName("deptLng")
    @JSONField(name = "deptLng")
    public String deptLng;

    @SerializedName("deptLat")
    @JSONField(name = "deptLat")
    public String deptLat;

    @SerializedName("deptAddr")
    @JSONField(name = "deptAddr")
    public String deptAddr;

    @SerializedName("arriLng")
    @JSONField(name = "arriLng")
    public String arriLng;

    @SerializedName("arriLat")
    @JSONField(name = "arriLat")
    public String arriLat;

    @SerializedName("arriAddr")
    @JSONField(name = "arriAddr")
    public String arriAddr;

    @SerializedName("maxMemberCount")
    @JSONField(name = "maxMemberCount")
    public Long maxMemberCount;

    @SerializedName("source")
    @JSONField(name = "source")
    public String source;

    @SerializedName("state")
    @JSONField(name = "state")
    public String state;

    @SerializedName("createTime")
    @JSONField(name = "createTime")
    public String createTime;

    @SerializedName("members")
    @JSONField(name = "members")
    public Long members;

    @SerializedName("chatroom_id")
    @JSONField(name = "chatroom_id")
    public String chatroomId;
}
