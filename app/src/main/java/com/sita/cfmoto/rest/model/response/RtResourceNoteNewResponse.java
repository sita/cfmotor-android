package com.sita.cfmoto.rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.RtResourceNote;

import java.util.List;

/**
 * Created by xiaodong on 16/2/18.
 */
public class RtResourceNoteNewResponse extends Reply {
    @SerializedName("data")
    public List<RtResourceNote> data;
}
