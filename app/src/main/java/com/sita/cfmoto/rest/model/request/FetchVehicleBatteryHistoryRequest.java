package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hongyun on 2016/4/5.
 */

public class FetchVehicleBatteryHistoryRequest extends CfmotorCommonRequest {
    @SerializedName("pagesize")
    public int pageSize;
    @SerializedName("pagenumber")
    public int pageNumber;
}
