package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RtResourceListRequest implements Serializable{

    @SerializedName("accountId")
    public String mAccountId;
//    @SerializedName("location")
//    public Location mLocation;
    @SerializedName("endDate")
    public Long mEndDate;
    @SerializedName("startDate")
    public Long mStartDate;
    @SerializedName("onlyShowFriends")
    public boolean mOnlyShowFriends;
    @SerializedName("excludeBlacklist")
    public boolean mExcludeBlackList;
    @SerializedName("pageNumber")
    public String mPageNumber;
    @SerializedName("pageSize")
    public String mPageSize;

}
