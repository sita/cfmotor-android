package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/2/15.
 */
public class AdvertisementResponse {
    @SerializedName("errorCode")
    public String mErrorCode;
    @SerializedName("data")
    public Advertisement mData;
}
