package com.sita.cfmoto.rest.model.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hongyun on 2016/4/5.
 */

//"user_id":315346971655145122,
//        "sn_id":"23345",
//        "sn_cpy":1,
//        "sn_password":"password"

public class BindVehicleRequest implements Serializable {
    @SerializedName("user_id")
    public long userId;
    @SerializedName("vin_id")
    public String vinId;
    @SerializedName("vin_cpy")
    public long vinCpy;
    @SerializedName("vin_password")
    public String vinPassword;
}
