package com.sita.cfmoto.rest.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lijuan zhang on 2016/8/15.
 */
public class Comment implements Serializable {
    /**
     * accountId : 425939193751931996
     * postmsgId : 430980242123787512
     * message : 必须建设局
     * lastModified : 1471421416095
     * partition_id : 20160817
     * avatar : http://123.206.201.243/yun/userinfo/avatar/425939193751931996/48701486319.jpg
     * nickname : 哈哈
     * comment_id : 431005136316271868
     * create_time : 2016-08-17 16:10
     */
    @SerializedName("accountId")
    @JSONField(name = "accountId")
    public String accountId;

    @SerializedName("postmsgId")
    @JSONField(name = "postmsgId")
    public String postMsgId;
    @SerializedName("message")
    @JSONField(name = "message")
    public String message;
    @SerializedName("lastModified")
    @JSONField(name = "lastModified")
    public long lastModified;
    @SerializedName("partition_id")
    @JSONField(name = "partition_id")
    public String partitionId;
    @SerializedName("avatar")
    @JSONField(name = "avatar")
    public String avatar;
    @SerializedName("nickname")
    @JSONField(name = "nickname")
    public String nickName;
    @SerializedName("comment_id")
    @JSONField(name = "comment_id")
    public long commentId;
    @SerializedName("create_time")
    @JSONField(name = "create_time")
    public String createTime;


}
