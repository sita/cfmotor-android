package com.sita.cfmoto.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangdepeng on 2016/1/21.
 */
public class DynamicPersonPic {
    @SerializedName("link")
    public String mLink;
}
