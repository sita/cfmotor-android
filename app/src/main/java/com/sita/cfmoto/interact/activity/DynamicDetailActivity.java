package com.sita.cfmoto.interact.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.adapter.DynamicDetailAdapter;
import com.sita.cfmoto.rest.model.Comment;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.rest.model.Member;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.PersistUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DynamicDetailActivity extends AppCompatActivity {
    @Bind(R.id.comment_rv)
    SuperRecyclerView dynamicDetailCommentList;
    @Bind(R.id.comment_edit)
    EditText commentContent;
    private LinearLayoutManager linearLayoutManager;
    private DynamicDetailAdapter dynamicDetailAdapter;
    private List<String> list;
    private int size = 10;
    private int page = 0;
    private String postMsgId;
    private Dynamic dynamic;
    private boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_detail);
        ButterKnife.bind(this);
        dynamic = (Dynamic) getIntent().getSerializableExtra("dynamic");
        postMsgId = dynamic.msgId;
        initDynamicDetail();
        //根据动态id 获取动态的信息
        doRefreshDynamic();
        //再根据动态id请求评论数
        doRefreshComment();
        doRefreshLike();
    }

    @OnClick(R.id.cancel)
    void onClickBack() {
        this.finish();
    }

    @OnClick(R.id.release_btn)
    void onClickReleaseComment() {
        final MaterialDialog uploadDialog = new MaterialDialog.
                Builder(this).
                content("正在发布...").
                progress(true, 5000).
                build();
        uploadDialog.show();
        String accountId = PersistUtils.getUsers().get(0).getAccountId().toString();
        String resId = postMsgId;
        String content = commentContent.getText().toString();
        InteractUtils.releaseComment(accountId, resId, content, new InteractUtils.ReleaseCommentCallback() {
            @Override
            public void onCommentReleased(boolean b) {
                if (b) {
                    Toast.makeText(DynamicDetailActivity.this, "发布成功", Toast.LENGTH_SHORT).show();
                    commentContent.setText("");
                    //根据动态ID刷新动态详情界面
                    doRefreshDynamic();
                    doRefreshComment();
                }
                uploadDialog.dismiss();
            }
        });
    }

    private void initDynamicDetail() {
        linearLayoutManager = new LinearLayoutManager(this);
        dynamicDetailCommentList.setLayoutManager(linearLayoutManager);
        dynamicDetailAdapter = new DynamicDetailAdapter(this);
        dynamicDetailCommentList.setAdapter(dynamicDetailAdapter);
        dynamicDetailCommentList.getRecyclerView().setItemAnimator(null);
        dynamicDetailAdapter.setDynamicDetailItem(dynamic);
        dynamicDetailCommentList.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                page++;
                InteractUtils.fetchCommentList(size, page, postMsgId, new InteractUtils.FetchCommentListCallback() {
                    @Override
                    public void onCommentListFetched(List<Comment> commentList) {
                        if (commentList != null && !commentList.isEmpty()) {
                            dynamicDetailAdapter.appendCommentData(commentList);
                        } else {
                            page--;
                        }
                    }
                });
            }
        }, 1);
        dynamicDetailCommentList.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isRefresh) {
                    isRefresh = true;
                    page = 0;
                    doRefreshDynamic();
                    doRefreshComment();
                    doRefreshLike();
                }
            }
        });

        dynamicDetailAdapter.setOnLikeStatusChangedListener(new DynamicDetailAdapter.OnLikeStatusChangedListener() {
            @Override
            public void onLikeStatusChanged(boolean isChanged) {
                if (isChanged) {
                    doRefreshLike();
                }
            }
        });
        dynamicDetailAdapter.setOnLikeMoreClickListener(new DynamicDetailAdapter.OnLikeMoreClickListener() {
            @Override
            public void onLikeMoreClick(List<Member> memberList) {
                if (memberList != null && !memberList.isEmpty()) {
                    startActivity(MemberListActivity.newIntent(DynamicDetailActivity.this, (ArrayList) memberList,true));
                }
            }
        });
    }

    private void doRefreshComment() {
        //包括获取评论详情和点赞详情，然后更新三个部分
        //再根据动态id请求评论数
        InteractUtils.fetchCommentList(size, 0, postMsgId, new InteractUtils.FetchCommentListCallback() {
            @Override
            public void onCommentListFetched(List<Comment> commentList) {
                dynamicDetailCommentList.setRefreshing(false);
                isRefresh = false;
                if (commentList != null && !commentList.isEmpty()) {
                    dynamicDetailAdapter.setCommentData(commentList);
                }
            }
        });
    }

    private void doRefreshLike() {
        //请求点赞人员列表
        InteractUtils.fetchLikeList(Long.parseLong(postMsgId), new InteractUtils.FetchLikeListCallback() {
            @Override
            public void onLikeListFetched(List<Member> likeMemberList) {
                dynamicDetailCommentList.setRefreshing(false);
                if (likeMemberList != null) {
                    dynamicDetailAdapter.setLikeMemberData(likeMemberList);
                }
            }
        });
    }

    private void doRefreshDynamic() {
        //请求动态，主要为了更新评论数和点赞数
        InteractUtils.fetchDynamic(Long.parseLong(postMsgId), new InteractUtils.FetchDynamicCallback() {
            @Override
            public void onDynamicFetched(Dynamic dynamic) {
                dynamicDetailCommentList.setRefreshing(false);
                if (dynamic != null) {
                    dynamicDetailAdapter.setDynamicDetailItem(dynamic);
                }
            }
        });
    }

}
