package com.sita.cfmoto.interact.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.adapter.TopicListAdapter;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.LocalStorage;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TopicListActivity extends AppCompatActivity {
    private final long REFRESH_START = 1420070400000l;
    @Bind(R.id.topic_list)
    SuperRecyclerView topicListView;
    private TopicListAdapter topicListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private int size = 10;
    private int currentPage = 0;
    private Long minId = 0L;
    private String topicId;
    private Integer topicPosition;
    private Boolean isRequestNewDataMore = false;
    private Boolean isRequestRefresh = true;
    private String moreMaxId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_list);
        ButterKnife.bind(this);
        LocalStorage.setTopicListRefreshTime(REFRESH_START);
        initTopicList();
    }


    @OnClick({R.id.cancel})
    public void onClickBack() {
        this.finish();
    }

    private void initTopicList() {
        linearLayoutManager = new LinearLayoutManager(this);
        topicListView.setLayoutManager(linearLayoutManager);
        topicListAdapter = new TopicListAdapter();
        topicListView.setAdapter(topicListAdapter);
        //设置分割线
        HorizontalDividerItemDecoration dividerItemDecoration = new HorizontalDividerItemDecoration.Builder(this)
                .color(getResources().getColor(R.color.common_background))
                .size(20)
                .showLastDivider()
                .build();
        topicListView.addItemDecoration(dividerItemDecoration);
        requestData();
        topicListView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                moreMaxId = topicListAdapter.getLastItemId();
                isRequestRefresh = false;
                requestData();
            }
        }, 1);
        topicListView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRequestRefresh = true;
                requestData();
            }
        });
        topicListAdapter.setOnItemClickListener(new TopicListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Topic topic) {
                //保存点击item的话题ID
                topicId = topic.topicId;
                topicPosition = position;
                //跳转到活动详情
                Intent intent = new Intent();
                intent.setClass(TopicListActivity.this, TopicDetailActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });
    }

    private void requestData() {
        if (isRequestRefresh) {
            //刷新
            long beginTime = LocalStorage.getTopicListRefreshTime();
            long endTime = System.currentTimeMillis();
            InteractUtils.fetchTopicList(0, size, 0, beginTime, endTime, null, minId, new InteractUtils.FetchTopicListCallback() {
                @Override
                public void onTopicListFetched(List<Topic> topicList) {
                    if (topicList != null && !topicList.isEmpty()) {
                        minId = Long.parseLong(topicList.get(0).topicId);
                        if (topicList.size() == size) {
                            topicListAdapter.setNewData(topicList);
                            isRequestNewDataMore = true;
                        } else {
                            topicListAdapter.setData(topicList);
                            isRequestNewDataMore = false;
                        }
                    } else {
                        topicListView.setRefreshing(false);
                    }
                }
            });
        } else {
            //更多
            if (isRequestNewDataMore) {
                String minId = topicListAdapter.getStorageFirstId();
                InteractUtils.fetchTopicList(1, size, 0, null, null, moreMaxId == null ? null : Long.parseLong(moreMaxId), (minId == null ? null : Long.parseLong(minId)), new InteractUtils.FetchTopicListCallback() {
                    @Override
                    public void onTopicListFetched(List<Topic> topicList) {
                        if (topicList != null) {
                            if (topicList.size() == size) {
                                topicListAdapter.appendData(topicList);
                            } else if (topicList.size() < size) {
                                topicListAdapter.appendData(topicList);
                                topicListAdapter.addStorageData();
                            }
                        }
                    }
                });
            } else {
                InteractUtils.fetchTopicList(1, size, 0, null, null, moreMaxId == null ? null : Long.parseLong(moreMaxId), null, new InteractUtils.FetchTopicListCallback() {
                    @Override
                    public void onTopicListFetched(List<Topic> topicList) {
                        if (topicList != null && !topicList.isEmpty()) {
                            topicListAdapter.appendData(topicList);
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //更新刚才查看的那条动态的数目
        if (topicId != null && topicPosition != null) {
            //根据话题id 获取话题信息
            InteractUtils.fetchTopic(topicId, new InteractUtils.FetchTopicCallback() {
                @Override
                public void onTopicFetched(Topic topic) {
                    if (topic != null) {
                        topicListAdapter.refreshOneTopicCount(topic, topicPosition);
                    }
                }
            });
        }
    }
}

