package com.sita.cfmoto.interact.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.adapter.DynamicListAdapter;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.LocalStorage;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DynamicListActivity extends AppCompatActivity {
    @Bind(R.id.dynamic_list)
    SuperRecyclerView dynamicListView;
    private LinearLayoutManager linearLayoutManager;
    private DynamicListAdapter dynamicListAdapter;
    private int size = 10;
    private int currentPage = 0;
    private Long minId = 0L;
    private int source;
    private String dynamicId;
    private Integer dynamicPosition;
    private Boolean isRequestNewDataMore = false;
    private Boolean isRequestRefresh = true;
    private String moreMaxId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_list);
        ButterKnife.bind(this);
        source = getIntent().getIntExtra("source", 0);
        LocalStorage.setDynamicListRefreshTime(Constants.REFRESH_START);
        initDynamicList();
    }


    @OnClick({R.id.cancel})
    public void onClickBack() {
        this.finish();
    }

    private void initDynamicList() {
        linearLayoutManager = new LinearLayoutManager(this);
        dynamicListView.setLayoutManager(linearLayoutManager);
        dynamicListAdapter = new DynamicListAdapter();
        dynamicListView.setAdapter(dynamicListAdapter);
        //设置分割线
        HorizontalDividerItemDecoration dividerItemDecoration = new HorizontalDividerItemDecoration.Builder(this)
                .color(getResources().getColor(R.color.common_background))
                .size(20)
                .showLastDivider()
                .build();
        dynamicListView.addItemDecoration(dividerItemDecoration);
        requestData();
        dynamicListView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRequestRefresh = true;
                requestData();
            }
        });
        dynamicListView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                moreMaxId = dynamicListAdapter.getLastItemId();
                isRequestRefresh = false;
                requestData();
            }
        }, 1);
        dynamicListAdapter.setOnItemClickListener(new DynamicListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Dynamic dynamic) {
                //保存点击item的动态ID
                dynamicId = dynamic.msgId;
                dynamicPosition = position;
                //跳转到活动详情
                Intent intent = new Intent();
                intent.setClass(DynamicListActivity.this, DynamicDetailActivity.class);
                intent.putExtra("dynamic", dynamic);
                startActivity(intent);
            }
        });
    }

    private void requestData() {
        if (isRequestRefresh) {
            //刷新
            long beginTime = LocalStorage.getDynamicListRefreshTime();
            long endTime = System.currentTimeMillis();
            InteractUtils.fetchDynamicList(0, size, 0, null, beginTime, endTime, null, minId, source, 2, new InteractUtils.FetchDynamicListCallback() {
                @Override
                public void onDynamicListFetched(List<Dynamic> dynamicList) {
                    if (dynamicList != null && !dynamicList.isEmpty()) {
                        minId = Long.parseLong(dynamicList.get(0).msgId);
                        if (dynamicList.size() == size) {
                            dynamicListAdapter.setNewData(dynamicList);
                            isRequestNewDataMore = true;
                        } else {
                            dynamicListAdapter.setData(dynamicList);
                            isRequestNewDataMore = false;
                        }
                    } else {
                        dynamicListView.setRefreshing(false);
                    }
                }
            });
        } else {
            //更多
            if (isRequestNewDataMore) {
                String minId = dynamicListAdapter.getStorageFirstId();
                InteractUtils.fetchDynamicList(2, size, 0, null, null, null, moreMaxId == null ? null : Long.parseLong(moreMaxId), (minId == null ? null : Long.parseLong(minId)), source, 2, new InteractUtils.FetchDynamicListCallback() {
                    @Override
                    public void onDynamicListFetched(List<Dynamic> dynamicList) {
                        if (dynamicList != null) {
                            if (dynamicList.size() == size) {
                                dynamicListAdapter.appendData(dynamicList);
                            } else if (dynamicList.size() < size) {
                                dynamicListAdapter.appendData(dynamicList);
                                dynamicListAdapter.addStorageData();
                            }
                        }

                    }
                });
            } else {
                InteractUtils.fetchDynamicList(2, size, 0, null, null, null, moreMaxId == null ? null : Long.parseLong(moreMaxId), null, source, 2, new InteractUtils.FetchDynamicListCallback() {
                    @Override
                    public void onDynamicListFetched(List<Dynamic> dynamicList) {
                        if (dynamicList != null && !dynamicList.isEmpty()) {
                            dynamicListAdapter.appendData(dynamicList);
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //更新刚才查看的那条动态的数目
        if (dynamicId != null && dynamicPosition != null) {
            //根据动态id 获取动态信息
            InteractUtils.fetchDynamic(Long.parseLong(dynamicId), new InteractUtils.FetchDynamicCallback() {
                @Override
                public void onDynamicFetched(Dynamic dynamic) {
                    if (dynamic != null) {
                        dynamicListAdapter.refreshOneDynamicCount(dynamic, dynamicPosition);
                    }
                }
            });
        }
    }
}
