package com.sita.cfmoto.interact.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Member;
import com.sita.cfmoto.support.GlobalContext;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by lijuan zhang on 2016/9/5.
 */
public class LikeMemberListAdapter extends RecyclerView.Adapter<LikeMemberListAdapter.ViewHolder> {

    private List<Member> data = new ArrayList<>();

    public void setData(List<Member> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyItemRangeChanged(0, data.size());
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_like_member, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //数据绑定
        Member likeMember = data.get(position);

        Picasso.with(GlobalContext.getGlobalContext())
                .load(likeMember.avatar)
                .placeholder(R.drawable.dynamic_default_icon)
                .centerCrop().fit()
                .into(holder.avatar);

    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView avatar;

        public ViewHolder(View view) {
            super(view);
            avatar = (CircleImageView) view.findViewById(R.id.avatar);
        }
    }

}
