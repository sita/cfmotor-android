package com.sita.cfmoto.interact.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.support.GlobalContext;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lijuan zhang on 2016/8/4.
 */
public class PersonPopularRecyclerAdapter extends RecyclerView.Adapter<PersonPopularRecyclerAdapter.ViewHolder> {

    private OnItemClickListener itemClickListener;
    private List<Dynamic> data = new ArrayList<>();

    public PersonPopularRecyclerAdapter() {
    }

    public void setData(List<Dynamic> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();

    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person_popular_recycler, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //数据绑定
        if (data.size() > position) {
            Dynamic dynamic = data.get(position);
            if (dynamic.picList == null) {
                holder.personPopularImg.setImageResource(R.drawable.dynamic_default_icon);
            } else {
                Picasso.with(GlobalContext.getGlobalContext())
                        .load(dynamic.picList.get(0))
                        .placeholder(R.drawable.dynamic_default_icon)
                        .centerCrop().fit()
                        .into(holder.personPopularImg);
            }
            holder.personPopularImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null)
                        itemClickListener.onItemClick(v, position, dynamic);
                }
            });
        } else {
            holder.personPopularImg.setImageResource(R.drawable.dynamic_default_icon);
        }

    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return 6;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Dynamic dynamic);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView personPopularImg;

        public ViewHolder(View view) {
            super(view);
            personPopularImg = (ImageView) view.findViewById(R.id.person_popular_item_img);
        }
    }

}
