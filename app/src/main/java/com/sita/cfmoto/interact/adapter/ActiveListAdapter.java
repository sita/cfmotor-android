package com.sita.cfmoto.interact.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.utils.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lijuan zhang on 2016/8/5.
 */
public class ActiveListAdapter extends RecyclerView.Adapter<ActiveListAdapter.ViewHolder> {

    private OnItemClickListener itemClickListener;
    private List<Active> data = new ArrayList<>();
    private List<Active> storageDataList = new ArrayList<>();

    public ActiveListAdapter() {
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setData(List<Active> data) {
        List<Active> list = new ArrayList<>();
        list.addAll(data);
        list.addAll(this.data);
        this.data = list;
        notifyItemRangeInserted(0, data.size());
        notifyDataSetChanged();
    }

    public void setNewData(List<Active> data) {
        storageDataList.addAll(this.data);
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void addStorageData() {
        if (!storageDataList.isEmpty()) {
            int currCount = getItemCount();
            this.data.addAll(storageDataList);
            notifyItemRangeInserted(currCount, storageDataList.size());
            storageDataList.clear();
        }
    }

    public void appendData(List<Active> data) {
        int currCount = getItemCount();
        this.data.addAll(data);
        notifyItemRangeInserted(currCount, data.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE.ITEM_TYPE_LEFT.ordinal()) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_active_list_left, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_active_list_right, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //数据绑定
        Active active = data.get(position);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null)
                    itemClickListener.onItemClick(v, position, active);
            }
        });
        Picasso.with(GlobalContext.getGlobalContext())
                .load((active.cover.isEmpty() ? null : active.cover))
                .placeholder(R.drawable.dynamic_default_rect_bg)
                .centerCrop()
                .fit()
                .into(holder.activeImg);
        holder.activeOrgan.setText(active.title);
        holder.activeName.setText(active.nickName);
        holder.activeTime.setText(GlobalContext.getGlobalContext().getString(R.string.active_time,
                DateUtils.shortDate(active.campaignStartTime),
                DateUtils.shortDate(active.campaignEndTime)));
    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return data.size();
    }

    public Active getItem(int position) {
        if (position < getItemCount()) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0 ? ITEM_TYPE.ITEM_TYPE_LEFT.ordinal() : ITEM_TYPE.ITEM_TYPE_RIGHT.ordinal();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static enum ITEM_TYPE {
        ITEM_TYPE_LEFT,
        ITEM_TYPE_RIGHT
    }

    public String getLastItemId() {
        return data.get(data.size() - 1).campaignId;
    }

    public String getStorageFirstId() {
        if (storageDataList != null && !storageDataList.isEmpty()) {
            return storageDataList.get(0).campaignId;
        } else {
            return null;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Active active);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout itemLayout;
        public ImageView activeImg;
        public TextView activeOrgan;
        public TextView activeName;
        public TextView activeTime;

        public ViewHolder(View view) {
            super(view);
            itemLayout = (RelativeLayout) view.findViewById(R.id.item_layout);
            activeImg = (ImageView) view.findViewById(R.id.active_img);
            activeOrgan = (TextView) view.findViewById(R.id.active_organ);
            activeName = (TextView) view.findViewById(R.id.active_name);
            activeTime = (TextView) view.findViewById(R.id.active_time);
        }
    }

}