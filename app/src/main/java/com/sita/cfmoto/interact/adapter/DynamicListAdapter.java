package com.sita.cfmoto.interact.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.ui.view.ResizableImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by lijuan zhang on 2016/8/5.
 */
public class DynamicListAdapter extends RecyclerView.Adapter<DynamicListAdapter.ViewHolder> {
    private OnItemClickListener itemClickListener;
    private List<Dynamic> data = new ArrayList<>();
    private List<Dynamic> storageDataList = new ArrayList<>();

    public DynamicListAdapter() {
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setData(List<Dynamic> data) {
        List<Dynamic> list = new ArrayList<>();
        list.addAll(data);
        list.addAll(this.data);
        this.data = list;
        notifyItemRangeInserted(0, data.size());
        notifyDataSetChanged();
    }

    public void setNewData(List<Dynamic> data) {
        storageDataList.addAll(this.data);
        this.data.clear();
        this.data.addAll(data);
//        notifyItemRangeChanged(0, data.size());
        notifyDataSetChanged();
    }

    public void addStorageData() {
        if (!storageDataList.isEmpty()) {
            int currCount = getItemCount();
            this.data.addAll(storageDataList);
            notifyItemRangeInserted(currCount, storageDataList.size());
            storageDataList.clear();
        }
    }

    public void appendData(List<Dynamic> data) {
        int currCount = getItemCount();
        this.data.addAll(data);
        notifyItemRangeInserted(currCount, data.size());
    }

    public void refreshOneDynamicCount(Dynamic dynamic, int position) {
        this.data.set(position, dynamic);
        notifyItemRangeChanged(position, 1);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dynamic_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //数据绑定
        Dynamic dynamic = data.get(position);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null)
                    itemClickListener.onItemClick(v, position, dynamic);
            }
        });
        Picasso.with(GlobalContext.getGlobalContext()).load(dynamic.accountIdAvat).centerCrop().placeholder(R.drawable.default_head_icon).fit()
                .into(holder.userAvatar);
        if (dynamic.source == 0) {
            Picasso.with(GlobalContext.getGlobalContext()).load((dynamic.picList == null || dynamic.picList.size() == 0 ? null : dynamic.picList.get(0))).centerCrop()
                    .placeholder(R.drawable.dynamic_default_icon).fit()
                    .into(holder.dynamicImg);
        } else if (dynamic.source == 1) {
            Picasso.with(GlobalContext.getGlobalContext()).load((dynamic.picList == null || dynamic.picList.size() == 0 ? null : dynamic.picList.get(0)))
                    .placeholder(R.drawable.official_dynamic_default_icon)
                    .into(holder.dynamicImg);
        }


        holder.userName.setText(dynamic.accountIdNickname);
        holder.dynamicTime.setText(dynamic.createTime);
        holder.dynamicContent.setText(dynamic.content);
        holder.dynamicLocation.setText(dynamic.address);
        holder.likeNum.setText(String.valueOf(dynamic.praiseCount));
        holder.commentNum.setText(String.valueOf(dynamic.commentCount));
    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return data.size();
    }

    public Dynamic getItem(int position) {
        if (position < getItemCount()) {
            return data.get(position);
        } else {
            return null;
        }
    }

    public String getLastItemId() {
        return data.get(data.size() - 1).msgId;
    }

    public String getStorageFirstId() {
        if (storageDataList != null && !storageDataList.isEmpty()) {
            return storageDataList.get(0).msgId;
        } else {
            return null;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Dynamic dynamic);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout itemLayout;
        public CircleImageView userAvatar;
        public TextView userName;
        public TextView dynamicTime;
        public TextView dynamicContent;
        public ResizableImageView dynamicImg;
        public TextView likeNum;
        public TextView commentNum;
        public ImageView share;
        public TextView dynamicLocation;


        public ViewHolder(View view) {
            super(view);
            itemLayout = (RelativeLayout) view.findViewById(R.id.item_layout);
            userAvatar = (CircleImageView) view.findViewById(R.id.user_avatar);
            userName = (TextView) view.findViewById(R.id.user_name);
            dynamicTime = (TextView) view.findViewById(R.id.dynamic_time);
            dynamicContent = (TextView) view.findViewById(R.id.dynamic_content_txt);
            dynamicImg = (ResizableImageView) view.findViewById(R.id.dynamic_content_img);
            likeNum = (TextView) view.findViewById(R.id.like_num);
            commentNum = (TextView) view.findViewById(R.id.comment_num);
            share = (ImageView) view.findViewById(R.id.share);
            dynamicLocation = (TextView) view.findViewById(R.id.dynamic_location);

        }
    }

}