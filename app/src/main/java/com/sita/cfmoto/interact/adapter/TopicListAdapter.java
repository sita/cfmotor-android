package com.sita.cfmoto.interact.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.ui.view.ResizableImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lijuan zhang on 2016/8/5.
 */
public class TopicListAdapter extends RecyclerView.Adapter<TopicListAdapter.ViewHolder> {
    private OnItemClickListener itemClickListener;
    private List<Topic> data = new ArrayList<>();
    private List<Topic> storageDataList = new ArrayList<>();

    public TopicListAdapter() {
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setData(List<Topic> data) {
        List<Topic> list = new ArrayList<>();
        list.addAll(data);
        list.addAll(this.data);
        this.data = list;
        notifyItemRangeInserted(0, data.size());
        notifyDataSetChanged();
    }

    public void setNewData(List<Topic> data) {
        storageDataList.addAll(this.data);
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void addStorageData() {
        if (!storageDataList.isEmpty()) {
            int currCount = getItemCount();
            this.data.addAll(storageDataList);
            notifyItemRangeInserted(currCount, storageDataList.size());
            storageDataList.clear();
        }
    }

    public void setMyTopicData(List<Topic> data){
        this.data.clear();
        this.data.addAll(data);
        notifyItemRangeChanged(0, data.size());
    }
    public void appendData(List<Topic> data) {
        int currCount = getItemCount();
        this.data.addAll(data);
        notifyItemRangeInserted(currCount, data.size());
    }
    public void refreshOneTopicCount(Topic topic,int position){
        this.data.set(position,topic);
        notifyItemRangeChanged(position,1);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //数据绑定
        Topic topic = data.get(position);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null)
                    itemClickListener.onItemClick(v, position, topic);
            }
        });
        Picasso.with(GlobalContext.getGlobalContext()).load(topic.pic).placeholder(R.drawable.dynamic_default_icon)
                .into(holder.topicImg);
        holder.topicName.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_name, topic.author.nickName, topic.subject));
        holder.topicCount.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_count, topic.joinedCounnt));

    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return data.size();
    }

    public Topic getItem(int position) {
        if (position < getItemCount()) {
            return data.get(position);
        } else {
            return null;
        }
    }

    public String getLastItemId() {
        return data.get(data.size() - 1).topicId;
    }

    public String getStorageFirstId() {
        if (storageDataList != null && !storageDataList.isEmpty()) {
            return storageDataList.get(0).topicId;
        } else {
            return null;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Topic topic);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout itemLayout;
        public ResizableImageView topicImg;
        public TextView topicName;
        public TextView topicCount;


        public ViewHolder(View view) {
            super(view);
            itemLayout = (RelativeLayout) view.findViewById(R.id.item_layout);
            topicImg = (ResizableImageView) view.findViewById(R.id.topic_img);
            topicName = (TextView) view.findViewById(R.id.topic_name);
            topicCount = (TextView) view.findViewById(R.id.topic_count);
        }
    }

}