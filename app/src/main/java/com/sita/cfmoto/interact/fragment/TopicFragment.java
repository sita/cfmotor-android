package com.sita.cfmoto.interact.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.activity.TopicDetailActivity;
import com.sita.cfmoto.interact.activity.TopicListActivity;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.utils.BeanUtils;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.PersistUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TopicFragment extends Fragment {
    @Bind(R.id.topic_fragment_refresh)
    SwipeRefreshLayout refreshTopic;
    @Bind(R.id.topic_img_01)
    ImageView topicImg01;
    @Bind(R.id.topic_img_02)
    ImageView topicImg02;
    @Bind(R.id.topic_img_03)
    ImageView topicImg03;
    @Bind(R.id.topic_name_01)
    TextView topicName01;
    @Bind(R.id.topic_name_02)
    TextView topicName02;
    @Bind(R.id.topic_name_03)
    TextView topicName03;
    @Bind(R.id.topic_count_01)
    TextView topicCount01;
    @Bind(R.id.topic_count_02)
    TextView topicCount02;
    @Bind(R.id.topic_count_03)
    TextView topicCount03;
    private Topic topic01, topic02, topic03;
    private boolean isRefresh = false;
    private String topicId;
    private TextView currentTopicCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_topic, container, false);
        ButterKnife.bind(this, view);
        initTopic();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshTopic.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isRefresh) {
                    isRefresh = true;
                    initTopic();
                }
            }
        });
    }

    @OnClick({R.id.topic_first_layout})
    void onClickTopic01() {
        if (topic01 == null) return;
        topicId = topic01.topicId;
        currentTopicCount = topicCount01;
        Intent intent = new Intent();
        intent.setClass(getActivity(), TopicDetailActivity.class);
        intent.putExtra("topic", topic01);
        startActivity(intent);
    }

    @OnClick({R.id.topic_second_layout})
    void onClickTopic02() {
        if (topic02 == null) return;
        topicId = topic02.topicId;
        currentTopicCount = topicCount02;
        Intent intent = new Intent();
        intent.setClass(getActivity(), TopicDetailActivity.class);
        intent.putExtra("topic", topic02);
        startActivity(intent);
    }

    @OnClick({R.id.topic_third_layout})
    void onClickTopic03() {
        if (topic03 == null) return;
        topicId = topic03.topicId;
        currentTopicCount = topicCount03;
        Intent intent = new Intent();
        intent.setClass(getActivity(), TopicDetailActivity.class);
        intent.putExtra("topic", topic03);
        startActivity(intent);
    }

    @OnClick(R.id.topic_more)
    void onClickMore() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), TopicListActivity.class);
        startActivity(intent);
    }


    private void initTopic() {
        InteractUtils.fetchTopicList(1, 3, 0, null, null, null, null, new InteractUtils.FetchTopicListCallback() {
            @Override
            public void onTopicListFetched(List<Topic> topicList) {
                refreshTopic.setRefreshing(false);
                isRefresh = false;
                if (topicList == null) {
                    topicList = new ArrayList<>();
                }
                if (topicList.isEmpty()) {
                    List<com.sita.cfmoto.persistence.Topic> allTopic = PersistUtils.getTopic(3);
                    Iterator<com.sita.cfmoto.persistence.Topic> iterator = allTopic.iterator();
                    while (iterator.hasNext()) {
                        com.sita.cfmoto.persistence.Topic topic = iterator.next();
                        topicList.add(BeanUtils.copyProperties(topic));
                    }
                }
                if (topicList.size() == 0) {
                    return;
                }
                topic01 = topicList.get(0);
                topic02 = topicList.size() > 1 ? topicList.get(1) : topic01;
                topic03 = topicList.size() > 2 ? topicList.get(2) : topic02;
                Picasso.with(GlobalContext.getGlobalContext()).load(topic01.pic)
                        .placeholder(R.drawable.dynamic_default_icon).into(topicImg01);
                topicName01.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_name, topic01.author.nickName, topic01.subject));
                topicCount01.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_count, topic01.joinedCounnt));

                Picasso.with(GlobalContext.getGlobalContext()).load(topic02.pic)
                        .placeholder(R.drawable.dynamic_default_icon).into(topicImg02);
                topicName02.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_name, topic02.author.nickName, topic02.subject));
                topicCount02.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_count, topic02.joinedCounnt));

                Picasso.with(GlobalContext.getGlobalContext()).load(topic03.pic)
                        .placeholder(R.drawable.dynamic_default_icon).into(topicImg03);
                topicName03.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_name, topic03.author.nickName, topic03.subject));
                topicCount03.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_count, topic03.joinedCounnt));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (topicId != null) {
            //根据topicId更新topic的参与人数
            //根据话题id 获取话题信息
            InteractUtils.fetchTopic(topicId, new InteractUtils.FetchTopicCallback() {
                @Override
                public void onTopicFetched(Topic topic) {
                    if (topic != null && currentTopicCount != null) {
                        currentTopicCount.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_count, topic.joinedCounnt));
                    }
                    topicId = null;
                    currentTopicCount = null;
                }
            });

        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
