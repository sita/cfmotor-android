package com.sita.cfmoto.interact.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.ui.view.ResizableImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by lijuan zhang on 2016/8/20.
 */
public class TopicDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private OnItemClickListener itemClickListener;
    private List<Dynamic> dynamicData = new ArrayList<>();
    private Topic topic;
    private int TOPIC = 0;
    private int DYNAMIC = 1;


    public TopicDetailAdapter() {
    }

    public void setTopicDetailItem(Topic topic) {
        this.topic = topic;
        notifyItemRangeChanged(0, 1);
    }

    public void setDynamicData(List<Dynamic> data) {
        List<Dynamic> list = new ArrayList<>();
        list.addAll(data);
        list.addAll(this.dynamicData);
        this.dynamicData = list;
//        dynamicData.clear();
//        dynamicData.addAll(data);
        notifyItemRangeInserted(1, data.size());
    }

    public void appendTopicData(List<Dynamic> data) {
        int currCount = getItemCount();
        this.dynamicData.addAll(data);
        notifyItemRangeInserted(currCount, data.size());
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == TOPIC) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic_detail, parent, false);
            return new TopicViewHolder(view);
        } else if (viewType == DYNAMIC) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dynamic_detail, parent, false);
            return new DynamicViewHolder(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TOPIC;
        } else {
            return DYNAMIC;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TopicViewHolder) {
            //话题详情
            Picasso.with(GlobalContext.getGlobalContext()).load(topic.pic).placeholder(R.drawable.dynamic_default_icon)
                    .into(((TopicViewHolder) holder).topicImg);
            ((TopicViewHolder) holder).topicName.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_name, topic.author.nickName, topic.subject));
            ((TopicViewHolder) holder).topicCount.setText(GlobalContext.getGlobalContext().getString(R.string.topic_detail_count, topic.joinedCounnt));
            ((TopicViewHolder) holder).topicContent.setText(topic.content);
        } else if (holder instanceof DynamicViewHolder) {
            Dynamic dynamic = dynamicData.get(position - 1);
            Picasso.with(GlobalContext.getGlobalContext()).load(dynamic.accountIdAvat).centerCrop().placeholder(R.drawable.default_head_icon).fit()
                    .into(((DynamicViewHolder) holder).userAvatar);
            if (dynamic.source == 0) {
                Picasso.with(GlobalContext.getGlobalContext()).load((dynamic.picList == null || dynamic.picList.size() == 0 ? null : dynamic.picList.get(0))).centerCrop()
                        .placeholder(R.drawable.dynamic_default_icon).fit()
                        .into(((DynamicViewHolder) holder).dynamicImg);
            } else if (dynamic.source == 1) {
                Picasso.with(GlobalContext.getGlobalContext()).load((dynamic.picList == null || dynamic.picList.size() == 0 ? null : dynamic.picList.get(0)))
                        .placeholder(R.drawable.official_dynamic_default_icon)
                        .into(((DynamicViewHolder) holder).dynamicImg);
            }

            ((DynamicViewHolder) holder).userName.setText(dynamic.accountIdNickname);
            ((DynamicViewHolder) holder).dynamicTime.setText(dynamic.createTime);
            ((DynamicViewHolder) holder).dynamicContent.setText(dynamic.content);
            ((DynamicViewHolder) holder).dynamicLocation.setText(dynamic.address);
            //应该根据获取的动态数和点赞数设置
            ((DynamicViewHolder) holder).likeNum.setText(String.valueOf(dynamic.praiseCount));
            ((DynamicViewHolder) holder).commentNum.setText(String.valueOf(dynamic.commentCount));
            ((DynamicViewHolder) holder).itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(v, position, dynamic);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return dynamicData.size() + 1;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Dynamic dynamic);
    }


    /*活动详情*/
    public class TopicViewHolder extends RecyclerView.ViewHolder {
        public ImageView topicImg;
        public TextView topicName;
        public TextView topicCount;
        public TextView topicContent;

        public TopicViewHolder(View view) {
            super(view);
            topicImg = (ImageView) view.findViewById(R.id.topic_img);
            topicCount = (TextView) view.findViewById(R.id.topic_count_01);
            topicName = (TextView) view.findViewById(R.id.topic_name);
            topicContent = (TextView) view.findViewById(R.id.topic_content);
        }
    }

    //动态详情
    public class DynamicViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout itemLayout;
        public CircleImageView userAvatar;
        public TextView userName;
        public TextView dynamicTime;
        public TextView dynamicContent;
        public ResizableImageView dynamicImg;
        public TextView likeNum;
        public TextView commentNum;
        public ImageView share;
        public TextView dynamicLocation;
        public LinearLayout likeBtn;
        public ImageView likeImg;

        public DynamicViewHolder(View view) {
            super(view);
            itemLayout = (RelativeLayout) view.findViewById(R.id.item_layout);
            userAvatar = (CircleImageView) view.findViewById(R.id.user_avatar);
            userName = (TextView) view.findViewById(R.id.user_name);
            dynamicTime = (TextView) view.findViewById(R.id.dynamic_time);
            dynamicContent = (TextView) view.findViewById(R.id.dynamic_content_txt);
            dynamicImg = (ResizableImageView) view.findViewById(R.id.dynamic_content_img);
            likeNum = (TextView) view.findViewById(R.id.like_num);
            commentNum = (TextView) view.findViewById(R.id.comment_num);
            share = (ImageView) view.findViewById(R.id.share);
            dynamicLocation = (TextView) view.findViewById(R.id.dynamic_location);
            likeBtn = (LinearLayout) view.findViewById(R.id.like_btn);
            likeImg = (ImageView) view.findViewById(R.id.like_img);
        }
    }

}
