package com.sita.cfmoto.interact.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.adapter.ActiveListAdapter;
import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.LocalStorage;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActiveListActivity extends AppCompatActivity {
    @Bind(R.id.active_list)
    SuperRecyclerView activeListView;
    private LinearLayoutManager linearLayoutManager;
    private ActiveListAdapter activeListAdapter;
    private int size = 10;
    private int currentPage = 0;
    private Long minId = 0L;
    private int source = 0; // personal
    private Boolean isRequestNewDataMore = false;
    private Boolean isRequestRefresh = true;
    private String moreMaxId;


    public static Intent newIntent(Context context, int source) {
        Intent intent = new Intent(context, ActiveListActivity.class);
        intent.putExtra("source", source);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_list);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            source = getIntent().getIntExtra("source", 0);
        }
        LocalStorage.setActiveListRefreshTime(Constants.REFRESH_START);
        initActiveList();
    }


    @OnClick({R.id.cancel})
    public void onClickBack() {
        this.finish();
    }

    private void initActiveList() {
        linearLayoutManager = new LinearLayoutManager(this);
        activeListView.setLayoutManager(linearLayoutManager);
        activeListAdapter = new ActiveListAdapter();
        activeListView.setAdapter(activeListAdapter);
        //设置分割线
        HorizontalDividerItemDecoration dividerItemDecoration = new HorizontalDividerItemDecoration.Builder(this)
                .color(getResources().getColor(R.color.common_background))
                .size(20)
                .showLastDivider()
                .build();
        activeListView.addItemDecoration(dividerItemDecoration);
        requestData();
        activeListView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRequestRefresh = true;
                requestData();
            }
        });
        activeListView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                moreMaxId = activeListAdapter.getLastItemId();
                isRequestRefresh = false;
                requestData();
            }
        }, 1);

        activeListAdapter.setOnItemClickListener(new ActiveListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Active active) {
                //跳转到活动详情
                Intent intent = new Intent();
                intent.setClass(ActiveListActivity.this, ActiveDetailActivity.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        });
    }


    private void requestData() {
        if (isRequestRefresh) {
            //刷新
            long beginTime = LocalStorage.getActiveListRefreshTime();
            long endTime = System.currentTimeMillis();
            InteractUtils.fetchActiveList(0, size, 0, beginTime, endTime, null, minId, source, new InteractUtils.FetchActiveListCallback() {
                @Override
                public void onActiveListFetched(List<Active> activeList) {
                    if (activeList != null && !activeList.isEmpty()) {
                        minId = Long.parseLong(activeList.get(0).campaignId);
                        if (activeList.size() == size) {
                            activeListAdapter.setNewData(activeList);
                            isRequestNewDataMore = true;
                        } else {
                            activeListAdapter.setData(activeList);
                            isRequestNewDataMore = false;
                        }
                    } else {
                        activeListView.setRefreshing(false);
                    }
                }
            });
        } else {
            //更多
            if (isRequestNewDataMore) {
                String minId = activeListAdapter.getStorageFirstId();
                InteractUtils.fetchActiveList(1, size, 0, null, null, moreMaxId == null ? null : Long.parseLong(moreMaxId), (minId == null ? null : Long.parseLong(minId)), source, new InteractUtils.FetchActiveListCallback() {
                    @Override
                    public void onActiveListFetched(List<Active> activeList) {
                        if (activeList != null) {
                            if (activeList.size() == size) {
                                activeListAdapter.appendData(activeList);
                            } else if (activeList.size() < size) {
                                activeListAdapter.appendData(activeList);
                                activeListAdapter.addStorageData();
                            }
                        }
                    }
                });
            } else {
                InteractUtils.fetchActiveList(1, size, 0, null, null, moreMaxId == null ? null : Long.parseLong(moreMaxId), null, source, new InteractUtils.FetchActiveListCallback() {
                    @Override
                    public void onActiveListFetched(List<Active> activeList) {
                        if (activeList != null && !activeList.isEmpty()) {
                            activeListAdapter.appendData(activeList);
                        }
                    }
                });
            }
        }
    }
}

