package com.sita.cfmoto.interact.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.sita.cfmoto.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class InteractFragment extends Fragment {

    @Bind(R.id.interact_tab_host)
    TabHost interactTab;
    private View dynamicView, topicView, activeView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_interact, null);
        ButterKnife.bind(this, view);
        dynamicView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_dynamic, null);
        topicView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_topic, null);
        activeView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_active, null);
        setTab();
        return view;
    }
    private void setTab() {

        interactTab.setup();

        TabHost.TabSpec tab_dynamic = interactTab.newTabSpec("dynamic");
        tab_dynamic.setIndicator(dynamicView);
        tab_dynamic.setContent(R.id.fragment_dynamic);
        interactTab.addTab(tab_dynamic);

        TabHost.TabSpec tab_topic = interactTab.newTabSpec("topic");
        tab_topic.setContent(R.id.fragment_topic);
        tab_topic.setIndicator(topicView);
        interactTab.addTab(tab_topic);

        TabHost.TabSpec tab_active = interactTab.newTabSpec("active");
        tab_active.setContent(R.id.fragment_active);
        tab_active.setIndicator(activeView);
        interactTab.addTab(tab_active);

        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_dynamic, new DynamicFragment());
        transaction.replace(R.id.fragment_topic, new TopicFragment());
        transaction.replace(R.id.fragment_active, new ActiveFragment());
        transaction.commit();
    }
}
