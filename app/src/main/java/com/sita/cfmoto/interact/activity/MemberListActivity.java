package com.sita.cfmoto.interact.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Member;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.utils.CircleTransform;
import com.squareup.picasso.Picasso;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemberListActivity extends AppCompatActivity {
    @Bind(R.id.active_members_list)
    RecyclerView mMemberList;
    @Bind(R.id.toolbar_title)
    TextView toolBarTitle;
    private boolean isFromDynamic;

    public static Intent newIntent(Context context, ArrayList<Member> users, boolean isFromDynamic) {
        Intent intent = new Intent(context, MemberListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromDynamic", false);
        bundle.putSerializable("users", users);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_list);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            isFromDynamic = getIntent().getExtras().getBoolean("isFromDynamic");
            List<Member> users = (List<Member>) getIntent().getExtras().getSerializable("users");
            initMemberList(users);
        }
        if (isFromDynamic) {
            toolBarTitle.setText("赞过的人");
        } else {
            toolBarTitle.setText("参与者");
        }
    }

    @OnClick(R.id.back)
    void OnClickBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    private void initMemberList(List<Member> users) {
        mMemberList.setLayoutManager(new LinearLayoutManager(this));
        HorizontalDividerItemDecoration dividerItemDecoration = new HorizontalDividerItemDecoration.Builder(this)
                .color(getResources().getColor(R.color.common_divider_background))
                .size(1)
                .build();
        mMemberList.addItemDecoration(dividerItemDecoration);
        mMemberList.setAdapter(new MemberListAdapter(users));
    }


    class MemberListAdapter extends RecyclerView.Adapter<MemberListAdapter.ViewHolder> {
        private List<Member> mMemberList = new ArrayList<>();

        public MemberListAdapter(List<Member> members) {
            super();
            this.mMemberList = members;

        }

        @Override
        public int getItemCount() {
            return mMemberList.size();
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.memberNickname.setText(mMemberList.get(position).nickName);
            if (TextUtils.isEmpty(mMemberList.get(position).avatar)) {
                return;
            }
            Picasso.with(GlobalContext.getGlobalContext()).load(mMemberList.get(position).avatar)
                    .placeholder(R.drawable.default_head_icon)
                    .transform(new CircleTransform())
                    .centerCrop()
                    .fit()
                    .into(holder.memberAvatar);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_member_list, parent, false);
            return new ViewHolder(view);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            @Bind(R.id.active_member_avatar)
            ImageView memberAvatar;

            @Bind(R.id.active_member_nickname)
            TextView memberNickname;

            public ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }
        }
    }
}
