package com.sita.cfmoto.event;

/**
 * Created by mark on 2015/5/18.
 */
public class UpdateProfileEvent {
    private String nickName;
    private String avatar;


    public UpdateProfileEvent(String avatar, String nickName) {
        this.avatar = avatar;
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
