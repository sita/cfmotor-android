package com.sita.cfmoto.event;

import com.amap.api.maps.model.LatLng;

/**
 * Class com.sita.bike.event is for
 *
 * @author xiaodong on 15/4/17.
 */
public class FetchVehicleLocEvent {

    private String identifier;
    private LatLng latLng;
    public FetchVehicleLocEvent() {
    }

    public String getIdentifier() {
        return identifier;
    }

    public FetchVehicleLocEvent withIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public FetchVehicleLocEvent withLatLng(LatLng latLng) {
        this.latLng = latLng;
        return this;
    }
}
