package com.sita.cfmoto.event;

/**
 * Created by lijuan zhang on 2016/9/9.
 */
public class HasBindVehicleEvent {
    public boolean hasBindVehicle;

    public HasBindVehicleEvent(boolean hasBindVehicle) {
        this.hasBindVehicle = hasBindVehicle;
    }
}
