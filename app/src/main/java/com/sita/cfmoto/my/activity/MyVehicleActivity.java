package com.sita.cfmoto.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.my.adapter.BindVehicleAdapter;
import com.sita.cfmoto.rest.model.BindedVehicle;
import com.sita.cfmoto.utils.VehicleUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyVehicleActivity extends AppCompatActivity {
    @Bind(R.id.vehicle_list)
    RecyclerView vehicleList;
    private LinearLayoutManager mLinearLayoutManager;
    private BindVehicleAdapter mBindVehicleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_vehicle);
        ButterKnife.bind(this);
        mLinearLayoutManager = new LinearLayoutManager(this);
        vehicleList.setLayoutManager(mLinearLayoutManager);
        mBindVehicleAdapter = new BindVehicleAdapter(this);
        vehicleList.setAdapter(mBindVehicleAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        VehicleUtils.fetchBindedVehicles(new VehicleUtils.FetchBindedVehiclesCallback() {
            @Override
            public void onBindedVehiclesFetched(List<BindedVehicle> vehicles) {
                if (vehicles != null && !vehicles.isEmpty())
                    mBindVehicleAdapter.setNewData(vehicles);
            }
        });
    }

    @OnClick(R.id.back_img)
    void onClickBack() {
        finish();
    }

    @OnClick(R.id.add_btn)
    void onClickAddVehicle() {
        Intent intent = new Intent();
        intent.setClass(this, BindVehicleActivity.class);
        intent.putExtra("isToMain",false);
        startActivity(intent);
    }
}
