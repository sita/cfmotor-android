package com.sita.cfmoto.my;

/**
 * Created by xiaodong on 16/3/3.
 */
/*{"accountId": "375691015094273742",
"nickName":"思塔-陈晨",
 "password": "1",
 "uniqueId":"abcd",
  "signature":""}*/
public class UpdateUserParams {
    public String accountId;
    public String nickName;
    public String password;
    public String uniqueId;
}
