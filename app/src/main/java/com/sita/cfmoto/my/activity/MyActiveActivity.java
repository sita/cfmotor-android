package com.sita.cfmoto.my.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;

import com.sita.cfmoto.R;
import com.sita.cfmoto.my.fragment.InitiateActiveFragment;
import com.sita.cfmoto.my.fragment.JoinActiveFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyActiveActivity extends AppCompatActivity {
    @Bind(R.id.active_tab_host)
    TabHost activeHost;
    private View initiateView, joinView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_active);
        ButterKnife.bind(this);
        initiateView = LayoutInflater.from(this).inflate(R.layout.tab_initiate_active, null);
        joinView = LayoutInflater.from(this).inflate(R.layout.tab_join_active, null);
        setTab();
    }

    private void setTab() {
        activeHost.setup();
        TabHost.TabSpec tab_initiate = activeHost.newTabSpec("initiate");
        tab_initiate.setIndicator(initiateView);
        tab_initiate.setContent(R.id.fragment_initiate_active);
        activeHost.addTab(tab_initiate);
        TabHost.TabSpec tab_join = activeHost.newTabSpec("join");
        tab_join.setIndicator(joinView);
        tab_join.setContent(R.id.fragment_join_active);
        activeHost.addTab(tab_join);


        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_initiate_active, new InitiateActiveFragment());
        transaction.replace(R.id.fragment_join_active, new JoinActiveFragment());
        transaction.commit();
    }

    @OnClick(R.id.back_img)
    void onClickBack() {
        finish();
    }
}
