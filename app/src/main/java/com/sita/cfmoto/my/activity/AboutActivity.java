package com.sita.cfmoto.my.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.VersionUtils;


/**
 * Created by hongyun on 2016/3/14.
 */
public class AboutActivity extends AppCompatActivity {

    private TextView tvAppName;
    private TextView tvAppServer;
    private ImageView imgBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_about);
        super.onCreate(savedInstanceState);
        tvAppName = (TextView) findViewById(R.id.tv_app);
        tvAppName.setText("ChunFeng App " + VersionUtils.getSimpleAppVersionName(this));
//        tvAppServer = (TextView) findViewById(R.id.tv_app_server);
//        String server = Constants.isProductServer() ? (getResources().getText(R.string.server_product) + ":" + getResources().getText(R.string.server_product_address)) :
//                (getResources().getText(R.string.server_inner_test) + Constants.BASE_URI);
//        tvAppServer.setText(server);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
