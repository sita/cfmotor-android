package com.sita.cfmoto.my.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.my.adapter.NoticeAdapter;
import com.sita.cfmoto.rest.model.Inform;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.LocalStorage;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoticeActivity extends AppCompatActivity {
    @Bind(R.id.notice_list)
    SuperRecyclerView noticeList;
    private LinearLayoutManager linearLayoutManager;
    private NoticeAdapter noticeAdapter;
    private int size = 10;
    private int currentPage = 0;

    public static Intent newIntent(Context context, ArrayList<Inform> list) {
        Intent intent = new Intent(context, NoticeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", list);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        ButterKnife.bind(this);

        if (getIntent() == null) {
            initNoticeList();
        } else {
            ArrayList<Inform> list = (ArrayList<Inform>) getIntent().getSerializableExtra("data");
            if (list != null) {
                initNoticeList(list);
            } else {
                initNoticeList();
            }
        }
    }

    @OnClick(R.id.back_img)
    void onClickBack() {
        finish();
    }


    private void initNoticeList(List<Inform> list) {
        linearLayoutManager = new LinearLayoutManager(this);
        noticeList.setLayoutManager(linearLayoutManager);
        noticeAdapter = new NoticeAdapter();
        noticeList.setAdapter(noticeAdapter);
        noticeAdapter.setData(list);
        noticeAdapter.notifyDataSetChanged();
    }

    private void initNoticeList() {
        linearLayoutManager = new LinearLayoutManager(this);
        noticeList.setLayoutManager(linearLayoutManager);
        noticeAdapter = new NoticeAdapter();
        noticeList.setAdapter(noticeAdapter);
        requestData();
        noticeList.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                currentPage++;
                requestData();
            }
        }, 1);
        noticeList.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 0;
                requestData();
            }
        });
        noticeAdapter.setOnJoinRequestHandleListener(new NoticeAdapter.OnJoinRequestHandleListener() {
            @Override
            public void onJoinRequestHandle(View view, int position) {
                currentPage = 0;
                requestData();
            }
        });
    }


    private void requestData() {
        if (currentPage == 0) {
            //刷新
            InteractUtils.fetchInform(size, 0, new InteractUtils.FetchInformListCallback() {
                @Override
                public void onInformListFetched(List<Inform> informList) {
                    if (informList != null && !informList.isEmpty()) {
                        // save timestamp
                        LocalStorage.setLastNotificationTimeStamp(informList.get(0).doTime);
                        noticeAdapter.setData(informList);
                    } else {
                        noticeList.setRefreshing(false);
                    }
                }
            });
        } else {
            //更多
            InteractUtils.fetchInform(size, currentPage, new InteractUtils.FetchInformListCallback() {
                @Override
                public void onInformListFetched(List<Inform> informList) {
                    if (informList != null && !informList.isEmpty()) {
                        noticeAdapter.appendData(informList);
                    } else {
                        currentPage--;
                    }
                }
            });
        }
    }
}
