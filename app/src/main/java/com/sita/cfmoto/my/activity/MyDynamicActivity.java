package com.sita.cfmoto.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.activity.DynamicDetailActivity;
import com.sita.cfmoto.interact.adapter.DynamicListAdapter;
import com.sita.cfmoto.rest.model.Dynamic;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.LocalStorage;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyDynamicActivity extends AppCompatActivity {
    @Bind(R.id.dynamic_list)
    SuperRecyclerView dynamicListView;
    private LinearLayoutManager linearLayoutManager;
    private DynamicListAdapter dynamicListAdapter;
    private int size = 10;
    private int currentPage = 0;
    private Long minId = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dynamic);
        ButterKnife.bind(this);
        LocalStorage.setDynamicListRefreshTime(Constants.REFRESH_START);
        initDynamicList();
    }


    @OnClick({R.id.cancel})
    public void onClickBack() {
        this.finish();
    }

    private void initDynamicList() {
        linearLayoutManager = new LinearLayoutManager(this);
        dynamicListView.setLayoutManager(linearLayoutManager);
        dynamicListAdapter = new DynamicListAdapter();
        dynamicListView.setAdapter(dynamicListAdapter);
        //设置分割线
        HorizontalDividerItemDecoration dividerItemDecoration = new HorizontalDividerItemDecoration.Builder(this)
                .color(getResources().getColor(R.color.common_background))
                .size(20)
                .showLastDivider()
                .build();
        dynamicListView.addItemDecoration(dividerItemDecoration);
        requestData();
        dynamicListView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 0;
                requestData();
            }
        });
        dynamicListView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                currentPage++;
                requestData();
            }
        }, 1);
        dynamicListAdapter.setOnItemClickListener(new DynamicListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Dynamic dynamic) {
                //跳转到活动详情
                Intent intent = new Intent();
                intent.setClass(MyDynamicActivity.this, DynamicDetailActivity.class);
                intent.putExtra("dynamic", dynamic);
                startActivity(intent);
            }
        });
    }

    private void requestData() {
        if (currentPage == 0) {
            //刷新
            long beginTime = LocalStorage.getDynamicListRefreshTime();
            long endTime = System.currentTimeMillis();
            InteractUtils.fetchDynamicList(0, size, currentPage, null, beginTime, endTime, null, minId, null, 1, new InteractUtils.FetchDynamicListCallback() {
                @Override
                public void onDynamicListFetched(List<Dynamic> dynamicList) {
                    if (dynamicList != null && !dynamicList.isEmpty()) {
                        minId = Long.parseLong(dynamicList.get(0).msgId);
                        dynamicListAdapter.setData(dynamicList);

                    } else {
                        dynamicListView.setRefreshing(false);
                    }
                }
            });
        } else {
            //更多
            final long endTime = LocalStorage.getDynamicListRefreshTime();
            final long maxId = minId;
            InteractUtils.fetchDynamicList(2, size, currentPage, null, null, endTime, maxId, null, null, 1, new InteractUtils.FetchDynamicListCallback() {
                @Override
                public void onDynamicListFetched(List<Dynamic> dynamicList) {
                    if (dynamicList != null && !dynamicList.isEmpty()) {
                        dynamicListAdapter.appendData(dynamicList);
                    } else {
                        currentPage--;
                    }
                }
            });
        }
    }
}
