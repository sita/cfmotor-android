package com.sita.cfmoto.my.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.utils.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lijuan zhang on 2016/8/11.
 */
public class MyActiveAdapter extends RecyclerView.Adapter<MyActiveAdapter.ViewHolder> {
    private OnItemClickListener itemClickListener;
    private List<Active> data = new ArrayList<>();

    public MyActiveAdapter() {
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setData(List<Active> data) {
        List<Active> list = new ArrayList<>();
        list.addAll(data);
        list.addAll(this.data);
        this.data = list;
        notifyItemRangeChanged(0, data.size());
    }

    public void appendData(List<Active> data) {
        int currCount = getItemCount();
        this.data.addAll(data);
        notifyItemRangeInserted(currCount, data.size());
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_initiate_active, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //数据绑定
        Active active = data.get(position);
        Picasso.with(GlobalContext.getGlobalContext())
                .load((active.cover.isEmpty()?null:active.cover))
                .placeholder(R.drawable.dynamic_default_rect_bg)
                .centerCrop()
                .fit()
                .into(holder.activeImg);
        holder.activeName.setText(active.title);
        holder.activeContent.setText(active.content);
        holder.activeTime.setText(GlobalContext.getGlobalContext().getString(R.string.my_active_date,
                DateUtils.shortDate(active.campaignStartTime)));
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null)
                    itemClickListener.onItemClick(v, position,active);
            }
        });
    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return data.size();
    }

    public Active getItem(int position) {
        if (position < getItemCount()) {
            return data.get(position);
        } else {
            return null;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position,Active active);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout itemLayout;
        public ImageView activeImg;
        public TextView activeName;
        public TextView activeContent;
        public TextView activeTime;


        public ViewHolder(View view) {
            super(view);
            itemLayout = (RelativeLayout) view.findViewById(R.id.item_layout);
            activeImg = (ImageView) view.findViewById(R.id.active_icon);
            activeName = (TextView) view.findViewById(R.id.active_name);
            activeContent = (TextView) view.findViewById(R.id.active_content);
            activeTime = (TextView) view.findViewById(R.id.active_time);
        }
    }

}
