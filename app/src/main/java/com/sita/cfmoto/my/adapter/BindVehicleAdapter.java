package com.sita.cfmoto.my.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.BindedVehicle;
import com.sita.cfmoto.utils.PersistUtils;
import com.sita.cfmoto.utils.TextUtils;
import com.sita.cfmoto.utils.ToastTool;
import com.sita.cfmoto.utils.VehicleUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lijuan zhang on 2016/8/10.
 */
public class BindVehicleAdapter extends RecyclerView.Adapter<BindVehicleAdapter.ViewHolder> {

    private List<BindedVehicle> data = new ArrayList<>();
    private Context context;

    public BindVehicleAdapter(Context context) {
        this.context = context;
    }

    public void setNewData(List<BindedVehicle> vehicles) {
        data.clear();
        data.addAll(vehicles);
        notifyItemRangeChanged(0, data.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_vehicle, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (data.size() - 1 == position) {
            holder.divider.setVisibility(View.GONE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }
        //数据绑定
        //当条item对应的车辆
        BindedVehicle vehicle = data.get(position);
        //获取当前绑定车辆
        BindedVehicle currentSelectedVehicle = VehicleUtils.getCurrentSelectedVehicle();
        if (currentSelectedVehicle != null && vehicle.vin.equals(currentSelectedVehicle.vin) && vehicle.vincpy == currentSelectedVehicle.vincpy) {
            holder.bindVehicle.setVisibility(View.GONE);
            holder.currentVehicle.setVisibility(View.VISIBLE);
        } else {
            holder.bindVehicle.setVisibility(View.VISIBLE);
            holder.currentVehicle.setVisibility(View.GONE);
        }
        holder.vehicleType.setText(vehicle.machineKind);
        holder.vehicleSn.setText(vehicle.vin);
        holder.bindVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doBind(vehicle);
            }
        });
        holder.unBindVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final NormalDialog dialog = new NormalDialog(context);
                dialog.style(NormalDialog.STYLE_TWO)
                        .titleLineColor(context.getResources().getColor(R.color.blue))
                        .titleTextSize(20)
                        .title(context.getString(R.string.unbind))
                        .contentTextColor(context.getResources().getColor(R.color.black))
                        .contentGravity(Gravity.CENTER)
                        .content(context.getString(R.string.unbind_confirm))
                        .show();
                dialog.setOnBtnClickL(
                        new OnBtnClickL() {
                            @Override
                            public void onBtnClick() {
                                // left
                                dialog.dismiss();
                            }
                        }, new OnBtnClickL() {
                            @Override
                            public void onBtnClick() {
                                // right
                                dialog.dismiss();
                                doUnbind(vehicle);
                            }
                        });
            }
        });
    }

    @Override
    public int getItemCount() {
        //返回数据长度
        return data.size();
    }

    public void doUnbind(BindedVehicle vehicle) {
        VehicleUtils.unbindVehicle(vehicle, new VehicleUtils.UnbindVehicleCallback() {
            @Override
            public void onUnbindVehicleResult(boolean isOk) {
                if (isOk) {
                    PersistUtils.deleteBindedVehicle(vehicle);
                    data.remove(vehicle);
                    BindedVehicle currentSelectedVehicle = VehicleUtils.getCurrentSelectedVehicle();
                    if (vehicle.vin.equals(currentSelectedVehicle.vin) && vehicle.vincpy == currentSelectedVehicle.vincpy && data.size() != 0) {
                        doBind(data.get(0));
                    } else {
                        notifyDataSetChanged();
                    }

                }
            }
        });
    }

    public void doBind(BindedVehicle vehicle) {
        VehicleUtils.bindVehicle(vehicle, new VehicleUtils.BindVehicleCallback() {
            @Override
            public void onBindVehicleSuccess(String vehicleSn) {
                if (vehicleSn != null) {
//                    LocalStorage.setCurrSelectedVehicle(vehicleSn);
                    VehicleUtils.setCurrentSelectedVehicle(vehicleSn);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onBindVehicleFailed(Throwable cause) {
                ToastTool.showDebugMessage(TextUtils.getThrowableMessage(cause));
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView currentVehicle;
        public Button bindVehicle;
        public Button unBindVehicle;
        public TextView vehicleType;
        public TextView vehicleSn;
        public View divider;

        public ViewHolder(View view) {
            super(view);
            currentVehicle = (TextView) view.findViewById(R.id.curr_selected_vehicle);
            bindVehicle = (Button) view.findViewById(R.id.btn_bind);
            unBindVehicle = (Button) view.findViewById(R.id.btn_delete);
            vehicleType = (TextView) view.findViewById(R.id.vehicle_type);
            vehicleSn = (TextView) view.findViewById(R.id.vehicle_sn);
            divider = view.findViewById(R.id.divider);
        }
    }
}
