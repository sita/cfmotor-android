package com.sita.cfmoto.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.activity.TopicDetailActivity;
import com.sita.cfmoto.interact.adapter.TopicListAdapter;
import com.sita.cfmoto.rest.model.Topic;
import com.sita.cfmoto.utils.InteractUtils;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyTopicActivity extends AppCompatActivity {

    @Bind(R.id.topic_list)
    SuperRecyclerView topicListView;
    private TopicListAdapter topicListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private int size = 10;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_topic);
        ButterKnife.bind(this);
        initTopicList();
    }

    @OnClick({R.id.cancel})
    public void onClickBack() {
        this.finish();
    }

    private void initTopicList() {
        linearLayoutManager = new LinearLayoutManager(this);
        topicListView.setLayoutManager(linearLayoutManager);
        topicListAdapter = new TopicListAdapter();
        topicListView.setAdapter(topicListAdapter);
        //设置分割线
        HorizontalDividerItemDecoration dividerItemDecoration = new HorizontalDividerItemDecoration.Builder(this)
                .color(getResources().getColor(R.color.common_background))
                .size(20)
                .showLastDivider()
                .build();
        topicListView.addItemDecoration(dividerItemDecoration);
        requestData();
        topicListView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                currentPage++;
                requestData();
            }
        }, 1);
        topicListView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 0;
                requestData();
            }
        });
        topicListAdapter.setOnItemClickListener(new TopicListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Topic topic) {
                //跳转到活动详情
                Intent intent = new Intent();
                intent.setClass(MyTopicActivity.this, TopicDetailActivity.class);
                intent.putExtra("topic", topic);
                startActivity(intent);
            }
        });
    }


    private void requestData() {
        if (currentPage == 0) {
            //刷新
            InteractUtils.fetchMyTopicList(size, 0, new InteractUtils.FetchMyTopicListCallback() {
                @Override
                public void onMyTopicListFetched(List<Topic> topicList) {
                    if (topicList != null && !topicList.isEmpty()) {
                        topicListAdapter.setMyTopicData(topicList);
                    } else {
                        topicListView.setRefreshing(false);
                    }
                }
            });
        } else {
            //更多
            InteractUtils.fetchMyTopicList(size, currentPage, new InteractUtils.FetchMyTopicListCallback() {
                @Override
                public void onMyTopicListFetched(List<Topic> topicList) {
                    if (topicList != null && !topicList.isEmpty()) {
                        topicListAdapter.appendData(topicList);
                    } else {
                        currentPage--;
                    }
                }
            });
        }
    }
}
