package com.sita.cfmoto.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.event.HasBindVehicleEvent;
import com.sita.cfmoto.event.UpdateProfileEvent;
import com.sita.cfmoto.my.activity.AboutActivity;
import com.sita.cfmoto.my.activity.MyActiveActivity;
import com.sita.cfmoto.my.activity.MyDynamicActivity;
import com.sita.cfmoto.my.activity.MyTopicActivity;
import com.sita.cfmoto.my.activity.MyVehicleActivity;
import com.sita.cfmoto.my.activity.UserInfoActivity;
import com.sita.cfmoto.persistence.User;
import com.sita.cfmoto.rest.model.BindedVehicle;
import com.sita.cfmoto.rest.model.InteractCount;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.PersistUtils;
import com.sita.cfmoto.utils.VehicleUtils;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileFragment extends Fragment {

    @Bind(R.id.avatar)
    CircleImageView avatar;
    @Bind(R.id.nickname)
    TextView nickName;
    @Bind(R.id.dynamic_count)
    TextView dynamicCount;
    @Bind(R.id.topic_count)
    TextView topicCount;
    @Bind(R.id.active_count)
    TextView activeCount;
    @Bind(R.id.vehicle_type)
    TextView vehicleType;
    private User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_profile, null);
        ButterKnife.bind(this, view);
        user = PersistUtils.getUsers().get(0);
        Picasso.with(GlobalContext.getGlobalContext()).load(user.getAvatar()).centerCrop().fit()
                .into(avatar);
        nickName.setText(user.getNickName());
        getInteractCount();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    public void onEventMainThread(UpdateProfileEvent event) {
        if (event != null) {
            Picasso.with(GlobalContext.getGlobalContext()).load(event.getAvatar()).centerCrop().fit()
                    .into(avatar);
            nickName.setText(event.getNickName());
        }
    }

    @OnClick(R.id.vehicle_layout)
    void onClickVehicle() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), MyVehicleActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.about_layout)
    void onClickAbout() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), AboutActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.active_layout)
    void onClickActive() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), MyActiveActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dynamic_layout)
    void onClickDynamic() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), MyDynamicActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.topic_layout)
    void onClickTopic() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), MyTopicActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.user_info_layout)
    void onClickUserInfo() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), UserInfoActivity.class);
        startActivity(intent);
    }


    private void getInteractCount() {
        InteractUtils.fetchInteractCount(user.getAccountId(), new InteractUtils.FetchInteractCountCallback() {
            @Override
            public void onInteractCountFetched(InteractCount interactCount) {
                if (interactCount != null) {
                    dynamicCount.setText(String.valueOf(interactCount.postmsgCount));
                    topicCount.setText(String.valueOf(interactCount.topicCount));
                    activeCount.setText(String.valueOf(interactCount.campaignCount));
                }
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getInteractCount();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onEventMainThread(HasBindVehicleEvent event) {
        if (event.hasBindVehicle) {
            BindedVehicle bindedVehicle = VehicleUtils.getCurrentSelectedVehicle();
            vehicleType.setText(bindedVehicle.machineType);
        } else {
            vehicleType.setText("--");
        }
    }
}
