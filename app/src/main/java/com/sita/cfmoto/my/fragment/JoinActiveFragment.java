package com.sita.cfmoto.my.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.interact.activity.ActiveDetailActivity;
import com.sita.cfmoto.my.adapter.MyActiveAdapter;
import com.sita.cfmoto.rest.model.Active;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.InteractUtils;
import com.sita.cfmoto.utils.LocalStorage;
import com.sita.cfmoto.utils.PersistUtils;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class JoinActiveFragment extends Fragment {
    @Bind(R.id.join_active_list)
    SuperRecyclerView joinActiveList;
    private LinearLayoutManager linearLayoutManager;
    private MyActiveAdapter myActiveAdapter;
    private int size = 10;
    private int currentPage = 0;
    private long minId;
    private long userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_join_active, container, false);
        ButterKnife.bind(this, view);
        LocalStorage.setJoinActiveListRefreshTime(Constants.REFRESH_START);
        userId = PersistUtils.getUsers().get(0).getAccountId();
        initActiveList();
        return view;
    }

    private void initActiveList() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        joinActiveList.setLayoutManager(linearLayoutManager);
        myActiveAdapter = new MyActiveAdapter();
        joinActiveList.setAdapter(myActiveAdapter);
        //设置分割线
        HorizontalDividerItemDecoration dividerItemDecoration = new HorizontalDividerItemDecoration.Builder(getActivity())
                .color(getResources().getColor(R.color.common_divider_background))
                .size(1)
                .build();
        joinActiveList.addItemDecoration(dividerItemDecoration);
        requestData();
        joinActiveList.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 0;
                requestData();
            }
        });
        joinActiveList.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
                currentPage++;
                requestData();
            }
        }, 1);

        myActiveAdapter.setOnItemClickListener(new MyActiveAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Active active) {
                //跳转到活动详情
                Intent intent = new Intent();
                intent.setClass(getActivity(), ActiveDetailActivity.class);
                intent.putExtra("active", active);
                startActivity(intent);
            }
        });
    }


    private void requestData() {
        if (currentPage == 0) {
            //刷新
            long beginTime = LocalStorage.getJoinActiveListRefreshTime();
            long endTime = System.currentTimeMillis();
            InteractUtils.fetchMyActiveList(userId, size, currentPage, String.valueOf(beginTime), String.valueOf(endTime), null, String.valueOf(minId),1, new InteractUtils.FetchMyActiveListCallback() {
                @Override
                public void onMyActiveListFetched(List<Active> activeList) {
                    if (activeList != null && !activeList.isEmpty()) {
                        minId = Long.parseLong(activeList.get(0).campaignId);
                        myActiveAdapter.setData(activeList);
                    } else {
                        joinActiveList.setRefreshing(false);
                    }
                }
            });
        } else {
            //更多
            final long endTime = LocalStorage.getJoinActiveListRefreshTime();
            final long maxId = minId;
            InteractUtils.fetchMyActiveList(userId, size, currentPage, null, String.valueOf(endTime), String.valueOf(maxId), null, 1, new InteractUtils.FetchMyActiveListCallback() {
                @Override
                public void onMyActiveListFetched(List<Active> activeList) {
                    if (activeList != null && !activeList.isEmpty()) {
                        myActiveAdapter.appendData(activeList);
                    } else {
                        currentPage--;
                    }
                }
            });
        }
    }
}
