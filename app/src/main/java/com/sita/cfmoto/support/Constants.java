package com.sita.cfmoto.support;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.amap.api.maps.model.LatLng;
import com.sita.cfmoto.utils.L;

/**
 * Class Constants is for all constant values
 *
 * @author xiaodong on 15/2/28.
 */
public class Constants {
    private static void getBaseUri() {
        if (BASE_URI == null) {
            try {
                ApplicationInfo appInfo = GlobalContext.getGlobalContext().getPackageManager()
                        .getApplicationInfo(GlobalContext.getGlobalContext().getPackageName(),
                                PackageManager.GET_META_DATA);
//                BASE_URI = appInfo.metaData.getString("URI");
                BASE_URI = "http://123.206.201.243:8080";

                L.i("readMetaDataFromApplication()", "URI=" + BASE_URI);
            } catch (PackageManager.NameNotFoundException e) {
                L.e("readMetaDataFromApplication()", e);
            }
        }
    }

    static {
        getBaseUri();
    }

    public static int AD_TIME = 2000;

    public static boolean isProductServer() {
        return BASE_URI.contains("www.sitayun.com");
    }

    public static final String TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";
    public static final int AVATAR_IMAGE_SIZE = 100;
    public static final String DB_NAME = "tracking-bike.db";
    public static final long RE_LOGIN_PERIOD = 7 * 24 * 60 * 60 * 1000;
    public static final long TIMEOUT = 5000;
    public static final LatLng DEFAULT_MAP_CENTER_LATLNG = new LatLng(39.08662, 117.203887);// 天津市经纬度
    public static final long LOCATION_REQUEST_TIMEOUT = 10000;
    public static final float LOCATION_ACCURACY = 10.0f;
    public static final float DEFAULT_ZOOM_LEVEL = 15.5f;

    public static final float ALERT_DISTANCE = 50.0f;

    public static final int LOCATION_SEND_INTERVAL_MIN_VALUE = 10000;

    public static final String SERVER_HOST = "182.254.212.137";
    public static final int SERVER_REST_PORT = 8080;
    public static final String SERVER_REST_SCHEMA = "http";
    public static final String SERVER_ADDRESS = SERVER_REST_SCHEMA + "://" + SERVER_HOST + ":" + SERVER_REST_PORT;

    public static final int SERVER_XMPP_PORT = 5222;
    public static final String SERVER_XMPP_SERVICE = "sitasrvtenshg001";
    public static final String SERVER_XMPP_ACCOUNT = "sixmppmailbox" + "@" + SERVER_XMPP_SERVICE;
    public static final String APPLICATION_OCTET_STREAM_VALUE = "application/octet-stream";
    public static final String APPLICATION_JSON = "application/json";
    public static final int TYPE_VEHICLE = 0;
    public static final int TYPE_USER = 1;
    public static final int USER_LOCSPEED = 100;
    public static final int USER_TXTMESSAGE = 101;
    public static final int USER_AUDIOMESSAGE = 102;
    public static final int VEHICLE_RTDATA = 200;
    public static final int VEHICLE_RTLOCSPEED = 201;
    public static final int VEHICLE_GPSLOC = 202;
    public static final int VEHICLE_WORKSUMMARY = 203;
    public static final int VEHICLE_TIMENOTIFICATION = 204;
    public static final int VEHICLE_TIMELOCNOTIFICATION = 205;
    public static final int VEHICLE_OVERSPEEDNOTIFICATION = 206;
    public static final int VEHICLE_LOWVOLTAGENOTIFICATION = 207;
    public static final int VEHICLE_RTLOCSPEED_SHUTTLE_BUS = 208;
    public static final int VEHICLE_RTLOCSPEED_CAR_TRIP = 209;
    public static final int VEHICLE_RTDATA_SHUTTLE_BUS = 210;
    public static final int VEHICLE_RTDATA_CAR_TRIP = 211;

    public static final int PNS_NOTIFICATION_ADD_CONTACT = 0;
    public static final int PNS_NOTIFICATION_ACCEPT_CONTACT = 1;
    public static final int PNS_NOTIFICATION_PICKUP_YOU_INVITE = 2;
    public static final int PNS_NOTIFICATION_PICKUP_YOU_FEEDBACK = 3;
    public static final int PNS_NOTIFICATION_PICKUP_ME_INVITE = 4;
    public static final int PNS_NOTIFICATION_PICKUP_ME_FEEDBACK = 5;

    public static final int PNS_CONTACT_FAMILY = 0;
    public static final int PNS_CONTACT_FRIEND = 1;
    public static final String ACTION_PUSH_MESSAGE = "push_message";
    public static final String ACTION_DEVICE_MAP = "device_map";
    public static final String ACTION_STATISTICS_REPORT = "statistics_report";
    public static final int PENDING_CONTACT_REQUEST_OK = 0;
    public static final int PENDING_CONTACT_REQUEST_BY_ME = 1;
    public static final int PENDING_CONTACT_REQUEST_BY_OTHER = 2;
    public static final int LOCATION_TYPE_WGS84 = 0;
    public static final int LOCATION_TYPE_GCJ02 = 1;
    public static final int LOCATION_TYPE_OTHER = 3;
    public static final String LOC_SENDER_WAKE_LOCK = "location_sender_wake_lock";
    private static final String AUTH_PROXY = "/authproxy";
    private static final String IF9A_USR = "/if9a/user";
    public static final String IF9A_USR_GET_SALT = AUTH_PROXY + IF9A_USR + "/get/salt";
    public static final String IF9A_USR_LOGIN = AUTH_PROXY + IF9A_USR + "/in";
    public static final String IF9A_USR_REGISTER = AUTH_PROXY + IF9A_USR + "/up";
    private static final String IF9A_BUS = "/if9a/bus";
    public static final String IF9A_BUS_SEARCH_LINES = AUTH_PROXY + IF9A_BUS + "/search/lines";
    public static final String IF9A_BUS_BIND_LINE_STATION = AUTH_PROXY + IF9A_BUS + "/subscribe/station";
    public static final String IF9A_BUS_UNBIND_LINE_STATION = AUTH_PROXY + IF9A_BUS + "/cancel/subscribe/station";
    private static final String IF9A_CAR = "/if9a/car";
    public static final String IF9A_CAR_SEARCH_DEVICE = AUTH_PROXY + IF9A_CAR + "/search/device";
    public static final String IF9A_CAR_ACTIVATE_DEVICE = AUTH_PROXY + IF9A_CAR + "/activate/device";
    public static final String IF9A_CAR_SEARCH_USER = AUTH_PROXY + IF9A_CAR + "/search/user";
    public static final String IF9A_CAR_ADD_CONTACT = AUTH_PROXY + IF9A_CAR + "/add/contact";
    public static final String IF9A_CAR_ACCEPT_CONTACT = AUTH_PROXY + IF9A_CAR + "/accept/contact";
    public static final String IF9A_CAR_PICKUP_YOU_INVITE = AUTH_PROXY + IF9A_CAR + "/pickup/you/invite";
    public static final String IF9A_CAR_PICKUP_YOU_FEEDBACK = AUTH_PROXY + IF9A_CAR + "/pickup/you/feedback";
    public static final String IF9A_CAR_TRIP_LOC_SPEED_TRACK = AUTH_PROXY + IF9A_CAR + "/trip/track/locspeed";
    public static final String IF9A_CAR_TERMINATE_TRIP = AUTH_PROXY + IF9A_CAR + "/terminate/trip";
    public static final String IF9A_USR_UPLOAD_HEAD_IMAGE = AUTH_PROXY + IF9A_USR + "/head/up";
    public static final String IF9A_USR_DOWNLOAD_HEAD_IMAGE = AUTH_PROXY + IF9A_USR + "/head/down";
    public static final String IF9A_TU = "/if9a/tu";
    public static final String IF9A_TU_SIGNUP = IF9A_TU + "/users";
    public static final String IF9A_TU_ = IF9A_TU + "/relationships";
    public static final String ROADTRUST_RESOURCE = "/roadtrust/v1/resources/upload";

    private static Long startTime;

    public static final String RT_RESOURCE_BUCKET = "roadtrust.resources";

    public static final float LOCATION_ZOOM = 15.5f;

    public static final String ACCOUNT_BUCKET = "accounts";
    public static final String RT_BLACKLIST_BUCKET = "roadtrust.blacklist";

    public static final String BUNDLE_ACCOUNT = "ACCOUNT";
    public static String BASE_URI;

    public static final String BUNDLE_APP_STATE = "APP_STATE";

    public static final long ADVERTISEMENT_SHOW_TIME = 4000;
    public static final long ADVERTISEMENT_INTERVAL = 1000;

    public static final String ROUTE_FILE_SYNC_STATUS_DOWNLOAD_NOT = "not download";
    public static final String ROUTE_FILE_SYNC_STATUS_DOWNLOADED = "downloaded";
    public static final String ROUTE_FILE_SYNC_STATUS_UPLOAD_NOT = "not upload";
    public static final String ROUTE_FILE_SYNC_STATUS_UPLOADED = "uploaded";

    public static final String USER_STATUS_REGISTERED = "user_registered";
    //    public static final String USER_STATUS_BOUND = "user_bound";
    public static final String USER_STATUS_FETCHED = "user_fetched";
    public static final String USER_STATUS_ACTIVATED = "user_vehicle_activated";

    public static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_CONNECT_DEVICE = 2;

    public static final long REFRESH_START = 1420070400000l;

    public static final int FLAG_ACTIVE_APPLICATION_REFUSE = 2;
    public static final int FLAG_ACTIVE_APPLICATION_AGREE = 3;
    public static final boolean DEBUG = false;

    // 动态来源
    public static final int DYNAMIC_SOURCE_PERSONAL = 0;
    public static final int DYNAMIC_SOURCE_OFFICIAL = 1;

    public enum AppState {
        FROM_LAUNCH,
        IN_FOREGROUND,
        IN_BACKGROUND,
        IN_CAMERA_PHOTO
    }

}