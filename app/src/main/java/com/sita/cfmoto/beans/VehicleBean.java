package com.sita.cfmoto.beans;

/**
 * Created by mark man on 2016/5/20.
 */
public class VehicleBean {
    public Long id = null;
    public String vin = "";
    public String vinpwd = "";

    public String controllerAddr = "";
    public String controllerKey = "";

    // fake data for B00G4U9FL3
//    public String controllerAddr =  "C0:27:15:09:B0:C9";
//    public String controllerKey = "NRWOipVE+USOlfiotZC56g==";

    // fake data for B00G3AA0C4
//    public String controllerAddr = "C0:27:15:09:B1:D8";
//    public String controllerKey = "/GSobadP5ZQAhjVhwWZq/w==";

    // fake data for Yadea Test
//    public String controllerAddr = "C0:27:15:09:B1:74";
//    public String controllerKey = "uEFmx5HRQ23oH1vy5yKIxw==";


    public int cpy = 1;
    public long accountId = 0;
    public boolean activated = false;
    public double lat;
    public double lng;

    public long timestamp = 0;

    // 剩余电量 %
    public int batteryRest = -1;
    // 电池寿命 %
//    public int batteryLife = 0;
    // 充电次数
    public int batteryExchangeCount = 0;
    // 剩余里程
    public float batteryDistanceRest = 0f;
    // 今日距离 米
    public long todayDistance = 0;
    // 今日骑行时间 分钟
    public long todayTime = 0;
    // 今日消耗电量
    public int todayCost = 0;

    // 总骑行里程 米
    public long totalMileage = 0;
    // 总骑行时间 分钟
    public long totalTime = 0;

    // 车辆状况 有故障、无故障
    public boolean isGood = true;

//    // 今日车况 0,1,2
//    public int todayVehicleStatus = 0;
}
