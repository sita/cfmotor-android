package com.sita.cfmoto.version;

import com.sita.cfmoto.version.rest.VersionModule;

/**
 * Created by xiaodong on 16/4/18.
 */
public interface CheckUpdateListener {
    void onSuccess(VersionModule vm);
    void onFail(Throwable throwable);
}
