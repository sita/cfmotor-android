package com.sita.cfmoto.version.rest;

import com.google.gson.annotations.SerializedName;
import com.sita.cfmoto.rest.model.response.Reply;

/**
 * Created by xiaodong on 16/4/18.
 */
public class VersionCheckResponse extends Reply {

    @SerializedName("data")
    public VersionModule versionModule;
}
