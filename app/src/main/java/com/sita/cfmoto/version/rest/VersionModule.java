package com.sita.cfmoto.version.rest;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by xiaodong on 16/4/18.
 */
public class VersionModule {
    private String platform;
    private int versionCode;
    private String versionName;
    private String url;
    private List<String> description;

    public List<String> getDescription() {
        return description;
    }

    public VersionModule setDescription(List<String> description) {
        this.description = description;
        return this;
    }

    public String getPlatform() {
        return platform;
    }

    public VersionModule setPlatform(String platform) {
        this.platform = platform;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public VersionModule setUrl(String url) {
        this.url = url;
        return this;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public VersionModule setVersionCode(int versionCode) {
        this.versionCode = versionCode;
        return this;
    }

    public String getVersionName() {
        return versionName;
    }

    public VersionModule setVersionName(String versionName) {
        this.versionName = versionName;
        return this;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
