package com.sita.cfmoto.version.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.LocalStorage;
import com.sita.cfmoto.utils.L;
import com.sita.cfmoto.utils.DateUtils;
import com.sita.cfmoto.version.CheckUpdateListener;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by xiaodong on 16/4/18.
 */
public enum VersionCheckHelper {
    INSTANCE;

    private interface VersionService {
        @GET("/yadea/v1/versions")
        void checkNewVersion(@Query("platform") String platform, Callback<VersionCheckResponse> callback);
    }

    private VersionService versionService;

    private void initRestService() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(120, TimeUnit.SECONDS);

        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().serializeNulls()
                .create();

        RequestInterceptor restInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Content-Type", Constants.APPLICATION_JSON);
                request.addHeader("Accept", Constants.APPLICATION_JSON);
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Constants.BASE_URI)
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(restInterceptor)
                .setClient(new OkClient(okHttpClient))
                .build();

        versionService = restAdapter.create(VersionService.class);
    }

    VersionCheckHelper() {
        initRestService();
    }

    public void checkNewVersion(final CheckUpdateListener listener) {
        long checkUpdateTime = LocalStorage.getCheckUpdateTime();
        if (!DateUtils.isSameDay(checkUpdateTime, System.currentTimeMillis())) {
            versionService.checkNewVersion("android", new Callback<VersionCheckResponse>() {
                        @Override
                        public void success(VersionCheckResponse vcr, Response response) {
                            LocalStorage.updateCheckUpdateTime(System.currentTimeMillis());
                            VersionModule vm = vcr.versionModule;
                            L.d("checkNewVersion()", vm);
                            listener.onSuccess(vm);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            L.e("checkNewVersion()", error);
                        }
                    }
            );
        }
    }

}
