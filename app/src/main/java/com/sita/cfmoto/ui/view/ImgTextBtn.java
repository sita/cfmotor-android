package com.sita.cfmoto.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sita.cfmoto.R;

public class ImgTextBtn extends LinearLayout {

    private static final String TAG = ImgTextBtn.class.getSimpleName();

    private ImageView mImg;
    private TextView mTV;
    private Context mContext = null;

    public ImgTextBtn(Context context) {
        this(context, null);
        mContext = context;
    }

    protected void inflaterLayout(Context context) {
        LayoutInflater.from(context).inflate(R.layout.img_text_btn, this, true);
    }

    @SuppressLint("NewApi")
    public ImgTextBtn(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        inflaterLayout(context);
        mImg = (ImageView) findViewById(R.id.widget_image_text_btn_Img);
        mImg.setAdjustViewBounds(true);
        mTV = (TextView) findViewById(R.id.widget_image_text_btn_TV);
        mImg.setClickable(false);

        TypedArray typeArray = context.obtainStyledAttributes(attrs,
                R.styleable.ImgTextBtn);
        if (typeArray == null) {
            return;
        }
        int count = typeArray.getIndexCount();
        int resId = 0;
        for (int i = 0; i < count; i++) {
            int attr = typeArray.getIndex(i);
            switch (attr) {
                case R.styleable.ImgTextBtn_ImgDraw:
                    Drawable background = typeArray.getDrawable(attr);
                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        mImg.setBackgroundDrawable(background);
                    } else {
//                        mImg.setBackground(background);
                        mImg.setImageDrawable(background);
                    }
                    break;
                case R.styleable.ImgTextBtn_ImgDrawWidth_Height:
                    int imageWidth = typeArray.getDimensionPixelSize(attr, -1);
                    ViewGroup.LayoutParams lp = mImg.getLayoutParams();
                    lp.width = imageWidth;
                    lp.height = imageWidth;
                    mImg.setLayoutParams(lp);
                    mImg.setScaleType(ImageView.ScaleType.FIT_XY);
                    break;
                case R.styleable.ImgTextBtn_TVText:
                    mTV.setText(typeArray.getText(attr));
                    break;
                case R.styleable.ImgTextBtn_TVTextSize:
                    mTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, typeArray.getDimensionPixelSize(attr, 15));
                    break;
                case R.styleable.ImgTextBtn_TVTextColor:
                    mTV.setTextColor(typeArray.getColor(attr, Color.BLACK));
                    break;
            }
        }
        typeArray.recycle();
    }

    @Override
    public void refreshDrawableState() {
        super.refreshDrawableState();
        //------------接下来处理联动效果，关键代码，请认真看-------------------
        //------------ ImageView控件的联动刷新 -------------------
        Drawable imgDrawable = mImg.getBackground(); //获取drawable资源
        Log.d(TAG, "drawable = " + imgDrawable);
        if (imgDrawable != null && imgDrawable.isStateful()) {
            //关键中的关键，根据当前状态设置drawable的状态，本来应该是LinearLayout的getDrawableState()状态，
            //但是现在是实现联动效果，而且获取的ImageView的getDrawState()结果不对。
            imgDrawable.setState(this.getDrawableState());
        }

        ColorStateList mTextColor = mTV.getTextColors();
        int color = mTextColor.getColorForState(this.getDrawableState(), 0);
        if (mTV.getCurrentTextColor() != color) {
            mTV.getPaint().setColor(color);
            //          mTV.setTextColor(color);
            mTV.invalidate();
        }

    }

    public void setImgViewResource(int resId) {
        mImg.setImageResource(resId);
    }

    public void setImgViewResource(Bitmap bitmap) {
        mImg.setImageBitmap(bitmap);
    }

    public void setTextViewText(int resId) {
        mTV.setText(resId);
    }

    public void setTextViewText(String text) {
        mTV.setText(text);
    }


}
