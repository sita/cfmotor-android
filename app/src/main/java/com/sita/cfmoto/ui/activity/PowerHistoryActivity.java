package com.sita.cfmoto.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sita.cfmoto.R;
import com.sita.cfmoto.model.PowerHistoryModel;
import com.sita.cfmoto.rest.model.response.FetchVehicleBatteryHistoryResponse;
import com.sita.cfmoto.support.GlobalContext;
import com.sita.cfmoto.ui.adapter.PowerHistoryAdapter;
import com.sita.cfmoto.utils.VehicleUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PowerHistoryActivity extends ActivityBase implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    @Bind(R.id.power_history_charging_list)
    SuperRecyclerView mChargingListView;

    private PowerHistoryAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private int currPage = 0;
    private boolean isRequesting = false;
    private boolean isAskMore = false;

    @SuppressWarnings("unused")
    public static Intent newIntent() {
        Intent intent = new Intent(GlobalContext.getGlobalContext(), PowerHistoryActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_history);
        ButterKnife.bind(this);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mChargingListView.setLayoutManager(mLinearLayoutManager);

        mAdapter = new PowerHistoryAdapter(this);
        mChargingListView.setAdapter(mAdapter);

        mChargingListView.setupMoreListener(this, 0);
        mChargingListView.setRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (currPage == 0) {
            fetchHistory();
        }
    }

    @Override
    public void onRefresh() {
        if (isRequesting) return;
        isAskMore = false;
        currPage = 0;
        fetchHistory();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        if (isRequesting) return;
        isAskMore = true;
        currPage += 1;
        fetchHistory();
    }

    private void fetchHistory() {
        isRequesting = true;
        VehicleUtils.fetchVehicleBatteryHistoryState(new VehicleUtils.FetchVehicleBatteryHistoryListener() {
            @Override
            public void onSuccess(List<FetchVehicleBatteryHistoryResponse> list) {
                isRequesting = false;
                if (list == null && isAskMore == true) {
                    // ask more 失败，则重新请求当前页
                    currPage -= 1;
                } else {
                    extractData(list);
                }
            }
        }, currPage);

//        ArrayList<PowerHistoryModel> items = new ArrayList<>();
//        for (int i = 0; i < 3; i++) {
//            PowerHistoryModel model = new PowerHistoryModel();
//            model.date = System.currentTimeMillis() + 1000 * 60 * 60 * 24 * i;
//            model.duration = 1000 * 60 * i + 0l;
//            model.volume = 0.2f * i;
//
//            items.add(model);
//        }
//        mLinearLayoutManager.scrollToPosition(0);
//        mAdapter.setData(items);
//        mChargingListView.setRefreshing(false);

    }

    private void extractData(List<FetchVehicleBatteryHistoryResponse> list) {
        if (list == null) {
            return;
        }

        ArrayList<PowerHistoryModel> items = new ArrayList<>();
        for (FetchVehicleBatteryHistoryResponse response :
                list) {
            PowerHistoryModel model = new PowerHistoryModel();
            model.date = response.startTime;
            model.duration = response.endTime - response.startTime;
            model.volume = (float)(response.esoc - response.bsoc);

            // save data into database
//            TracksInfoUtils.saveRouteIntoDbWithoutPoints(bean);
            // after save data into database, bean has id saved

            items.add(model);

        }
        if (items != null && items.size() > 0) {
            if (isAskMore) {
                mAdapter.append(items);
            } else {
                mAdapter.setData(items);
            }
            mAdapter.notifyDataSetChanged();
        }

    }

    @OnClick(R.id.img_back)
    void onBack() {
        this.finish();
    }

}
