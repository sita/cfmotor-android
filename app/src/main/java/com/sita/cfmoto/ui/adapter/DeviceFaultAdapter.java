package com.sita.cfmoto.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.model.DeviceFaultModel;
import com.sita.cfmoto.ui.widget.BaseTreeViewAdapter;
import com.sita.cfmoto.ui.widget.TreeView;


/**
 * TreeView demo adapter
 *
 * @author markmjw
 * @date 2014-01-04
 */
public class DeviceFaultAdapter extends BaseTreeViewAdapter {
    private LayoutInflater mInflater;

    public DeviceFaultAdapter(Context context, TreeView treeView) {
        super(treeView);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
//        return mChildren[groupPosition][childPosition];
        DeviceFaultModel fault = DeviceFaultModel.getFault(groupPosition, childPosition);
        return (fault == null) ? null : fault.item;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
//        return mChildren[groupPosition].length;
        return DeviceFaultModel.getItems(groupPosition);
    }

    @Override
    public Object getGroup(int groupPosition) {
//        return mGroups[groupPosition];
        return DeviceFaultModel.getModel(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return DeviceFaultModel.getModels();
//        return mGroups.length;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

//    private boolean getStatus(int group, int child) {
//        if (mValue != null && group < mValue.length && child < mValue[group].length) {
//            return mValue[group][child];
//        }
//        return true;
//    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.device_state_list_item_view, null);
        }

        ChildHolder holder = getChildHolder(convertView);
        holder.name.setText(String.valueOf(getChild(groupPosition, childPosition)));
        boolean status = DeviceFaultModel.getFault(groupPosition, childPosition).getStatus();
        holder.statusOk.setVisibility(status ? View.VISIBLE : View.INVISIBLE);
        holder.statusFail.setVisibility(status ? View.INVISIBLE : View.VISIBLE);
//        holder.price.setVisibility(!status ? View.VISIBLE : View.INVISIBLE);
//        float price = DeviceFaultModel.getFault(groupPosition, childPosition).getPrice();
//        holder.price.setText(String.valueOf((int)price));
        holder.top.setVisibility((childPosition == 0) ? View.VISIBLE : View.INVISIBLE);
        holder.bottom.setVisibility(isLastChild ? View.VISIBLE : View.INVISIBLE);
        return convertView;
    }

    private ChildHolder getChildHolder(final View view) {
        ChildHolder holder = (ChildHolder) view.getTag();
        if (null == holder) {
            holder = new ChildHolder(view);
            view.setTag(holder);
        }
        return holder;
    }

    private class ChildHolder {
        TextView name;
//        TextView price;
        TextView bottom;
        TextView top;
        ImageView statusOk;
        ImageView statusFail;

        public ChildHolder(View view) {
            name = (TextView) view.findViewById(R.id.device_state_list_item);
//            price = (TextView) view.findViewById(R.id.device_state_list_item_price);
            statusOk = (ImageView) view.findViewById(R.id.device_state_icon_ok);
            statusFail = (ImageView) view.findViewById(R.id.device_state_icon_fail);
            top = (TextView) view.findViewById(R.id.device_state_list_item_divider_top);
            bottom = (TextView) view.findViewById(R.id.device_state_list_item_divider_bottom);
        }
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.device_state_list_group_view, null);
        }

        GroupHolder holder = getGroupHolder(convertView);

        holder.name.setText(DeviceFaultModel.getModel(groupPosition));
//        holder.price.setVisibility((groupPosition == 0) ? View.VISIBLE : View.INVISIBLE);

        return convertView;
    }

    private GroupHolder getGroupHolder(final View view) {
        GroupHolder holder = (GroupHolder) view.getTag();
        if (null == holder) {
            holder = new GroupHolder(view);
            view.setTag(holder);
        }
        return holder;
    }

    private class GroupHolder {
        TextView name;
        TextView price;

        public GroupHolder(View view) {
            name = (TextView) view.findViewById(R.id.device_state_module_name);
            price = (TextView) view.findViewById(R.id.device_state_module_price_item);
        }
    }

    @Override
    public void updateHeader(View header, int groupPosition, int childPosition, int alpha) {
//        ((TextView) header.findViewById(R.id.module_name)).setText(mGroups[groupPosition]);
//        ((TextView) header.findViewById(R.id.online_count)).setText(getChildrenCount
//                (groupPosition) + "/" + getChildrenCount(groupPosition));
//        header.setAlpha(alpha);
    }
}
