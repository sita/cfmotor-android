package com.sita.cfmoto.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MyTrafficStyle;
import com.sita.cfmoto.R;
import com.sita.cfmoto.persistence.Route;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.MapUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MapFragment extends Fragment {
    private static final LatLng TIANJIN = new LatLng(39.08662, 117.203887);
    private static final CameraPosition CAMERA_POSITION_TIANJIN = new CameraPosition.Builder()
            .target(TIANJIN).zoom(Constants.LOCATION_ZOOM).bearing(0).tilt(0).build();
    public static MapFragment mapFragment;
    private static List<Route> dayRoutes;
    @Bind(R.id.roadmates_map)
    public MapView mapView;
    protected AMap aMap;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment getInstance(List<Route> tracks) {
        dayRoutes = tracks;
        mapFragment = new MapFragment();
        return mapFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);
        mapView.onCreate(savedInstanceState);
        initMap();
        setTraffic();
        MapUtils.displayMultiTracks(dayRoutes, aMap);
        return view;
    }

    @Override
    public void onResume() {
        if (mapView != null) {
            mapView.onResume();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (mapView != null) {
            mapView.onPause();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (mapView != null) {
            mapView.onDestroy();
        }
        super.onDestroy();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapView != null) {
            mapView.onSaveInstanceState(outState);
        }
    }

    private void setMapUi() {
        UiSettings mUiSettings = aMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);
        mUiSettings.setLogoPosition(AMapOptions.LOGO_POSITION_BOTTOM_LEFT);
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(CAMERA_POSITION_TIANJIN);
        aMap.moveCamera(update);
    }

    private void setTraffic() {
        MyTrafficStyle myTrafficStyle = new MyTrafficStyle();
        myTrafficStyle.setSeriousCongestedColor(0xff92000a);
        myTrafficStyle.setCongestedColor(0xffea0312);
        myTrafficStyle.setSlowColor(0xffff7508);
        myTrafficStyle.setSmoothColor(0xff00a209);
        aMap.setTrafficEnabled(false);
    }

    protected void initMap() {

        if (aMap == null) {
            aMap = mapView.getMap();
            setMapUi();
        }
        setTraffic();
    }



}
