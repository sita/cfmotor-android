package com.sita.cfmoto.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sita.cfmoto.utils.DateUtils;
import com.sita.cfmoto.utils.NumberUtils;
import com.sita.cfmoto.R;
import com.sita.cfmoto.model.PowerHistoryModel;
import com.sita.cfmoto.support.GlobalContext;

import java.util.ArrayList;
import java.util.List;

public class PowerHistoryAdapter extends RecyclerView.Adapter<PowerHistoryAdapter.ViewHolder> {

    private ArrayList<PowerHistoryModel> mValues = new ArrayList<PowerHistoryModel>();

    private Context mContext;

    public PowerHistoryAdapter(Context context) {
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.power_history_charging_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PowerHistoryModel item = mValues.get(position);
        holder.bindData(item);

    }

    public PowerHistoryModel getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setData(List<PowerHistoryModel> items) {
        mValues.clear();
        mValues.addAll(items);
        notifyItemRangeChanged(0, items.size());
    }

    public void append(List<PowerHistoryModel> items) {
        int count = getItemCount();
        mValues.addAll(items);
        if (items.size() == 1) {
            notifyItemInserted(count);
        } else if (items.size() > 1) {
            notifyItemRangeInserted(count, items.size());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mDateView;
        private TextView mVolumeView;
        private TextView mDurationView;

        public ViewHolder(View view) {
            super(view);

            mDateView = (TextView) view.findViewById(R.id.power_history_charging_list_item_date);
            mVolumeView = (TextView) view.findViewById(R.id.power_history_charging_list_item_volume);
            mDurationView = (TextView) view.findViewById(R.id.power_history_charging_list_item_duration);
        }

        public void bindData(PowerHistoryModel item) {
            mDateView.setText(GlobalContext.getGlobalContext().getString(R.string.power_history_list_charging_date,
                    DateUtils.formatDateTime(item.date)));
            mVolumeView.setText(GlobalContext.getGlobalContext().getString(R.string.power_history_list_charging_volume,
                    item.volume));
            String duration = NumberUtils.formatNumber((float) (item.duration / 1000 / 60) / 60f) + "小时";
            mDurationView.setText(GlobalContext.getGlobalContext().getString(R.string.power_history_list_charging_duration,
                    duration));

        }
    }

}
