package com.sita.cfmoto.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.BindUsers;
import com.sita.cfmoto.support.GlobalContext;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by lijuan zhang on 2016/7/28.
 */
public class BindUsersAdapter extends RecyclerView.Adapter<BindUsersAdapter.ViewHolder> {

    // 数据集
    private List<BindUsers> mBindUsersList;

    public BindUsersAdapter(List<BindUsers> mBindUsersList) {
        super();
        this.mBindUsersList = mBindUsersList;
    }

    @Override
    public BindUsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.bind_users_list_item, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(BindUsersAdapter.ViewHolder holder, int position) {

        BindUsers bindUsers = mBindUsersList.get(position);
        holder.bindUserName.setText(bindUsers.username);
        holder.bindUserMobile.setText(bindUsers.mobile);
        Picasso.with(GlobalContext.getGlobalContext()).load(bindUsers.allavatarfilename).error(R.drawable.default_head_icon).centerCrop().fit()
                .into(holder.bindUserIcon);


    }

    @Override
    public int getItemCount() {
        return mBindUsersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView bindUserIcon;
        public TextView bindUserName;
        public TextView bindUserMobile;

        public ViewHolder(View itemView) {
            super(itemView);
            bindUserIcon = (ImageView) itemView.findViewById(R.id.bind_user_icon);
            bindUserName = (TextView) itemView.findViewById(R.id.bind_user_name);
            bindUserMobile = (TextView) itemView.findViewById(R.id.bind_user_mobile);
        }
    }
}
