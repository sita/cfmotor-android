package com.sita.cfmoto.ui.activity;

/**
 * Created by mark on 2015/2/26.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sita.cfmoto.R;
import com.sita.cfmoto.login.LoginActivity;
import com.sita.cfmoto.support.Constants;
import com.sita.cfmoto.utils.SessionUtils;


public class LaunchActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        if (true/*LocalStorage.isFirstRun()*/) {
            Class<? extends AppCompatActivity> target = SessionUtils.isLoggedIn() ? MainActivity.class : LoginActivity.class;
            SplashScreen config = new SplashScreen(LaunchActivity.this)
                    .withFullScreen()
                    .withTargetActivity(target)
                    .withHeaderText(null)
                    .withFooterText(null)
                    .withBeforeLogoText(null)
                    .withAfterLogoText(null)
                    .withSplashTimeOut(Constants.AD_TIME)
                    .withLogo(R.mipmap.splash);
            config.getFooterTextView().setVisibility(View.GONE);
            config.getHeaderTextView().setVisibility(View.GONE);
            config.getBeforeLogoTextView().setVisibility(View.GONE);
            config.getAfterLogoTextView().setVisibility(View.GONE);

            View easySplashScreenView = config.create();
            setContentView(easySplashScreenView);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
