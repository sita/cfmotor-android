package com.sita.cfmoto.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.drivedata.DriveDataMainFragment;
import com.sita.cfmoto.support.GlobalContext;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriveStatActivity extends ActivityBase {

    @Bind(R.id.btn_back)
    ImageView mImgBack;
    @Bind(R.id.toolbar_title)
    TextView mTvTitle;
    private static int TAG = 0;

    public static Intent newIntent(int tag) {
        TAG = tag;
        return new Intent(GlobalContext.getGlobalContext(), DriveStatActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_stat);
        ButterKnife.bind(this);
        initTitle();
        DriveDataMainFragment.TAG = TAG;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.drive_stat_container, new DriveDataMainFragment());
        tr.commit();
    }

    private void initTitle() {
        mTvTitle.setText("骑行统计");
    }

    @OnClick(R.id.btn_back)
    public void OnClickBack(View view) {
        this.finish();
    }
}
