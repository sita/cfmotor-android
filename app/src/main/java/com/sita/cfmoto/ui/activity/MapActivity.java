package com.sita.cfmoto.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.sita.cfmoto.R;
import com.sita.cfmoto.shouye.ShouyeMapFragment;

import butterknife.ButterKnife;

public class MapActivity extends ActivityBase {

    private final String TAG = MapActivity.class.getSimpleName();

    ShouyeMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        initMap();
        initToolbar(getApplicationContext().getString(R.string.vehicle_location));
    }


    private void initMap() {
        // change the fragment
        FragmentTransaction ftx = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("MapFragment");
        if (fragment == null) {
            mapFragment = ShouyeMapFragment.newInstance();
            fragment = mapFragment;
            ftx.replace(R.id.map_container, fragment, "MapFragment");
            ftx.addToBackStack(null);
            ftx.commit();
        } else {
            ftx.show(fragment).commit();
        }

    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            mapFragment.handleResult(requestCode, resultCode, data);
        }
    }
}
