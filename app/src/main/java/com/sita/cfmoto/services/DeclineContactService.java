package com.sita.cfmoto.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Class com.sita.bike.services is for
 *
 * @author xiaodong on 15/4/7.
 */
public class DeclineContactService extends IntentService {

    public DeclineContactService() {
        super("DeclineContactService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extra = intent.getExtras();
        int notificationId = extra.getInt("notificationId");

        NotificationManager mNotification = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotification.cancel(notificationId);
    }
}
