package com.sita.cfmoto.drivedata.rest;

import com.amap.api.maps.model.LatLng;

/**
 * Created by wxd on 16/5/8.
 */
public class RoutePoint {
    private double latitude;
    private double longitude;
    private double altitude;
    private long time;

    public long getTime() {
        return time;
    }

    public RoutePoint setTime(long time) {
        this.time = time;
        return this;
    }

    public double getAltitude() {
        return altitude;
    }

    public RoutePoint setAltitude(double altitude) {
        this.altitude = altitude;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public RoutePoint setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public RoutePoint setLatLng(LatLng latLng){
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
        return this;
    }

    public RoutePoint setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }
}
