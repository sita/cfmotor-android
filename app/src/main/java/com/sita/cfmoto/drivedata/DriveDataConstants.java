package com.sita.cfmoto.drivedata;

import com.sita.cfmoto.drivedata.rest.DriveDataBean;

/**
 * Created by wxd on 16/5/7.
 */
public class DriveDataConstants {
    public static final String DAY = "DriveDataDay";
    public static final String MONTH = "DriveDataMonth";
    public static final String HISTORY = "DriveDataHistory";

    public static DriveDataBean driveData;
}
