package com.sita.cfmoto.drivedata.rest;

import java.util.ArrayList;

/**
 * Created by wxd on 16/5/7.
 */
public class DriveDataBean {

    public long id;
    public long serverId;
    public long begintime;
    public long endtime;
    public float distance; // unit is 米
    public float speed;
    public float soc;
    public String routefile;

    public ArrayList<RoutePoint> routePoints;
}
