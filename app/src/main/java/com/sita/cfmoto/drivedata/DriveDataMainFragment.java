package com.sita.cfmoto.drivedata;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.sita.cfmoto.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DriveDataMainFragment extends Fragment {

    public static int TAG = 0;
    @Bind(R.id.travel_tab_host)
    TabHost driveStatTab;
    private View monthData, dayData, historyData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drive_data, null);
        ButterKnife.bind(this, view);
        monthData = LayoutInflater.from(getActivity()).inflate(R.layout.tab_month_data, null);
        dayData = LayoutInflater.from(getActivity()).inflate(R.layout.tab_day_data, null);
        historyData = LayoutInflater.from(getActivity()).inflate(R.layout.tab_history_data, null);
        setTab();
        return view;
    }

    private void setTab() {

        driveStatTab.setup();

        TabHost.TabSpec tab_month_data = driveStatTab.newTabSpec("monthdata");
        tab_month_data.setIndicator(monthData);
        tab_month_data.setContent(R.id.fragment_month_data);
        driveStatTab.addTab(tab_month_data);

        TabHost.TabSpec tab_day_data = driveStatTab.newTabSpec("daydata");
        tab_day_data.setContent(R.id.fragment_day_data);
        tab_day_data.setIndicator(dayData);
        driveStatTab.addTab(tab_day_data);

        TabHost.TabSpec tab_history_data = driveStatTab.newTabSpec("historydata");
        tab_history_data.setContent(R.id.fragment_history_data);
        tab_history_data.setIndicator(historyData);
        driveStatTab.addTab(tab_history_data);

        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_month_data, new DriveDataMonth());
        transaction.replace(R.id.fragment_day_data, new DriveDataDay());
        transaction.replace(R.id.fragment_history_data, new DriveDataHistoryFragment());
        transaction.commit();

        if (TAG == 1) {
            driveStatTab.setCurrentTab(2);
        }
    }
}
