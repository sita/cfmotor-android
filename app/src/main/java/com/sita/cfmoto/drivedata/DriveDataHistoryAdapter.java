package com.sita.cfmoto.drivedata;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sita.cfmoto.R;
import com.sita.cfmoto.drivedata.rest.DriveDataBean;
import com.sita.cfmoto.utils.FormatUtils;
import com.sita.cfmoto.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wxd on 16/5/7.
 */
public class DriveDataHistoryAdapter extends RecyclerView.Adapter<DriveDataHistoryAdapter.ViewHolder> {

    private ArrayList<DriveDataBean> mData = new ArrayList<DriveDataBean>();

    private Context mContext;

    public DriveDataHistoryAdapter() {
    }

    public void setData(List<DriveDataBean> data) {
        this.mData.clear();
        this.mData.addAll(data);
        notifyItemRangeChanged(0, data.size());
    }

    public void appendData(List<DriveDataBean> data) {
        int currCount = getItemCount();
        this.mData.addAll(data);
        notifyItemRangeInserted(currCount, data.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_drive_data_history, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DriveDataBean data = mData.get(position);
        String startTime = DateUtils.formatDate(data.begintime, DateUtils.DATETIME_FORMAT);
        holder.startTimeView.setText(mContext.getString(R.string.start_time, startTime));
        String endTime = DateUtils.formatDate(data.endtime, DateUtils.DATETIME_FORMAT);
        holder.endTimeView.setText(mContext.getString(R.string.end_time, endTime));
        String distance = FormatUtils.formatDistance2(data.distance);
        holder.distanceView.setText(mContext.getString(R.string.tl_distance, distance));
//        String speed = NumberUtils.formatNumber(data.speed);
//        holder.speedView.setText(mContext.getString(R.string.ave_speed, speed));
//        String soc = NumberUtils.formatNumber(data.soc);
//        holder.socView.setText(mContext.getString(R.string.cost_battery, soc));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public DriveDataBean getItem(int position) {
        if (position < getItemCount()) {
            return mData.get(position);
        } else {
            return null;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView startTimeView;
        TextView endTimeView;
        TextView distanceView;
//        TextView speedView;
//        TextView socView;

        public ViewHolder(View itemView) {
            super(itemView);
            startTimeView = (TextView) itemView.findViewById(R.id.start_time);
            endTimeView = (TextView) itemView.findViewById(R.id.end_time);
            distanceView = (TextView) itemView.findViewById(R.id.distance);
//            speedView = (TextView) itemView.findViewById(R.id.speed);
//            socView = (TextView) itemView.findViewById(R.id.soc);
        }

    }
}
