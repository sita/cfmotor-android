package com.sita.cfmoto.shouye;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.amap.api.maps.AMap;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.overlay.DrivingRouteOverlay;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.DrivePath;
import com.sita.cfmoto.R;
import com.sita.cfmoto.rest.model.response.RepairStore;
import com.sita.cfmoto.utils.BitmapUtils;

/**
 * Created by wxd on 16/6/17.
 */
public class MyDrivingRouteOverlay extends DrivingRouteOverlay {
    private int mIconSize;
    private Context mContext;
    private RepairStore mRepairStore;
    public MyDrivingRouteOverlay(Context context, AMap aMap, DrivePath drivePath,
                                 LatLonPoint latLonPoint, LatLonPoint latLonPoint1, RepairStore repairStore) {
        super(context, aMap, drivePath, latLonPoint, latLonPoint1);
        mContext = context;
        mIconSize = (int) mContext.getResources().getDimension(R.dimen.map_icon_size_small);
        mRepairStore = repairStore;
    }

    @Override
    protected void addStartAndEndMarker() {
        this.startMarker = this.mAMap.addMarker((new MarkerOptions()).position(this.startPoint).icon(this.getStartBitmapDescriptor())
                .title("我的位置"));
        if (mRepairStore!=null){
        this.endMarker = this.mAMap.addMarker((new MarkerOptions()).position(this.endPoint).icon(this.getEndBitmapDescriptor())
                .title(mRepairStore.storeAddress));}else {
            this.endMarker = this.mAMap.addMarker((new MarkerOptions()).position(this.endPoint).icon(this.getEndBitmapDescriptor())
                    .title("目的地"));
        }
    }

    @Override
    protected BitmapDescriptor getEndBitmapDescriptor() {
        Bitmap bitmap;
        if (mRepairStore!=null){
        bitmap = BitmapUtils.zoomBitmap(BitmapFactory
                        .decodeResource(mContext.getResources(), R.drawable.map_service_small),
                mIconSize, mIconSize);}else {
            bitmap = BitmapUtils.zoomBitmap(BitmapFactory
                            .decodeResource(mContext.getResources(), R.mipmap.location_icon),
                    mIconSize, mIconSize);
        }
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    protected BitmapDescriptor getStartBitmapDescriptor() {
        Bitmap bitmap = BitmapUtils.zoomBitmap(BitmapFactory
                        .decodeResource(mContext.getResources(), R.drawable.location_me),
                mIconSize, mIconSize);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
