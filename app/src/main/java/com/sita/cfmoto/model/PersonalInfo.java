package com.sita.cfmoto.model;

/**
 * Created by new on 2016/3/16.
 */
public class PersonalInfo {
    private double total;
    private long time;
    private long rank;
    private long trendCount;
    private boolean isTotal = false;
    private boolean isTime = false;
    private boolean isRank = false;
    private boolean isTrendCount = false;

    public boolean isTrendCount() {
        return isTrendCount;
    }

    public boolean isRank() {
        return isRank;
    }

    public boolean isTime() {
        return isTime;
    }

    public boolean isTotal() {
        return isTotal;
    }

    public long getTrendCount() {
        return trendCount;
    }

    public void setTrendCount(long trendCount) {
        this.trendCount = trendCount;
        isTrendCount = true;
    }

    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
        isRank = true;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
        isTime = true;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
        isTotal = true;
    }
}
