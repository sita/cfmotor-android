package com.sita.cfmoto.model;


//private String[] mGroups = {
//        "能源模块:", "控制模块:", "动力模块:", "车载电脑故障:", "通信故障", "其他"};
//
//private String[][] mChildren = {
//        {"短路故障", "过流故障", "欠压故障", "BMS故障"},
//        {"控制器故障", "通讯故障","前照灯故障","后尾灯故障", "转把故障","刹把故障" ,"喇叭故障"},
//        {"电机故障", "转换器故障","MOS故障", "运放故障","缺相故障","霍尔故障","堵转故障","总线故障"},
//        {"EDR故障", "BLE故障", "GSENSOR故障"},
//        {"GPS故障", "无回应故障", "遥控器故障"},
//        {"左右转向故障"}};

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mark on 2016/5/7.
 */
public class DeviceFaultModel {
    private static final int FAULT_STATUS_UNKNOWN = 2;
    private static final int FAULT_STATUS_GOOD = 0;
    private static final int FAULT_STATUS_BAD = 1;


    public static final String MODEL_1 = "";
//    public static final String MODEL_2 = "点火系统";
//    public static final String MODEL_3 = "控制系统";
//    public static final String MODEL_BATTERY = "电池";
//    public static final String MODEL_COM = "通讯";
//    public static final String MODEL_OTHER = "其他";

    public int model;
    public String item;
    private float price = 0f; // RMB ￥
    private int status = FAULT_STATUS_GOOD;

    public void setStatus(boolean isok) {
        status = (isok) ? FAULT_STATUS_GOOD : FAULT_STATUS_BAD;
    }

    public boolean getStatus() {
        return status == FAULT_STATUS_GOOD;
    }

    public float getPrice() {
        return price;
    }

    public static String[] models = {MODEL_1
//            ,
//            MODEL_2,
//            MODEL_3
    };

    public static final int MODEL_ITEM_1 = 0;
    public static final int MODEL_ITEM_2 = 1;
    public static final int MODEL_ITEM_3 = 2;
    public static final int MODEL_TOTAL = 3;

    public static ArrayList<DeviceFaultModel> mFaultList1 = new ArrayList<>();
    public static ArrayList<DeviceFaultModel> mFaultList2 = new ArrayList<>();
    public static ArrayList<DeviceFaultModel> mFaultList3 = new ArrayList<>();

    public DeviceFaultModel(String item, int model, float price) {
        this(item, model);
        this.price = price;
    }

    public DeviceFaultModel(String item, int model) {
        this.item = item;
        this.model = model;

        if (model == MODEL_ITEM_2) {
            mFaultList2.add(this);
        } else if (model == MODEL_ITEM_1) {
            mFaultList1.add(this);
        } else if (model == MODEL_ITEM_3) {
            mFaultList3.add(this);
        }

    }

    public static ArrayList<String> faultListString = new ArrayList<>();
    String [] faultListName = {};

    public static DeviceFaultModel Fault_1_1 = new DeviceFaultModel("氧传感器信号", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_2 = new DeviceFaultModel("进气压力信号", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_3 = new DeviceFaultModel("进气温度信号", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_4 = new DeviceFaultModel("冷却液温度信号", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_5 = new DeviceFaultModel("节气门位置信号", MODEL_ITEM_1, 0);

    public static DeviceFaultModel Fault_1_6 = new DeviceFaultModel("喷油器信号", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_7 = new DeviceFaultModel("触发器信号", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_8 = new DeviceFaultModel("风扇控制", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_9 = new DeviceFaultModel("系统蓄电池电压", MODEL_ITEM_1, 0);
    public static DeviceFaultModel Fault_1_10 = new DeviceFaultModel("故障灯", MODEL_ITEM_1, 0);

    public static DeviceFaultModel Fault_1_11 = new DeviceFaultModel("发动机温度", MODEL_ITEM_1, 0);


    public static int getItems(int model) {
        switch (model) {
            case MODEL_ITEM_1:
                return mFaultList1.size();
            case MODEL_ITEM_2:
                return mFaultList2.size();
            case MODEL_ITEM_3:
                return mFaultList3.size();
            default:
                return 0;
        }
    }

    public static int getModels() {
        return models.length;
    }

    public static String getModel(int model) {
        return models[model];
    }

    public static DeviceFaultModel getFault(int model, int item) {
        DeviceFaultModel fault;
        switch (model) {
            case MODEL_ITEM_1:
                fault = mFaultList1.get(item);
                break;
            case MODEL_ITEM_2:
                fault = mFaultList2.get(item);
                break;
            case MODEL_ITEM_3:
                fault = mFaultList3.get(item);
                break;
            default:
                fault = null;
        }
        return fault;
    }

    private static float mTotalMark = 0f;
    private static int mTotalItems = 0;
    private static int mBadItems = 0;

    public static float getTotalPrice() {
        float total = 0f;
        mFaultList.clear();
        total += getModelPrice(mFaultList1);
        total += getModelPrice(mFaultList2);
        total += getModelPrice(mFaultList3);
        return total;
    }

    public static int getStatusMark() {
        if (mTotalItems == 0) {
            return 100;
        }

        mTotalMark = 1 - (float) (mBadItems) / (float) mTotalItems;
        // 百分
        return (int) ((mTotalMark + 0.005) * 100);
    }

    private static float getModelPrice(ArrayList<DeviceFaultModel> model) {
        float total = 0f;
        for (DeviceFaultModel fault : model
                ) {
            total += (fault.getStatus()) ? 0 : fault.getPrice();
            mTotalItems += 1;
            mBadItems += (fault.getStatus()) ? 0 : 1;
            if (!fault.getStatus()) { // todo remove it
                mFaultList.add(fault.item);
            }
        }
        return total;
    }

    private static List<String> mFaultList = new ArrayList<>();

    public static List<String> getFaultList() {
        return mFaultList;
    }

    public static List<String> getFaultList(ArrayList<DeviceFaultModel> modelList) {
        List<String> list = new ArrayList<>();
        for (DeviceFaultModel fault : modelList) {
            if (!fault.getStatus()) {
                list.add(fault.item);
            }
        }
        return list;
    }

    public static void clearData() {
        for (DeviceFaultModel item: mFaultList1
             ) {
            item.setStatus(true);
        }
    }
}
