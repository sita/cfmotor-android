package com.sita.cfmoto.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by new on 2016/5/8.
 */
public class FaultMapping {
    private final static String GOOD = "0";
    private final static String BAD = "1";
    private static Map<String, DeviceFaultModel> errorMap = new HashMap<>();

    public static void mapping(Map<String, String> faultMap) {

        if (faultMap != null && faultMap.size() > 0) {

            for (Map.Entry<String, String> entry : faultMap.entrySet()
                    ) {
                if (entry.getValue().equals(BAD)){
                    errorMap.get(entry.getKey()).setStatus(false);
                }
            }
        } else {
            DeviceFaultModel.Fault_1_1.setStatus(true);
            DeviceFaultModel.Fault_1_2.setStatus(true);
            DeviceFaultModel.Fault_1_3.setStatus(true);
            DeviceFaultModel.Fault_1_4.setStatus(false);
            DeviceFaultModel.Fault_1_5.setStatus(false);
            DeviceFaultModel.Fault_1_6.setStatus(false);
            DeviceFaultModel.Fault_1_7.setStatus(true);
            DeviceFaultModel.Fault_1_8.setStatus(true);
            DeviceFaultModel.Fault_1_9.setStatus(true);
            DeviceFaultModel.Fault_1_10.setStatus(true);
            DeviceFaultModel.Fault_1_11.setStatus(true);

        }
    }
    static {
        errorMap.put("p0030", DeviceFaultModel.Fault_1_1);
        errorMap.put("p0031", DeviceFaultModel.Fault_1_1);
        errorMap.put("p0032", DeviceFaultModel.Fault_1_1);
        errorMap.put("p0130", DeviceFaultModel.Fault_1_1);
        errorMap.put("p0131", DeviceFaultModel.Fault_1_1);
        errorMap.put("p0132", DeviceFaultModel.Fault_1_1);
        errorMap.put("p0134", DeviceFaultModel.Fault_1_1);

        errorMap.put("p0107", DeviceFaultModel.Fault_1_2);
        errorMap.put("p0108", DeviceFaultModel.Fault_1_2);

        errorMap.put("p0112", DeviceFaultModel.Fault_1_3);
        errorMap.put("p0113", DeviceFaultModel.Fault_1_3);

        errorMap.put("p0117", DeviceFaultModel.Fault_1_4);
        errorMap.put("p0118", DeviceFaultModel.Fault_1_4);

        errorMap.put("p0122", DeviceFaultModel.Fault_1_5);
        errorMap.put("p0123", DeviceFaultModel.Fault_1_5);

        errorMap.put("p0201", DeviceFaultModel.Fault_1_6);
        errorMap.put("p0261", DeviceFaultModel.Fault_1_6);
        errorMap.put("p0262", DeviceFaultModel.Fault_1_6);

        errorMap.put("p0322", DeviceFaultModel.Fault_1_7);

        errorMap.put("p0480", DeviceFaultModel.Fault_1_8);
        errorMap.put("p0691", DeviceFaultModel.Fault_1_8);
        errorMap.put("p0692", DeviceFaultModel.Fault_1_8);

        errorMap.put("p0560", DeviceFaultModel.Fault_1_9);
        errorMap.put("p0562", DeviceFaultModel.Fault_1_9);
        errorMap.put("p0563", DeviceFaultModel.Fault_1_9);

        errorMap.put("p0650", DeviceFaultModel.Fault_1_10);

        errorMap.put("p1116", DeviceFaultModel.Fault_1_11);
    }

}