package com.sita.cfmoto.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by xiaodong on 16/3/31.
 */
public class PowerHistoryModel implements Serializable {
    @SerializedName("date")
    public Long date;
    @SerializedName("duration")
    public Long duration;
    @SerializedName("volume")
    public Float volume;

}
