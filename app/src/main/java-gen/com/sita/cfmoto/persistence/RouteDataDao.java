package com.sita.cfmoto.persistence;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.sita.cfmoto.persistence.RouteData;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table ROUTE_DATA.
*/
public class RouteDataDao extends AbstractDao<RouteData, Long> {

    public static final String TABLENAME = "ROUTE_DATA";

    /**
     * Properties of entity RouteData.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Latitude = new Property(1, Double.class, "latitude", false, "LATITUDE");
        public final static Property Longitude = new Property(2, Double.class, "longitude", false, "LONGITUDE");
        public final static Property Time = new Property(3, Long.class, "time", false, "TIME");
        public final static Property Speed = new Property(4, Float.class, "speed", false, "SPEED");
        public final static Property Rpm = new Property(5, Integer.class, "rpm", false, "RPM");
        public final static Property Shift = new Property(6, Integer.class, "shift", false, "SHIFT");
        public final static Property RouteId = new Property(7, Integer.class, "routeId", false, "ROUTE_ID");
    };


    public RouteDataDao(DaoConfig config) {
        super(config);
    }
    
    public RouteDataDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'ROUTE_DATA' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'LATITUDE' REAL," + // 1: latitude
                "'LONGITUDE' REAL," + // 2: longitude
                "'TIME' INTEGER," + // 3: time
                "'SPEED' REAL," + // 4: speed
                "'RPM' INTEGER," + // 5: rpm
                "'SHIFT' INTEGER," + // 6: shift
                "'ROUTE_ID' INTEGER);"); // 7: routeId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'ROUTE_DATA'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, RouteData entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Double latitude = entity.getLatitude();
        if (latitude != null) {
            stmt.bindDouble(2, latitude);
        }
 
        Double longitude = entity.getLongitude();
        if (longitude != null) {
            stmt.bindDouble(3, longitude);
        }
 
        Long time = entity.getTime();
        if (time != null) {
            stmt.bindLong(4, time);
        }
 
        Float speed = entity.getSpeed();
        if (speed != null) {
            stmt.bindDouble(5, speed);
        }
 
        Integer rpm = entity.getRpm();
        if (rpm != null) {
            stmt.bindLong(6, rpm);
        }
 
        Integer shift = entity.getShift();
        if (shift != null) {
            stmt.bindLong(7, shift);
        }
 
        Integer routeId = entity.getRouteId();
        if (routeId != null) {
            stmt.bindLong(8, routeId);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public RouteData readEntity(Cursor cursor, int offset) {
        RouteData entity = new RouteData( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getDouble(offset + 1), // latitude
            cursor.isNull(offset + 2) ? null : cursor.getDouble(offset + 2), // longitude
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3), // time
            cursor.isNull(offset + 4) ? null : cursor.getFloat(offset + 4), // speed
            cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5), // rpm
            cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6), // shift
            cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7) // routeId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, RouteData entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setLatitude(cursor.isNull(offset + 1) ? null : cursor.getDouble(offset + 1));
        entity.setLongitude(cursor.isNull(offset + 2) ? null : cursor.getDouble(offset + 2));
        entity.setTime(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
        entity.setSpeed(cursor.isNull(offset + 4) ? null : cursor.getFloat(offset + 4));
        entity.setRpm(cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5));
        entity.setShift(cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6));
        entity.setRouteId(cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(RouteData entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(RouteData entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
