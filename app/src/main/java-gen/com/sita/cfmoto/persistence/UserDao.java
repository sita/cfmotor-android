package com.sita.cfmoto.persistence;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.sita.cfmoto.persistence.User;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table USER.
*/
public class UserDao extends AbstractDao<User, Long> {

    public static final String TABLENAME = "USER";

    /**
     * Properties of entity User.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property AccountId = new Property(1, Long.class, "accountId", false, "ACCOUNT_ID");
        public final static Property Mobile = new Property(2, String.class, "mobile", false, "MOBILE");
        public final static Property Email = new Property(3, String.class, "email", false, "EMAIL");
        public final static Property UserName = new Property(4, String.class, "userName", false, "USER_NAME");
        public final static Property NickName = new Property(5, String.class, "nickName", false, "NICK_NAME");
        public final static Property UniqueId = new Property(6, String.class, "uniqueId", false, "UNIQUE_ID");
        public final static Property Avatar = new Property(7, String.class, "avatar", false, "AVATAR");
        public final static Property Gender = new Property(8, Integer.class, "gender", false, "GENDER");
        public final static Property Address = new Property(9, String.class, "address", false, "ADDRESS");
        public final static Property Birthday = new Property(10, String.class, "birthday", false, "BIRTHDAY");
        public final static Property Signature = new Property(11, String.class, "signature", false, "SIGNATURE");
        public final static Property XmppUser = new Property(12, String.class, "xmppUser", false, "xmpp_user");
        public final static Property XmppPass = new Property(13, String.class, "xmppPass", false, "xmpp_pass");
    };


    public UserDao(DaoConfig config) {
        super(config);
    }
    
    public UserDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'USER' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'ACCOUNT_ID' INTEGER," + // 1: accountId
                "'MOBILE' TEXT," + // 2: mobile
                "'EMAIL' TEXT," + // 3: email
                "'USER_NAME' TEXT," + // 4: userName
                "'NICK_NAME' TEXT," + // 5: nickName
                "'UNIQUE_ID' TEXT," + // 6: uniqueId
                "'AVATAR' TEXT," + // 7: avatar
                "'GENDER' INTEGER," + // 8: gender
                "'ADDRESS' TEXT," + // 9: address
                "'BIRTHDAY' TEXT," + // 10: birthday
                "'SIGNATURE' TEXT," + // 11: signature
                "'xmpp_user' TEXT," + // 12: xmppUser
                "'xmpp_pass' TEXT);"); // 13: xmppPass
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'USER'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, User entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Long accountId = entity.getAccountId();
        if (accountId != null) {
            stmt.bindLong(2, accountId);
        }
 
        String mobile = entity.getMobile();
        if (mobile != null) {
            stmt.bindString(3, mobile);
        }
 
        String email = entity.getEmail();
        if (email != null) {
            stmt.bindString(4, email);
        }
 
        String userName = entity.getUserName();
        if (userName != null) {
            stmt.bindString(5, userName);
        }
 
        String nickName = entity.getNickName();
        if (nickName != null) {
            stmt.bindString(6, nickName);
        }
 
        String uniqueId = entity.getUniqueId();
        if (uniqueId != null) {
            stmt.bindString(7, uniqueId);
        }
 
        String avatar = entity.getAvatar();
        if (avatar != null) {
            stmt.bindString(8, avatar);
        }
 
        Integer gender = entity.getGender();
        if (gender != null) {
            stmt.bindLong(9, gender);
        }
 
        String address = entity.getAddress();
        if (address != null) {
            stmt.bindString(10, address);
        }
 
        String birthday = entity.getBirthday();
        if (birthday != null) {
            stmt.bindString(11, birthday);
        }
 
        String signature = entity.getSignature();
        if (signature != null) {
            stmt.bindString(12, signature);
        }
 
        String xmppUser = entity.getXmppUser();
        if (xmppUser != null) {
            stmt.bindString(13, xmppUser);
        }
 
        String xmppPass = entity.getXmppPass();
        if (xmppPass != null) {
            stmt.bindString(14, xmppPass);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public User readEntity(Cursor cursor, int offset) {
        User entity = new User( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // accountId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // mobile
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // email
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // userName
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // nickName
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // uniqueId
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // avatar
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // gender
            cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), // address
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), // birthday
            cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), // signature
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // xmppUser
            cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13) // xmppPass
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, User entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setAccountId(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setMobile(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setEmail(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setUserName(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setNickName(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setUniqueId(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setAvatar(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setGender(cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.setAddress(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setBirthday(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setSignature(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setXmppUser(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setXmppPass(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(User entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(User entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
