package com.sita.cfmoto.persistence;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table CC_DRIVER_TICKET_ENTITY.
 */
public class CcDriverTicketEntity implements java.io.Serializable {

    private Long id;
    private String driverId;
    private String passengerId;
    private String fromAddress;
    private String toAddress;
    private String fromLocation;
    private String toLocation;
    private Long departureTime;
    private Float money;
    private Integer status;

    public CcDriverTicketEntity() {
    }

    public CcDriverTicketEntity(Long id) {
        this.id = id;
    }

    public CcDriverTicketEntity(Long id, String driverId, String passengerId, String fromAddress, String toAddress, String fromLocation, String toLocation, Long departureTime, Float money, Integer status) {
        this.id = id;
        this.driverId = driverId;
        this.passengerId = passengerId;
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.departureTime = departureTime;
        this.money = money;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public Long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Long departureTime) {
        this.departureTime = departureTime;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
