package com.sita.cfmoto.persistence;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import com.sita.cfmoto.persistence.User;
import com.sita.cfmoto.persistence.Friend;
import com.sita.cfmoto.persistence.Tracks;
import com.sita.cfmoto.persistence.Route;
import com.sita.cfmoto.persistence.RouteData;
import com.sita.cfmoto.persistence.BluetoothData;
import com.sita.cfmoto.persistence.RtResourceEntity;
import com.sita.cfmoto.persistence.RtBlacklistEntity;
import com.sita.cfmoto.persistence.RtNoteEntity;
import com.sita.cfmoto.persistence.CcDriverEntity;
import com.sita.cfmoto.persistence.CcDriverTicketEntity;
import com.sita.cfmoto.persistence.CcPassengerTicketEntity;
import com.sita.cfmoto.persistence.GroupChatDetailEvent;
import com.sita.cfmoto.persistence.VehicleInfo;
import com.sita.cfmoto.persistence.BindedVehicle;
import com.sita.cfmoto.persistence.Dynamic;
import com.sita.cfmoto.persistence.TopicAuthor;
import com.sita.cfmoto.persistence.Topic;
import com.sita.cfmoto.persistence.Active;
import com.sita.cfmoto.persistence.Comment;

import com.sita.cfmoto.persistence.UserDao;
import com.sita.cfmoto.persistence.FriendDao;
import com.sita.cfmoto.persistence.TracksDao;
import com.sita.cfmoto.persistence.RouteDao;
import com.sita.cfmoto.persistence.RouteDataDao;
import com.sita.cfmoto.persistence.BluetoothDataDao;
import com.sita.cfmoto.persistence.RtResourceEntityDao;
import com.sita.cfmoto.persistence.RtBlacklistEntityDao;
import com.sita.cfmoto.persistence.RtNoteEntityDao;
import com.sita.cfmoto.persistence.CcDriverEntityDao;
import com.sita.cfmoto.persistence.CcDriverTicketEntityDao;
import com.sita.cfmoto.persistence.CcPassengerTicketEntityDao;
import com.sita.cfmoto.persistence.GroupChatDetailEventDao;
import com.sita.cfmoto.persistence.VehicleInfoDao;
import com.sita.cfmoto.persistence.BindedVehicleDao;
import com.sita.cfmoto.persistence.DynamicDao;
import com.sita.cfmoto.persistence.TopicAuthorDao;
import com.sita.cfmoto.persistence.TopicDao;
import com.sita.cfmoto.persistence.ActiveDao;
import com.sita.cfmoto.persistence.CommentDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig userDaoConfig;
    private final DaoConfig friendDaoConfig;
    private final DaoConfig tracksDaoConfig;
    private final DaoConfig routeDaoConfig;
    private final DaoConfig routeDataDaoConfig;
    private final DaoConfig bluetoothDataDaoConfig;
    private final DaoConfig rtResourceEntityDaoConfig;
    private final DaoConfig rtBlacklistEntityDaoConfig;
    private final DaoConfig rtNoteEntityDaoConfig;
    private final DaoConfig ccDriverEntityDaoConfig;
    private final DaoConfig ccDriverTicketEntityDaoConfig;
    private final DaoConfig ccPassengerTicketEntityDaoConfig;
    private final DaoConfig groupChatDetailEventDaoConfig;
    private final DaoConfig vehicleInfoDaoConfig;
    private final DaoConfig bindedVehicleDaoConfig;
    private final DaoConfig dynamicDaoConfig;
    private final DaoConfig topicAuthorDaoConfig;
    private final DaoConfig topicDaoConfig;
    private final DaoConfig activeDaoConfig;
    private final DaoConfig commentDaoConfig;

    private final UserDao userDao;
    private final FriendDao friendDao;
    private final TracksDao tracksDao;
    private final RouteDao routeDao;
    private final RouteDataDao routeDataDao;
    private final BluetoothDataDao bluetoothDataDao;
    private final RtResourceEntityDao rtResourceEntityDao;
    private final RtBlacklistEntityDao rtBlacklistEntityDao;
    private final RtNoteEntityDao rtNoteEntityDao;
    private final CcDriverEntityDao ccDriverEntityDao;
    private final CcDriverTicketEntityDao ccDriverTicketEntityDao;
    private final CcPassengerTicketEntityDao ccPassengerTicketEntityDao;
    private final GroupChatDetailEventDao groupChatDetailEventDao;
    private final VehicleInfoDao vehicleInfoDao;
    private final BindedVehicleDao bindedVehicleDao;
    private final DynamicDao dynamicDao;
    private final TopicAuthorDao topicAuthorDao;
    private final TopicDao topicDao;
    private final ActiveDao activeDao;
    private final CommentDao commentDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        userDaoConfig = daoConfigMap.get(UserDao.class).clone();
        userDaoConfig.initIdentityScope(type);

        friendDaoConfig = daoConfigMap.get(FriendDao.class).clone();
        friendDaoConfig.initIdentityScope(type);

        tracksDaoConfig = daoConfigMap.get(TracksDao.class).clone();
        tracksDaoConfig.initIdentityScope(type);

        routeDaoConfig = daoConfigMap.get(RouteDao.class).clone();
        routeDaoConfig.initIdentityScope(type);

        routeDataDaoConfig = daoConfigMap.get(RouteDataDao.class).clone();
        routeDataDaoConfig.initIdentityScope(type);

        bluetoothDataDaoConfig = daoConfigMap.get(BluetoothDataDao.class).clone();
        bluetoothDataDaoConfig.initIdentityScope(type);

        rtResourceEntityDaoConfig = daoConfigMap.get(RtResourceEntityDao.class).clone();
        rtResourceEntityDaoConfig.initIdentityScope(type);

        rtBlacklistEntityDaoConfig = daoConfigMap.get(RtBlacklistEntityDao.class).clone();
        rtBlacklistEntityDaoConfig.initIdentityScope(type);

        rtNoteEntityDaoConfig = daoConfigMap.get(RtNoteEntityDao.class).clone();
        rtNoteEntityDaoConfig.initIdentityScope(type);

        ccDriverEntityDaoConfig = daoConfigMap.get(CcDriverEntityDao.class).clone();
        ccDriverEntityDaoConfig.initIdentityScope(type);

        ccDriverTicketEntityDaoConfig = daoConfigMap.get(CcDriverTicketEntityDao.class).clone();
        ccDriverTicketEntityDaoConfig.initIdentityScope(type);

        ccPassengerTicketEntityDaoConfig = daoConfigMap.get(CcPassengerTicketEntityDao.class).clone();
        ccPassengerTicketEntityDaoConfig.initIdentityScope(type);

        groupChatDetailEventDaoConfig = daoConfigMap.get(GroupChatDetailEventDao.class).clone();
        groupChatDetailEventDaoConfig.initIdentityScope(type);

        vehicleInfoDaoConfig = daoConfigMap.get(VehicleInfoDao.class).clone();
        vehicleInfoDaoConfig.initIdentityScope(type);

        bindedVehicleDaoConfig = daoConfigMap.get(BindedVehicleDao.class).clone();
        bindedVehicleDaoConfig.initIdentityScope(type);

        dynamicDaoConfig = daoConfigMap.get(DynamicDao.class).clone();
        dynamicDaoConfig.initIdentityScope(type);

        topicAuthorDaoConfig = daoConfigMap.get(TopicAuthorDao.class).clone();
        topicAuthorDaoConfig.initIdentityScope(type);

        topicDaoConfig = daoConfigMap.get(TopicDao.class).clone();
        topicDaoConfig.initIdentityScope(type);

        activeDaoConfig = daoConfigMap.get(ActiveDao.class).clone();
        activeDaoConfig.initIdentityScope(type);

        commentDaoConfig = daoConfigMap.get(CommentDao.class).clone();
        commentDaoConfig.initIdentityScope(type);

        userDao = new UserDao(userDaoConfig, this);
        friendDao = new FriendDao(friendDaoConfig, this);
        tracksDao = new TracksDao(tracksDaoConfig, this);
        routeDao = new RouteDao(routeDaoConfig, this);
        routeDataDao = new RouteDataDao(routeDataDaoConfig, this);
        bluetoothDataDao = new BluetoothDataDao(bluetoothDataDaoConfig, this);
        rtResourceEntityDao = new RtResourceEntityDao(rtResourceEntityDaoConfig, this);
        rtBlacklistEntityDao = new RtBlacklistEntityDao(rtBlacklistEntityDaoConfig, this);
        rtNoteEntityDao = new RtNoteEntityDao(rtNoteEntityDaoConfig, this);
        ccDriverEntityDao = new CcDriverEntityDao(ccDriverEntityDaoConfig, this);
        ccDriverTicketEntityDao = new CcDriverTicketEntityDao(ccDriverTicketEntityDaoConfig, this);
        ccPassengerTicketEntityDao = new CcPassengerTicketEntityDao(ccPassengerTicketEntityDaoConfig, this);
        groupChatDetailEventDao = new GroupChatDetailEventDao(groupChatDetailEventDaoConfig, this);
        vehicleInfoDao = new VehicleInfoDao(vehicleInfoDaoConfig, this);
        bindedVehicleDao = new BindedVehicleDao(bindedVehicleDaoConfig, this);
        dynamicDao = new DynamicDao(dynamicDaoConfig, this);
        topicAuthorDao = new TopicAuthorDao(topicAuthorDaoConfig, this);
        topicDao = new TopicDao(topicDaoConfig, this);
        activeDao = new ActiveDao(activeDaoConfig, this);
        commentDao = new CommentDao(commentDaoConfig, this);

        registerDao(User.class, userDao);
        registerDao(Friend.class, friendDao);
        registerDao(Tracks.class, tracksDao);
        registerDao(Route.class, routeDao);
        registerDao(RouteData.class, routeDataDao);
        registerDao(BluetoothData.class, bluetoothDataDao);
        registerDao(RtResourceEntity.class, rtResourceEntityDao);
        registerDao(RtBlacklistEntity.class, rtBlacklistEntityDao);
        registerDao(RtNoteEntity.class, rtNoteEntityDao);
        registerDao(CcDriverEntity.class, ccDriverEntityDao);
        registerDao(CcDriverTicketEntity.class, ccDriverTicketEntityDao);
        registerDao(CcPassengerTicketEntity.class, ccPassengerTicketEntityDao);
        registerDao(GroupChatDetailEvent.class, groupChatDetailEventDao);
        registerDao(VehicleInfo.class, vehicleInfoDao);
        registerDao(BindedVehicle.class, bindedVehicleDao);
        registerDao(Dynamic.class, dynamicDao);
        registerDao(TopicAuthor.class, topicAuthorDao);
        registerDao(Topic.class, topicDao);
        registerDao(Active.class, activeDao);
        registerDao(Comment.class, commentDao);
    }
    
    public void clear() {
        userDaoConfig.getIdentityScope().clear();
        friendDaoConfig.getIdentityScope().clear();
        tracksDaoConfig.getIdentityScope().clear();
        routeDaoConfig.getIdentityScope().clear();
        routeDataDaoConfig.getIdentityScope().clear();
        bluetoothDataDaoConfig.getIdentityScope().clear();
        rtResourceEntityDaoConfig.getIdentityScope().clear();
        rtBlacklistEntityDaoConfig.getIdentityScope().clear();
        rtNoteEntityDaoConfig.getIdentityScope().clear();
        ccDriverEntityDaoConfig.getIdentityScope().clear();
        ccDriverTicketEntityDaoConfig.getIdentityScope().clear();
        ccPassengerTicketEntityDaoConfig.getIdentityScope().clear();
        groupChatDetailEventDaoConfig.getIdentityScope().clear();
        vehicleInfoDaoConfig.getIdentityScope().clear();
        bindedVehicleDaoConfig.getIdentityScope().clear();
        dynamicDaoConfig.getIdentityScope().clear();
        topicAuthorDaoConfig.getIdentityScope().clear();
        topicDaoConfig.getIdentityScope().clear();
        activeDaoConfig.getIdentityScope().clear();
        commentDaoConfig.getIdentityScope().clear();
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public FriendDao getFriendDao() {
        return friendDao;
    }

    public TracksDao getTracksDao() {
        return tracksDao;
    }

    public RouteDao getRouteDao() {
        return routeDao;
    }

    public RouteDataDao getRouteDataDao() {
        return routeDataDao;
    }

    public BluetoothDataDao getBluetoothDataDao() {
        return bluetoothDataDao;
    }

    public RtResourceEntityDao getRtResourceEntityDao() {
        return rtResourceEntityDao;
    }

    public RtBlacklistEntityDao getRtBlacklistEntityDao() {
        return rtBlacklistEntityDao;
    }

    public RtNoteEntityDao getRtNoteEntityDao() {
        return rtNoteEntityDao;
    }

    public CcDriverEntityDao getCcDriverEntityDao() {
        return ccDriverEntityDao;
    }

    public CcDriverTicketEntityDao getCcDriverTicketEntityDao() {
        return ccDriverTicketEntityDao;
    }

    public CcPassengerTicketEntityDao getCcPassengerTicketEntityDao() {
        return ccPassengerTicketEntityDao;
    }

    public GroupChatDetailEventDao getGroupChatDetailEventDao() {
        return groupChatDetailEventDao;
    }

    public VehicleInfoDao getVehicleInfoDao() {
        return vehicleInfoDao;
    }

    public BindedVehicleDao getBindedVehicleDao() {
        return bindedVehicleDao;
    }

    public DynamicDao getDynamicDao() {
        return dynamicDao;
    }

    public TopicAuthorDao getTopicAuthorDao() {
        return topicAuthorDao;
    }

    public TopicDao getTopicDao() {
        return topicDao;
    }

    public ActiveDao getActiveDao() {
        return activeDao;
    }

    public CommentDao getCommentDao() {
        return commentDao;
    }

}
