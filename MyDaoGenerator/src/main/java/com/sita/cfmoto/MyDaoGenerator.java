package com.sita.cfmoto;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {
    public static void main(String[] args) throws Exception {
        int version = Integer.valueOf(args[0]);
        String outputDir = args[1];
        Schema schema = new Schema(version, "com.sita.cfmoto.persistence");
        addUser(schema);

        addFriends(schema);
        addRouteTrackData(schema);

        addRtResource(schema);
        addRtBlacklist(schema);
        addRtNote(schema);

        addCcDriver(schema);
        addCcDriverTickets(schema);
        addCcPassengerTickets(schema);

        addGroupChatDetailEvent(schema);
        //addMembers(schema);
        //addTrendMember(schema);
        addVehicleInfo(schema);
        addInteract(schema);
        new DaoGenerator().generateAll(schema, outputDir);
    }

    private static void addUser(Schema schema) {
        Entity user = schema.addEntity("User");
        user.addIdProperty().columnName("_id").primaryKey();
        user.addLongProperty("accountId");
        user.addStringProperty("mobile");
        user.addStringProperty("email");
        user.addStringProperty("userName");
        user.addStringProperty("nickName");
        user.addStringProperty("uniqueId");
        user.addStringProperty("avatar");
        user.addIntProperty("gender");
        user.addStringProperty("address");
        user.addStringProperty("birthday");
        user.addStringProperty("signature");
        user.addStringProperty("xmppUser").columnName("xmpp_user");
        user.addStringProperty("xmppPass").columnName("xmpp_pass");

        user.implementsSerializable();
    }

    private static void addFriends(Schema schema) {
        Entity friend = schema.addEntity("Friend");
        friend.addIdProperty().columnName("_id").primaryKey();
        friend.addStringProperty("nickname");
        friend.addStringProperty("mobile");
        friend.addStringProperty("gender");
        friend.addStringProperty("avatarUrl");
        friend.addByteArrayProperty("photo");
        friend.implementsSerializable();
    }

    private static void addRtResource(Schema schema) {
        Entity entity = schema.addEntity("RtResourceEntity");
        entity.addIdProperty().primaryKeyDesc();
        entity.addStringProperty("title");
        entity.addStringProperty("trafficType");
        entity.addStringProperty("accountId");
        entity.addLongProperty("createDate").indexDesc("idx_rt_res_crdt", false);
        entity.addStringProperty("pics");
        entity.addStringProperty("thumbnails");
        entity.addStringProperty("orgpicname");
        entity.addStringProperty("audios");
        entity.addStringProperty("shortVideo");
        entity.addStringProperty("fullVideo");
        entity.addStringProperty("location");
        entity.addLongProperty("voteCount");
        entity.implementsSerializable();
    }

    private static void addRtNote(Schema schema) {
        Entity entity = schema.addEntity("RtNoteEntity");
        entity.addIdProperty().primaryKeyDesc();
        entity.addStringProperty("accountId");
        entity.addStringProperty("resourceId");
        entity.addStringProperty("message");
        entity.addLongProperty("createTime").indexDesc("idx_rt_note_crtm", false);
        entity.implementsSerializable();
    }

    private static void addRtBlacklist(Schema schema) {
        Entity entity = schema.addEntity("RtBlacklistEntity");
        entity.addIdProperty().primaryKeyDesc();
        entity.addStringProperty("blockUser");
        entity.implementsSerializable();
    }

    private static void addCcDriver(Schema schema) {
        Entity entity = schema.addEntity("CcDriverEntity");
        entity.addIdProperty().primaryKeyDesc();
        entity.addStringProperty("vehicleNumber");
        entity.addStringProperty("vehicleBrand");
        entity.implementsSerializable();
    }

    private static void addCcDriverTickets(Schema schema) {
        Entity entity = schema.addEntity("CcDriverTicketEntity");
        entity.addIdProperty().primaryKeyDesc();
        entity.addStringProperty("driverId");
        entity.addStringProperty("passengerId");
        entity.addStringProperty("fromAddress");
        entity.addStringProperty("toAddress");
        entity.addStringProperty("fromLocation");
        entity.addStringProperty("toLocation");
        entity.addLongProperty("departureTime");
        entity.addFloatProperty("money");
        entity.addIntProperty("status");
        entity.implementsSerializable();
    }

    private static void addCcPassengerTickets(Schema schema) {
        Entity entity = schema.addEntity("CcPassengerTicketEntity");
        entity.addIdProperty().primaryKeyDesc();
        entity.addStringProperty("driverId");
        entity.addStringProperty("passengerId");
        entity.addStringProperty("fromAddress");
        entity.addStringProperty("toAddress");
        entity.addStringProperty("fromLocation");
        entity.addStringProperty("toLocation");
        entity.addLongProperty("departureTime");
        entity.addFloatProperty("money");
        entity.addIntProperty("status");
        entity.implementsSerializable();
    }

    private static void addRouteTrackData(Schema schema) {
        Entity tracks = schema.addEntity("Tracks");
        tracks.addIdProperty().primaryKey();
        tracks.implementsSerializable();

        Entity route = schema.addEntity("Route");
        route.addIdProperty().columnName("_id").primaryKey();
        route.addStringProperty("description");
        route.addLongProperty("startTime");
        route.addLongProperty("endTime");
        route.addLongProperty("distance"); // meters
        route.addIntProperty("averageRpm"); // 1000x
        route.addIntProperty("maxRpm"); // 1000x
        route.addFloatProperty("maxSpeed"); // m/s
        route.addStringProperty("startLoc"); // lat,long
        route.addStringProperty("endLoc"); // lat,long
        route.addStringProperty("startAddress");
        route.addStringProperty("endAddress");
        route.addStringProperty("syncStatus"); // TBD sync status flag, NOT_UPLOAD; UPLOADED; NOT_DOWNLOAD;DOWNLOADED
        route.addStringProperty("filePath");
        route.addStringProperty("serverRouteId"); // server returned

        Entity routeData = schema.addEntity("RouteData");
        routeData.addIdProperty().columnName("_id").primaryKey();
        routeData.addDoubleProperty("latitude");
        routeData.addDoubleProperty("longitude");
        routeData.addLongProperty("time");
        routeData.addFloatProperty("speed");
        routeData.addIntProperty("rpm");
        routeData.addIntProperty("shift");
        routeData.addIntProperty("routeId");

//        Property tracksgpsId = gpsLocations.addLongProperty("tracksgpsId").unique().notNull().getProperty();
//        gpsLocations.addToMany(tracks, tracksgpsId);

        // create new table for speed, round, oil
        Entity bluetoothdata = schema.addEntity("BluetoothData");
        bluetoothdata.addIdProperty().columnName("_id").primaryKey();
        bluetoothdata.addIntProperty("speed");
        bluetoothdata.addIntProperty("round");
        bluetoothdata.addIntProperty("oil");

//        Property tracksblueId = bluetoothdata.addLongProperty("tracksblueId").unique().notNull().getProperty();
//        bluetoothdata.addToMany(tracks, tracksblueId);

        route.implementsSerializable();
        routeData.implementsSerializable();
        bluetoothdata.implementsSerializable();
    }

//    private static void addContact(Schema schema){
//        Entity contact = schema.addEntity("Contact");
//        contact.addIdProperty().columnName("_id").primaryKey();
//        contact.addStringProperty("phone");
//        contact.addStringProperty("nickName");
//        contact.addStringProperty("city");
//        contact.addByteArrayProperty("photo");
//        contact.implementsSerializable();
//    }
//
//    private static void addPendingContact(Schema schema){
//        Entity contact = schema.addEntity("PendingContact");
//        contact.addIdProperty().columnName("_id").primaryKey();
//        contact.addStringProperty("phone");
//        contact.addStringProperty("nickName");
//        contact.addStringProperty("city");
//        contact.addByteArrayProperty("photo");
//        contact.addIntProperty("status");
//        contact.implementsSerializable();
//    }

    private static void addGroupChatDetailEvent(Schema schema) {
        Entity groupChatDetailEvent = schema.addEntity("GroupChatDetailEvent");
        groupChatDetailEvent.addIdProperty();
        groupChatDetailEvent.addStringProperty("groupid");
        groupChatDetailEvent.addStringProperty("targetid");
        groupChatDetailEvent.implementsSerializable();
    }

    /*private static void addMembers(Schema schema) {
        Entity friend = schema.addEntity("GroupMember");
        friend.addIdProperty().columnName("_id").primaryKey();
        friend.addStringProperty("nickname");
        friend.addStringProperty("gender");
        friend.addStringProperty("avatarUrl");
        friend.implementsSerializable();
    }

    private static void addTrendMember(Schema schema) {
        Entity friend = schema.addEntity("TrendMember");
        friend.addIdProperty().columnName("accountId").primaryKey();
        friend.addStringProperty("nickname");
        friend.addStringProperty("gender");
        friend.addStringProperty("avatarUrl");
        friend.implementsSerializable();
    }*/

    private static void addVehicleInfo(Schema schema) {
        Entity vehicle = schema.addEntity("VehicleInfo");
        vehicle.addIdProperty().columnName("_id").primaryKey();
        vehicle.addLongProperty("accountId");
        vehicle.addStringProperty("vin");
        vehicle.addStringProperty("vinpwd");
        vehicle.addStringProperty("bleAddr");
        vehicle.addStringProperty("bleKey");
        vehicle.addBooleanProperty("activated");
        vehicle.implementsSerializable();

        Entity bindedVehicle = schema.addEntity("BindedVehicle");
        bindedVehicle.addIdProperty().primaryKey();
        bindedVehicle.addLongProperty("userId");
//        bindedVehicle.addLongProperty("id");
        bindedVehicle.addStringProperty("mcuid");
        bindedVehicle.addStringProperty("vin");
        bindedVehicle.addStringProperty("iccid");
        bindedVehicle.addStringProperty("imsi");
        bindedVehicle.addStringProperty("imei");
        bindedVehicle.addStringProperty("dealer_id");
        bindedVehicle.addStringProperty("hard_ware_code");
        bindedVehicle.addStringProperty("soft_ware_code");
        bindedVehicle.addStringProperty("vproto");
        bindedVehicle.addLongProperty("terminal_id");
        bindedVehicle.addStringProperty("seeds");
        bindedVehicle.addStringProperty("password");
        bindedVehicle.addLongProperty("vincpy");
        bindedVehicle.addIntProperty("imeicpy");
        bindedVehicle.addLongProperty("do_time");
        bindedVehicle.addStringProperty("machine_status");
        bindedVehicle.addStringProperty("machine_type");
        bindedVehicle.addStringProperty("machine_kind");
        bindedVehicle.implementsSerializable();
    }

    public static void addInteract(Schema schema) {
        Entity dynamic = schema.addEntity("Dynamic");
        dynamic.addStringProperty("msgId").notNull().primaryKey();
        dynamic.addStringProperty("authorId");
        dynamic.addStringProperty("content");
        dynamic.addDoubleProperty("lng");
        dynamic.addDoubleProperty("lat");
        dynamic.addStringProperty("address");
        dynamic.addIntProperty("visible");
        dynamic.addStringProperty("pic");
        dynamic.addStringProperty("video");
        dynamic.addStringProperty("audio");
        dynamic.addIntProperty("source");
        dynamic.addLongProperty("praiseCount");
        dynamic.addLongProperty("shareCount");
        dynamic.addLongProperty("commentCount");
        dynamic.addStringProperty("refMsgId");
        dynamic.addStringProperty("replyMsgId");
        dynamic.addStringProperty("campaignId");
        dynamic.addStringProperty("topicId");
        dynamic.addStringProperty("statisDate");
        dynamic.addIntProperty("isActivity");
        dynamic.addStringProperty("activityUrl");
        dynamic.addLongProperty("doTime");
        dynamic.addStringProperty("createTime");
        dynamic.addStringProperty("accountIdAvat");
        dynamic.addStringProperty("accountIdNickname");
        dynamic.addIntProperty("refMsgCounr");
        dynamic.addStringProperty("picList");
        dynamic.implementsSerializable();

        Entity topicAuthor = schema.addEntity("TopicAuthor");
        topicAuthor.addStringProperty("userId").notNull().primaryKey();
        topicAuthor.addStringProperty("nickName");
        topicAuthor.addStringProperty("avatar");
        topicAuthor.implementsSerializable();

        Entity topic = schema.addEntity("Topic");
        topic.addStringProperty("topicId").notNull().primaryKey();
        topic.addIntProperty("type");
        topic.addIntProperty("source");
        topic.addStringProperty("headPic");
        topic.addStringProperty("pic");
        topic.addStringProperty("video");
        topic.addStringProperty("subject");
        topic.addStringProperty("content");
//        public TopicAuthor author;
        topic.addIntProperty("state");
        topic.addLongProperty("praiseCount");
        topic.addLongProperty("shareCount");
        topic.addLongProperty("readCounnt");
        topic.addStringProperty("createTime");
        topic.addStringProperty("statisDate");
        Property authorIdProperty = topic.addStringProperty("userId").getProperty();
        topic.addToOne(topicAuthor, authorIdProperty);
        topic.implementsSerializable();

        Entity active = schema.addEntity("Active");
        active.addStringProperty("campaignId").notNull().primaryKey();
        active.addStringProperty("userId");
        active.addStringProperty("nickName");
        active.addStringProperty("title");
        active.addStringProperty("content");
        active.addStringProperty("cover");
        active.addStringProperty("campaignStartTime");
        active.addStringProperty("campaignEndTime");
        active.addStringProperty("registerStartTime");
        active.addStringProperty("registerEndTime");
        active.addStringProperty("deptLng");
        active.addStringProperty("deptLat");
        active.addStringProperty("deptAddr");
        active.addStringProperty("arriLng");
        active.addStringProperty("arriLat");
        active.addStringProperty("arriAddr");
        active.addLongProperty("maxMemberCount");
        active.addStringProperty("source");
        active.addStringProperty("state");
        active.addStringProperty("createTime");
        active.addStringProperty("chatroomId");
        active.addLongProperty("members");
        active.implementsSerializable();

        Entity comment = schema.addEntity("Comment");
        comment.addStringProperty("accountId").notNull().primaryKey();
        comment.addStringProperty("postMsgId");
        comment.addStringProperty("message");
        comment.addStringProperty("partitionId");
        comment.addStringProperty("avatar");
        comment.addStringProperty("nickName");
        comment.addStringProperty("createTime");
        comment.addLongProperty("lastModified");
        comment.addLongProperty("commentId");
        comment.implementsSerializable();


    }
}
